<?php defined('SYSPATH') or die('No direct script access.');

if (class_exists('Eventmanager') && defined('ON_FOLLOWED_LINK'))
{
    Eventmanager::addAction(ON_FOLLOWED_LINK, function (array $data)
    {
        DeliveryMan_Message_Delivery::instance(Model_Index::GSCMS_CONFIG('deliveryman.default.email'))->statusFollowedLink($data['email_id']);
    });

    if (isset($_GET['email_id']) && is_string($_GET['email_id']))
    {
        Eventmanager::callAction(ON_FOLLOWED_LINK, array(
            'email_id' => $_GET['email_id'],
        ), false);
    }
}

if (class_exists('Eventmanager') && defined('ON_MESSAGE_DELIVERY_STATUS'))
{
    Eventmanager::addAction(ON_MESSAGE_DELIVERY_STATUS, function (array $data)
    {
        DeliveryMan_Message_Delivery::instance(Model_Index::GSCMS_CONFIG('deliveryman.default.email'))->updateStatus($data['message_delivery_status']);
    });

    if (isset($_GET['mds']) && is_string($_GET['mds']))
    {
        Eventmanager::callAction(ON_MESSAGE_DELIVERY_STATUS, array(
            'message_delivery_status' => $_GET['mds'],
        ), false);
    }
}

if (class_exists('Eventmanager') && defined('ON_MAILING_DELIVERY_STATUS'))
{
    Eventmanager::addAction(ON_MAILING_DELIVERY_STATUS, function (array $data)
    {
        //TODO установить статус Model_MailingDelivery::STATUS_OPENED для сообщения
        $modelMailing_Delivery = ORM::factory('MailingDelivery')->where('id', '=', $data['message_delivery_id'])->find();
        $modelMailing_Delivery->status = Model_MailingDelivery::STATUS_OPENED;
        $modelMailing_Delivery->opened_at = date('Y-m-d H:i:s');
        $modelMailing_Delivery->save();
    });

    if (isset($_GET['mds']) && is_string($_GET['mds']))
    {
        Eventmanager::callAction(ON_MAILING_DELIVERY_STATUS, array(
            'message_delivery_status' => $_GET['mds'],
        ), false);
    }
}

Route::set('trackRead', 'deliveryman/message/delivery/<message_delivery_status>')
    ->defaults(array(
        'directory'  => 'DeliveryMan/Message',
        'controller' => 'Delivery',
        'action'     => 'trackRead',
    ));

Route::set('trackMailingRead', 'deliveryman/mailing/delivery/<message_delivery_status>')
    ->defaults(array(
        'directory'  => 'DeliveryMan/Mailing',
        'controller' => 'Delivery',
        'action'     => 'trackRead',
    ));

Route::set('unsubscribe', 'deliveryman/unsubscribe/<message_delivery_id_enc>')
    ->defaults(array(
        'controller' => 'DeliveryMan',
        'action'     => 'unsubscribe',
    ));

Route::set('unsubscribemailing', 'deliveryman/unsubscribemailing/<message_delivery_id_enc>')
    ->defaults(array(
        'controller' => 'DeliveryMan',
        'action'     => 'unsubscribemailing',
    ));

Route::set('messagetemplate', '__messagetemplate(/<action>(/<_param>))')
    ->defaults(array(
        'directory'  => 'backend',
        'controller' => 'messagetemplate',
        'action'     => 'index',
    ));

Route::set('mailings', '__mailings(/<action>(/<_param>))')
    ->defaults(array(
        'directory'  => 'backend',
        'controller' => 'mailings',
        'action'     => 'index',
    ));
