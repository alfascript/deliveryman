<section>
    <p><a class="btn btn-default" href="#/<?= BACKEND; ?>/mailings">&larr; Вернуться к истории рассылок</a></p>
</section>

<div class="alert alert-<?= $params['alert_color'] ?>"><h4><i class="fa fa-<?= $params['fa_icon'] ?>"></i> Рассылка
        №<?= $mailing->id . " " . $params['message'] ?></h4></div>

<!-- widget grid -->
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-4 col-md-5 col-lg-6 sortable-grid ui-sortable">
            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-editbutton="false" role="widget"
                 data-widget-editbutton="true" data-widget-sortable="false">
                <!-- widget header -->
                <header role="heading">
                    <div class="jarviswidget-ctrls" role="menu"><a href="#" class="button-icon jarviswidget-toggle-btn"
                                                                   rel="tooltip" title="" data-placement="bottom"
                                                                   data-original-title="Collapse"><i
                                class="fa fa-minus "></i></a> <a href="javascript:void(0);"
                                                                 class="button-icon jarviswidget-fullscreen-btn"
                                                                 rel="tooltip" title="" data-placement="bottom"
                                                                 data-original-title="Fullscreen"><i
                                class="fa fa-expand "></i></a> <a href="javascript:void(0);"
                                                                  class="button-icon jarviswidget-delete-btn"
                                                                  rel="tooltip" title="" data-placement="bottom"
                                                                  data-original-title="Delete"><i
                                class="fa fa-times"></i></a></div>
                    <div class="widget-toolbar" role="menu"><a data-toggle="dropdown"
                                                               class="dropdown-toggle color-box selector"
                                                               href="javascript:void(0);"></a>
                        <ul class="dropdown-menu arrow-box-up-right color-select pull-right">
                            <li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green"
                                      rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li>
                            <li><span class="bg-color-greenDark" data-widget-setstyle="jarviswidget-color-greenDark"
                                      rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li>
                            <li><span class="bg-color-greenLight" data-widget-setstyle="jarviswidget-color-greenLight"
                                      rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li>
                            <li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple"
                                      rel="tooltip" data-placement="top" data-original-title="Purple"></span></li>
                            <li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta"
                                      rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li>
                            <li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink" rel="tooltip"
                                      data-placement="right" data-original-title="Pink"></span></li>
                            <li><span class="bg-color-pinkDark" data-widget-setstyle="jarviswidget-color-pinkDark"
                                      rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li>
                            <li><span class="bg-color-blueLight" data-widget-setstyle="jarviswidget-color-blueLight"
                                      rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li>
                            <li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal" rel="tooltip"
                                      data-placement="top" data-original-title="Teal"></span></li>
                            <li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue" rel="tooltip"
                                      data-placement="top" data-original-title="Ocean Blue"></span></li>
                            <li><span class="bg-color-blueDark" data-widget-setstyle="jarviswidget-color-blueDark"
                                      rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li>
                            <li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken"
                                      rel="tooltip" data-placement="right" data-original-title="Night"></span></li>
                            <li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow"
                                      rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li>
                            <li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange"
                                      rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li>
                            <li><span class="bg-color-orangeDark" data-widget-setstyle="jarviswidget-color-orangeDark"
                                      rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span>
                            </li>
                            <li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red" rel="tooltip"
                                      data-placement="bottom" data-original-title="Red Rose"></span></li>
                            <li><span class="bg-color-redLight" data-widget-setstyle="jarviswidget-color-redLight"
                                      rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li>
                            <li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white"
                                      rel="tooltip" data-placement="right" data-original-title="Purity"></span></li>
                            <li><a href="javascript:void(0);" class="jarviswidget-remove-colors" data-widget-setstyle=""
                                   rel="tooltip" data-placement="bottom"
                                   data-original-title="Reset widget color to default">Remove</a></li>
                        </ul>
                    </div>
                    <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                    <h2><strong>Cтатистика</strong></h2>
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>
                <!-- end widget header -->
                <!-- widget div-->
                <div role="content">

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <table class="table table-hover table-bordered">
                            <tbody>
                            <?php foreach ($mailing_stats as $mailing_stat) : ?>
                                <tr>
                                    <td><?= $mailing_stat['title'] ?></td>
                                    <? if ($mailing_stat['no_percentage']) : ?>
                                        <td colspan="2" style="text-align: center;">
                                            <span><?= $mailing_stat['count'] ?></span>
                                        </td>
                                    <?php else : ?>
                                        <td style="text-align: center;">
                                            <span><?= $mailing_stat['count'] ?></span>
                                        </td>
                                        <td style="text-align: center;">
                                            <?= round(($mailing_stat['count'] * 100 / $mailing_stats['created']['count']), 1) ?>&#37;
                                        </td>
                                    <? endif; ?>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table> <!-- end table -->
                    </div>
                    <!-- end widget content -->
                </div>
                <!-- end widget div -->
            </div>
        </article>
        <!-- WIDGET END -->
    </div>
    <!-- end row -->
    <!-- row -->
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
            <div class="jarviswidget jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" role="widget"
                 data-widget-editbutton="true" data-widget-sortable="false" data-id="<?= $mailing->id; ?>">
                <!-- widget header -->
                <header role="heading">
                    <div class="jarviswidget-ctrls" role="menu"><a href="#" class="button-icon jarviswidget-toggle-btn"
                                                                   rel="tooltip" title="" data-placement="bottom"
                                                                   data-original-title="Collapse"><i
                                class="fa fa-minus "></i></a> <a href="javascript:void(0);"
                                                                 class="button-icon jarviswidget-fullscreen-btn"
                                                                 rel="tooltip" title="" data-placement="bottom"
                                                                 data-original-title="Fullscreen"><i
                                class="fa fa-expand "></i></a> <a href="javascript:void(0);"
                                                                  class="button-icon jarviswidget-delete-btn"
                                                                  rel="tooltip" title="" data-placement="bottom"
                                                                  data-original-title="Delete"><i
                                class="fa fa-times"></i></a></div>
                    <div class="widget-toolbar" role="menu"><a data-toggle="dropdown"
                                                               class="dropdown-toggle color-box selector"
                                                               href="javascript:void(0);"></a>
                        <ul class="dropdown-menu arrow-box-up-right color-select pull-right">
                            <li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green"
                                      rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li>
                            <li><span class="bg-color-greenDark" data-widget-setstyle="jarviswidget-color-greenDark"
                                      rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li>
                            <li><span class="bg-color-greenLight" data-widget-setstyle="jarviswidget-color-greenLight"
                                      rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li>
                            <li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple"
                                      rel="tooltip" data-placement="top" data-original-title="Purple"></span></li>
                            <li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta"
                                      rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li>
                            <li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink" rel="tooltip"
                                      data-placement="right" data-original-title="Pink"></span></li>
                            <li><span class="bg-color-pinkDark" data-widget-setstyle="jarviswidget-color-pinkDark"
                                      rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li>
                            <li><span class="bg-color-blueLight" data-widget-setstyle="jarviswidget-color-blueLight"
                                      rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li>
                            <li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal" rel="tooltip"
                                      data-placement="top" data-original-title="Teal"></span></li>
                            <li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue" rel="tooltip"
                                      data-placement="top" data-original-title="Ocean Blue"></span></li>
                            <li><span class="bg-color-blueDark" data-widget-setstyle="jarviswidget-color-blueDark"
                                      rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li>
                            <li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken"
                                      rel="tooltip" data-placement="right" data-original-title="Night"></span></li>
                            <li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow"
                                      rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li>
                            <li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange"
                                      rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li>
                            <li><span class="bg-color-orangeDark" data-widget-setstyle="jarviswidget-color-orangeDark"
                                      rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span>
                            </li>
                            <li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red" rel="tooltip"
                                      data-placement="bottom" data-original-title="Red Rose"></span></li>
                            <li><span class="bg-color-redLight" data-widget-setstyle="jarviswidget-color-redLight"
                                      rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li>
                            <li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white"
                                      rel="tooltip" data-placement="right" data-original-title="Purity"></span></li>
                            <li><a href="javascript:void(0);" class="jarviswidget-remove-colors" data-widget-setstyle=""
                                   rel="tooltip" data-placement="bottom"
                                   data-original-title="Reset widget color to default">Remove</a></li>
                        </ul>
                    </div>
                    <!--<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>-->
                    <h2><strong>Описание</strong></h2>
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>
                <!-- end widget header -->
                <!-- widget div-->
                <div role="content">
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                    </div>
                    <!-- end widget edit box -->
                    <div class="widget-body no-padding">
                        <div id="form-update-mailing" class="smart-form">
                            <fieldset>
                                <section>
                                    <h4 class="input" style="margin-bottom: 10px">Название рассылки:</h4>
                                    <div><?= HTML::chars($mailing->description); ?></div>
                                </section>
                            </fieldset>
                            <fieldset>
                                <section>
                                    <h4 class="input" style="margin-bottom: 10px">Шаблон письма:</h4>
                                    <?php foreach ($messagetemplates as $messagetemplateIndex => $messagetemplate) : ?>
                                        <?php if ($messagetemplate->id == $mailing->template_id) : ?>
                                            <div class="input"><?= $messagetemplate->name; ?></div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <div
                                        style="width: 100%; height: 400px; overflow: scroll; border: 1px #BDBDBD solid; box-sizing: border-box; margin-top: 5px;"
                                        id="templateView">
                                        <h1 class="text-center" style="margin-top: 10px;">Шаблон не выбран</h1>
                                    </div>
                                </section>
                            </fieldset>
                            <?php if ($mailing->only_confirmed_users == 1) : ?>
                                <fieldset>
                                    <section>
                                        <label class="label" style="color:#a90329">Только подтвержденным
                                            пользователям</label>
                                    </section>
                                </fieldset>
                            <?php endif; ?>
                            <fieldset>
                                <section>
                                    <h4 class="input" style="margin-bottom: 10px">Список адресатов:</h4>
                                    <div class="textarea textarea-resizable">
                                        <ul style="list-style-type: none; max-height: 200px; overflow: auto; border: 1px solid rgba(0,0,0,.1); padding-left: 5px;">
                                            <li><?= str_replace("\n", "</li><li>", $mailing->recipients) ?></li>
                                        </ul>
                                    </div>
                                </section>
                            </fieldset>
                            <fieldset>
                                <section>
                                    <h4 class="input" style="margin-bottom: 10px">Была запланирована на:</h4>
                                    <div><?= $mailing->scheduled_to ?></div>
                                </section>
                            </fieldset>
                            <?php if (!empty($params['finished'])) : ?>
                                <fieldset>
                                    <section>
                                        <h4 class="input" style="margin-bottom: 10px"><?= $params['finished'] ?></h4>
                                        <div><?= $mailing->finished_at ?></div>
                                    </section>
                                </fieldset>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <!-- end widget div -->
            </div>
        </article>
        <!-- WIDGET END -->
    </div>
    <!-- end row -->
</section>
<!-- end widget grid -->

<script>
    <? if($mailing->loaded()) { ?>
    loadTemplate(<?=$mailing->template_id?>);
    <? } ?>
    function loadTemplate(id) {
        $.ajax({
            url: '/__administer__/mailings/loadtemplate/' + id + '?_clean',
            success: function (r) {
                $('#templateView').html(r);
            }
        });
    }
    pageSetUp();
</script>
