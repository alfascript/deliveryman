function mailings_mailing($scope) {
    'use strict';
    
    pageSetUp($scope);

    var pagefunction = function() {

        var $scopeWidget = $('#wid-id-item', $scope);
        var $formUpdateMailing= $('#form-update-mailing', $scopeWidget);
        var mailing_id = $formUpdateMailing.data('mailing_id');
        var callback = null;
        $formUpdateMailing.validate({
            submitHandler: function() {
                $formUpdateMailing.ajaxSubmit({
                    success: function() {
                        $.smallBox({
                            title: 'Сохранено',
                            content : 'Рассылка ' + (mailing_id ? ' обновлена' : ' создана'),
                            color : "#739E73",
                            iconSmall : "fa fa-thumbs-up bounce animated",
                            timeout : 4000
                        });
                        if ( ! mailing_id ) {
                            window.location.hash = '#/' + $.backend.path + '/mailings';
                        }
                        if ( callback ) {
                            callback();
                            callback = null;
                        }
                    },
                    error: function(response) {
                        if ( response.status === 400 ) {
                            $formUpdateMailing.data('validator').showErrors(JSON.parse(response.responseText));
                        } else {
                            $.smallBox({
                                title : 'Ошибка',
                                content : 'Ошибка при сохранении рассылки',
                                color : "#C46A69",
                                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                                timeout : 4000
                            });
                        }
                    }
                });
            }
        });
   };


    // load related plugins

    loadScript("/assets/smartadmin.1.4.1/js/plugin/jquery-form/jquery-form.min.js", pagefunction);
}