function mailings_index($scope) {
    'use strict';
    
    pageSetUp($scope);

    var pagefunction = function() {

        var $filters = $('#filters', $scope);
        var $formFilters = $('#form-filters', $filters);

        var table = $('#messagetemplate-datatable', $scope).dataTable({
            serverSide: true,
            "pageLength": 50,
            "deferRender": true,
            "autoWidth": false,
            "ajax": {
                  "url": "/" + $.backend.path + "/mailings/datatable"
                , "type": "POST"
                , data: function ( data ) {
                    $.each($formFilters.serializeArray(), function() {
                        data[this.name] = this.value;
                    });
                }
            },
            "initComplete": function(settings, json) {
				$('.ColVis_Button').addClass('btn btn-default btn-sm').html('Столбцы <i class="fa fa-caret-down"></i>');
                $filters = $filters
                    .detach()
                    .appendTo($('.dt-toolbar div:eq(0)', table.parent()))
                    .show()
                    .siblings('.dataTables_filter')
                    .css('width', 'auto')
                    .css('margin-right', '10px')
                    .end();
                //pageSetUp($(table));
            },
            "order": [[ 0, "desc" ]],
            "columns": [
                {
                    "data": "id",
                    "render": function(data,type,row) {
                        return '<a href="#/' + $.backend.path + '/mailings/edit/' + data + '">'+data+'</a>';
                    }
                },
                {
                    "data": "description"
                },
                {
                    "data": "template_id"
                },
                {
                    "data": "created_at"
                },
                {
                    "data": "scheduled_to"
                },
                {
                    "data": "finished_at"
                },

            ],
            "rowCallback": function( row, data, displayIndex ) {
                $(row).addClass('cursor-pointer');
            }
        });

        table.on('dblclick', 'tbody tr', function() {
            window.location = $('td:eq(0) a', this).attr('href');
        });
        
        var $btnFilters = $('#btn-filters', $filters);
        $btnFilters.on('click', function(e) {
            e.preventDefault();
            if ( ! $formFilters.is(':visible') ) {
                $('#dt_basic_filter input', $scope).val('');
            }
            $formFilters.toggle();
        });
        
        $('#close-form-filters', $formFilters).on('click', function(e) {
            e.preventDefault();
            $formFilters.hide();
        });

        $('#reset-form-filters', $formFilters).on('click', function(e) {
            e.preventDefault();
            $formFilters.find('input[type="text"],select').val('');
            table._fnReDraw();
        });

        var applyFormFilters = function() {
            $formFilters.hide();
            table._fnReDraw();
        };
        
        $('#apply-form-filters', $formFilters).on('click', function(e) {
            e.preventDefault();
            applyFormFilters();
        });
        
        $('input', $formFilters).keypress(function(e) {
            if (13 == e.which) {
                e.preventDefault();
                applyFormFilters();
            }
        });
        
    };

    // load related plugins
    loadScript("/assets/smartadmin.1.4.1/js/plugin/datatables/jquery.dataTables.min.js", function(){
        loadScript("/assets/smartadmin.1.4.1/js/plugin/datatables/dataTables.colVis.min.js", function(){
            loadScript("/assets/smartadmin.1.4.1/js/plugin/datatables/dataTables.tableTools.min.js", function(){
                loadScript("/assets/smartadmin.1.4.1/js/plugin/datatables/dataTables.bootstrap.min.js", pagefunction);
            });
        });
    });
}