<section>
    <p>
        <a class="btn btn-default" href="#/<?= BACKEND; ?>/mailings">&larr; Вернуться к истории рассылок</a>
    </p>
</section>

<section id="widget-grid">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="jarviswidget jarviswidget-sortable" id="wid-id-item" data-widget-colorbutton="false" data-widget-editbutton="true" data-widget-sortable="false" data-id="<?= $mailing->id; ?>">
                <header role="heading">
                    <h2><?= $mailing->loaded() ? 'Редактирование рассылки' : 'Создание рассылки'; ?></h2>
                    
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                        <input class="form-control" type="text">
                    </div>
                    <div class="widget-body no-padding">
                        <form id="form-update-mailing" class="smart-form" action="/<?= BACKEND; ?>/mailings/create/<?= $mailing->id; ?>" method="post" enctype="multipart/form-data" data-mailing_id="<?= $mailing->id; ?>">
                            <fieldset>
                                <section>
                                    <label class="label" for="">Назначение рассылки</label>
                                    <label class="input" for=""><input type="text" name="description" value="<?= HTML::chars($mailing->description); ?>" required="" /></label>
                                </section>
                            </fieldset>
                            <fieldset>
                                <section>
                                    <label class="label" for="">Шаблон письма</label>
                                    <label class="select" for="">
                                        <select name="template_id">
                                            <option value="">Выбрать шаблон...</option>
                                            <?php foreach ( $messagetemplates as $messagetemplateIndex => $messagetemplate ) : ?>
                                                <option value="<?= $messagetemplate->id; ?>"<?php if ( $messagetemplate->id == $mailing->template_id ) : ?> selected="selected"<?php endif; ?>><?=$messagetemplate->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <i></i>
                                    </label>
                                    <div style="width: 100%; height: 400px; overflow: scroll; border: 1px #BDBDBD solid; box-sizing: border-box; margin-top: 5px;" id="templateView">
                                        <h1 class="text-center" style="margin-top: 10px;">Шаблон не выбран</h1>
                                    </div>
                                </section>
                            </fieldset>
                            <fieldset>
                                <section>
                                    <label class="label">Только подтвержденным пользователям:  <input type="checkbox" name="only_confirmed_users" value="1" <?=($mailing->only_confirmed_users == 1) ? 'checked' : ''?>/></label>
                                </section>
                            </fieldset>
                            <fieldset>
                                <section>
                                    <label class="label" for="">Список адресатов <span class="small txt-color-red">(По одному на каждую строчку)</span></label>
                                    <label class="textarea textarea-resizable" for=""><textarea style="width: 300px; height:200px; font-family: Consolas, Menlo, Monaco, Courier, monospace" rows="10" name="recipients"><?= $mailing->recipients; ?></textarea></label>
                                </section>
                            </fieldset>
                            <fieldset>
                                <section>
                                    <label class="label" for="">Запланировать отправку на</label>
                                    <label class="input" for="" style="width:300px;">
                                        <div class="input-group">
                                            <input id="scheduled_date" type="text" name="scheduled_to[date]" placeholder="Дата" class="form-control" value="<?=substr($mailing->scheduled_to, 0, 10)?>">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input id="scheduled_time" type="text" name="scheduled_to[time]" placeholder="Время" class="form-control" value="<?=substr($mailing->scheduled_to, -8)?>">
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </label>
                                </section>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                                <a class="btn btn-default" href="#/<?= BACKEND; ?>/mailings">Отменить</a>
							</footer>
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>

<script>
    $('#scheduled_date').datepicker({
        dateFormat: 'yy-mm-dd'
    });
    loadScript('/vendor/alfascript/deliveryman/views/backend/mailings/mailing.js?v=1.0.3', function() {
        mailings_mailing($('#content'));
        <? if($mailing->loaded()) { ?>
        loadTemplate(<?=$mailing->template_id?>);
    <? } ?>
    });
    loadScript('/assets/smartadmin.1.4.1/js/plugin/bootstrap-timepicker/bootstrap-timepicker.min.js', function() {
        $('#scheduled_time').timepicker({
            showMeridian: false,
            defaultTime: '00:00',
            minuteStep: 5
        });
    });

    function loadTemplate(id)
    {
        $.ajax({
            url: '/__administer__/mailings/loadtemplate/' + id + '?_clean',
            success: function(r)
            {
                $('#templateView').html(r);
            }
        });
    }

    $('select[name=template_id]').change(function(e){
        loadTemplate($(this).val())
    });


</script>
