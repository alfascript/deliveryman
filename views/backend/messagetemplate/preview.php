<? if(isset($messagetemplate)) { ?>

<h1 class="well" style="padding:16px 16px; margin: 10px; border-radius: 0;">
    <span class="text-muted">Заголовок:</span> <?=$messagetemplate->messageheader->subject?>
</h1>
<div style="margin: 10px">
    <?=$messagetemplate->messagebody->body?>
</div>

<? } else { ?>
<h1 class="txt-color-red text-center">
    <i class="fa fa-3x fa-warning"></i>
</h1>
<h1 class="txt-color-red text-center">Шаблон не найден</h1>
<? } ?>
