<section id="widget-grid">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="true" data-widget-sortable="false">
                <header role="heading">
                    <h2>Сообщения</h2>

                    <!-- Actions specific to this page -->
                    <div class="widget-toolbar smart-form" role="menu">
                        <div class="btn-group">
                            <a class="btn btn-default btn-xs" href="#/<?= BACKEND; ?>/messagetemplate/item/"><i class="fa fa-plus" style="padding-right: 6px;"></i>Добавить сообщение</a>
                        </div>
					</div>
                    <!-- END Actions specific to this page -->
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                        <input class="form-control" type="text">
                    </div>
                    <div class="widget-body no-padding">
                        <div id="filters" style="display: none;">
                            <button id="btn-filters" class="btn btn-default btn-sm">Фильтры <i class="fa fa-angle-down"></i></button>
                            <form id="form-filters" class="form-horizontal" style="display: none; position: absolute; left: 224px; top: 31px; z-index: 2; background: #fff; padding: 6px 14px 14px 14px; border: 1px solid #dcdcdc; box-shadow: 0 4px 8px rgba(0,0,0,.5);">
                                <div>
                                    <div class="pull-right"><a id="close-form-filters" href="#" style="text-decoration: none; color: gray; font-size: 95%;">Закрыть <i class="fa fa-times"></i></a></div>
                                    <div style="clear: both;"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label style="text-align: left !important;">
                                                <input style="vertical-align: -2px;" type="checkbox" name="old" value="1"> Старые
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label style="text-align: left !important;">
                                                <input style="vertical-align: -2px;" type="checkbox" name="disabled" value="1"> Выключенные
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a id="reset-form-filters" class="btn btn-default" href="#">Сбросить</a>
                                            <a id="apply-form-filters" class="btn btn-primary" href="#">Применить</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <table id="messagetemplate-datatable" class="table table-striped table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Название</th>
                                <th>Тема</th>
                                <th>Последнее редактирование</th>
                                <th>Системное</th>
                                <th>Устарело</th>
                                <th>Включен</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>

<script>
    loadScript('/vendor/alfascript/deliveryman/views/backend/messagetemplate/index.js?v=1.0.0', function() {
        messagetemplate_index($('#content'));
    });
</script>
