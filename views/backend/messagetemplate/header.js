function messagetemplate_header($scope) {
    'use strict';
    
    pageSetUp($scope);

    var pagefunction = function() {

        var $scopeWidget = $('#wid-id-item', $scope);
        var $formUpdateTemplate= $('#form-update-messagetemplate', $scopeWidget);
        var messagetemplate_id = $formUpdateTemplate.data('messagetemplate_id');
        var callback = null;
        $formUpdateTemplate.validate({
            submitHandler: function() {
                $formUpdateTemplate.ajaxSubmit({
                    success: function() {
                        $.smallBox({
                            title: 'Сохранено',
                            content : 'Заголовок ' + (messagetemplate_id ? ' обновлен' : ' создан'),
                            color : "#739E73",
                            iconSmall : "fa fa-thumbs-up bounce animated",
                            timeout : 4000
                        });
                        if ( ! messagetemplate_id ) {
                            window.location.hash = '#/' + $.backend.path + '/messagetemplate/headers';
                        }
                        if ( callback ) {
                            callback();
                            callback = null;
                        }
                    },
                    error: function(response) {
                        if ( response.status === 400 ) {
                            $formUpdateTemplate.data('validator').showErrors(JSON.parse(response.responseText));
                        } else {
                            $.smallBox({
                                title : 'Ошибка',
                                content : 'Ошибка при сохранении заголовка',
                                color : "#C46A69",
                                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                                timeout : 4000
                            });
                        }
                    }
                });
            }
        });
   };


    // load related plugins

    loadScript("/assets/smartadmin.1.4.1/js/plugin/jquery-form/jquery-form.min.js", pagefunction);
}