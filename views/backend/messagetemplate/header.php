<section>
    <p>
        <a class="btn btn-default" href="#/<?= BACKEND; ?>/messagetemplate/headers">&larr; Вернуться к списку заголовков</a>
    </p>
</section>

<section id="widget-grid">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="jarviswidget jarviswidget-sortable" id="wid-id-item" data-widget-colorbutton="false" data-widget-editbutton="true" data-widget-sortable="false" data-id="<?= $messageheader->id; ?>">
                <header role="heading">
                    <h2><?= $messageheader->loaded() ? 'Редактирование заголовка' : 'Добавление заголовка'; ?></h2>
                    
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                        <input class="form-control" type="text">
                    </div>
                    <div class="widget-body no-padding">
                        <form id="form-update-messagetemplate" class="smart-form" action="/<?= BACKEND; ?>/messagetemplate/header/<?= $messageheader->id; ?>" method="post" enctype="multipart/form-data" data-messagetemplate_id="<?= $messageheader->id; ?>">
                            <fieldset>
                                <section>
                                    <label class="label" for="">Текст заголовка</label>
                                    <label class="input" for=""><input type="text" name="subject" value="<?= HTML::chars($messageheader->subject); ?>" required="" /></label>
                                </section>
                                <section>
                                    <label class="label" for="">Группа заголовка</label>
                                    <label class="input" for=""><input type="text" name="group" value="<?= HTML::chars($messageheader->group); ?>" /></label>
                                </section>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                                <a class="btn btn-default" href="#/<?= BACKEND; ?>/messagetemplate/headers">Отменить</a>
                            </footer>
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>

<script>
    loadScript('/vendor/alfascript/deliveryman/views/backend/messagetemplate/header.js?v=1.0.2', function() {
        messagetemplate_header($('#content'));
    });
</script>
