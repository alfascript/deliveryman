function messagetemplate_item($scope) {
    'use strict';
    
    pageSetUp($scope);

    var pagefunction = function() {

        var $scopeWidget = $('#wid-id-item', $scope);
        var $formUpdateTemplate= $('#form-update-messagetemplate', $scopeWidget);
        var messagetemplate_id = $formUpdateTemplate.data('messagetemplate_id');
        var callback = null;
        $formUpdateTemplate.validate({
            submitHandler: function() {
                $formUpdateTemplate.ajaxSubmit({
                    success: function() {
                        $.smallBox({
                            title: 'Сохранено',
                            content : 'Сообщение ' + (messagetemplate_id ? ' обновлено' : ' создано'),
                            color : "#739E73",
                            iconSmall : "fa fa-thumbs-up bounce animated",
                            timeout : 4000
                        });
                        if ( ! messagetemplate_id ) {
                            window.location.hash = '#/' + $.backend.path + '/messagetemplate/';
                        }
                        if ( callback ) {
                            callback();
                            callback = null;
                        }
                    },
                    error: function(response) {
                        if ( response.status === 400 ) {
                            $formUpdateTemplate.data('validator').showErrors(JSON.parse(response.responseText));
                        } else {
                            $.smallBox({
                                title : 'Ошибка',
                                content : 'Ошибка при сохранении сообщений',
                                color : "#C46A69",
                                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                                timeout : 4000
                            });
                        }
                    }
                });
            }
        });
        
        $('.btn-send', $scopeWidget).on('click', function(e) {
            e.preventDefault();
            if ( ! $formUpdateTemplate.valid() ) {
                return;
            }
            
            var $btnSend = $(this);
            $btnSend.button('loading');
            callback = function() {
                $.ajax({
                      type: 'POST'
                    , url: $btnSend.attr('href').substring($btnSend.attr('href').indexOf("#") + 1)
                    , data: {
                          emailOverride: $('input[name="emailOverride"]', $scope).val()
                        , smsOverride: $('input[name="smsOverride"]', $scope).val()
                    }
                })
                .done(function() {
                    $.smallBox({
                        title: 'Отправлено',
                        content : 'Тестовое сообщение-' + $btnSend.data('deliverySystem') + ' отправлено',
                        color : "#739E73",
                        iconSmall : "fa fa-thumbs-up bounce animated",
                        timeout : 6000
                    });
                })
                .fail(function() {
                    $.smallBox({
                        title : 'Ошибка',
                        content :  'Возникла ошибка при отправке сообщения',
                        color : "#A90329",
                        iconSmall : "fa fa-thimes bounce animated",
                        timeout : 4000
                    });
                })
                .always(function() {
                    $btnSend.button('reset');
                });
            };
            $formUpdateTemplate.triggerHandler('submit');
        });
   };


    // load related plugins

    loadScript("/assets/smartadmin.1.4.1/js/plugin/jquery-form/jquery-form.min.js", pagefunction);
}