<section id="widget-grid">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="true" data-widget-sortable="false">
                <header role="heading">
                    <h2>Заголовки</h2>

                    <!-- Actions specific to this page -->
                    <div class="widget-toolbar smart-form" role="menu">
                        <div class="btn-group">
                            <a class="btn btn-default btn-xs" href="#/<?= BACKEND; ?>/messagetemplate/header"><i class="fa fa-plus" style="padding-right: 6px;"></i>Добавить заголовок</a>
                        </div>
                    </div>
                    <!-- END Actions specific to this page -->
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                        <input class="form-control" type="text">
                    </div>

                    <div class="widget-body no-padding">
                        <table id="messagetemplate-datatable" class="table table-striped table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Текст заголовка</th>
                                <th>Кол-во связей</th>
                                <th>Группа</th>
                                <th>Время создания</th>
                                <th>Время изменения</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>

<script>
    loadScript('/vendor/alfascript/deliveryman/views/backend/messagetemplate/headers.js?v=1.0.0', function() {
        messagetemplate_headers($('#content'));
    });
</script>
