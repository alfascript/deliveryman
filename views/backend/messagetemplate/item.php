<section>
    <p>
        <a class="btn btn-default" href="#/<?= BACKEND; ?>/messagetemplate">&larr; Вернуться к списку сообщений</a>
    </p>
</section>

<section id="widget-grid">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="jarviswidget jarviswidget-sortable" id="wid-id-item" data-widget-colorbutton="false" data-widget-editbutton="true" data-widget-sortable="false" data-id="<?= $messagetemplate->id; ?>">
                <header role="heading">
                    <h2><?= $messagetemplate->loaded() ? 'Редактирование сообщения' : 'Добавление сообщения'; ?></h2>
                    
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                        <input class="form-control" type="text">
                    </div>
                    <div class="widget-body no-padding">
                        <form id="form-update-messagetemplate" class="smart-form" action="/<?= BACKEND; ?>/messagetemplate/item/<?= $messagetemplate->id; ?>" method="post" enctype="multipart/form-data" data-messagetemplate_id="<?= $messagetemplate->id; ?>">
                            <fieldset>
                                <section>
                                    <label class="label" for="">Название сообщения</label>
                                    <label class="input" for=""><input type="text" name="name" value="<?= HTML::chars($messagetemplate->name); ?>"<?php if ( $messagetemplate->loaded() && $messagetemplate->system ) : ?> readonly=""<?php endif; ?> required="" /></label>
                                </section>
                                <section>
                                    <label class="label" for="">Тема письма</label>
                                    <label class="select" for="">
                                        <select name="header_id">
                                            <?php foreach ( $messageHeaders as $messageHeader ) : ?>
                                                <option value="<?= $messageHeader->id; ?>"<?php if ( $messageHeader->id == $messagetemplate->header_id ) : ?> selected="selected"<?php endif; ?>><?=$messageHeader->subject; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <i></i>
                                    </label>
                                </section>
                                <section>
                                    <label class="label" for="">X-Postmaster-Msgtype</label>
                                    <label class="input" for=""><input type="text" name="postmaster_msgtype" value="<?= $messagetemplate->postmaster_msgtype; ?>" /></label>
                                </section>
                                <section>
                                    <label class="label" for="">Текст письма</label>
                                    <label class="select" for="">
                                        <select name="body_id">
                                            <option value="0"<?php if ( 0 == $messagetemplate->body_id ) : ?> selected="selected"<?php endif; ?>>Не выбран</option>
                                            <?php foreach ( $messageBodies as $messageBodyIndex => $messageBody ) : ?>
                                                <option value="<?= $messageBody->id; ?>"<?php if ( $messageBody->id == $messagetemplate->body_id ) : ?> selected="selected"<?php endif; ?>><?=$messageBody->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <i></i>
                                    </label>

                                    <div style="width: 100%; height: 400px; overflow: scroll; border: 1px #BDBDBD solid; box-sizing: border-box; margin-top: 5px;" id="bodyView"></div>

                                </section>
                                <section>
                                    <label class="label" for="">Переопределение драйвера доставки email-сообщений</label>
                                </section>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="select" for="">
                                            <select name="emailDriverOverride">
                                                <option value="">-- по умолчанию --</option>
                                                <?php foreach ( $emailDrivers as $emailDriver ) : ?>
                                                <option value="<?= $emailDriver; ?>"<?php if ( $emailDriver == $messagetemplate->emailDriverOverride ) : ?> selected="selected"<?php endif; ?>><?= $emailDriver; ?></option>
                                                <?php endforeach; ?>
                                            </select>                                        
                                            <i></i>
                                        </label>
                                    </section>
                                    <section class="col col-2">
                                        <label class="input" for=""">
                                            <input name="emailOverride">
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label class="label" for=""><?php
                                            if ( $messagetemplate->loaded() && null !== Auth::instance()->get_user()->gso_user_id ) :
                                                ?><a class="btn-send btn btn-default btn-sm" href="<?= BACKEND; ?>/messagetemplate/sendEmail/<?= $messagetemplate->id; ?>" data-delivery-system="письмо" data-loading-text="&lt;i class=&quot;fa fa-refresh fa-spin&quot;&gt;&lt;/i&gt;&nbsp; Отправка тестового письма"><i class="fa fa-envelope-o" style="padding-right: 6px;"></i>Отправить тестовое письмо</a><?php
                                            endif;
                                        ?></label>
                                    </section>
                                </div>
                                <section>
                                    <label class="label" for="">Текст SMS и сообщения в личном кабинете</label>
                                     <label class="textarea textarea-resizable" for=""><textarea style="width: 100%;" rows="5" name="bodyShort" required=""><?= $messagetemplate->bodyShort; ?></textarea></label>
                                </section>
                                <section>
                                    <label class="label" for="">Переопределение драйвера доставки sms-сообщений</label>
                                </section>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="select" for="">
                                            <select name="smsDriverOverride">
                                                <option value="">-- по умолчанию --</option>
                                                <?php foreach ( $smsDrivers as $smsDriver ) : ?>
                                                <option value="<?= $smsDriver; ?>"<?php if ( $smsDriver == $messagetemplate->smsDriverOverride ) : ?> selected="selected"<?php endif; ?>><?= $smsDriver; ?></option>
                                                <?php endforeach; ?>
                                            </select>                                        
                                            <i></i>
                                        </label>
                                    </section>
                                    <section class="col col-2">
                                        <label class="input" for=""">
                                            <input name="smsOverride">
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label class="label" for=""><?php
                                            if ( $messagetemplate->loaded() && null !== Auth::instance()->get_user()->gso_user_id ) :
                                                ?><a class="btn-send btn btn-default btn-sm" href="<?= BACKEND; ?>/messagetemplate/sendSms/<?= $messagetemplate->id; ?>" data-delivery-system="SMS" data-loading-text="&lt;i class=&quot;fa fa-refresh fa-spin&quot;&gt;&lt;/i&gt;&nbsp; Отправка тестового SMS"><i class="fa fa-envelope-o" style="padding-right: 6px;"></i>Отправить тестовое SMS</a><?php
                                            endif;
                                        ?></label>
                                    </section>
                                </div>
                                <section>
                                    <label class="label" for="">Текст модального диалогового окна</label>
                                    <label class="textarea textarea-resizable" for=""><textarea style="width: 100%;" rows="5" name="bodyModal"><?= $messagetemplate->bodyModal; ?></textarea></label>
                                </section>
                                <section>
                                    <label class="label" for=""><?php
                                        if ( $messagetemplate->loaded() && null !== Auth::instance()->get_user()->gso_user_id ) :
                                            ?><a class="btn-send btn btn-default btn-sm" href="<?= BACKEND; ?>/messagetemplate/sendNotificationModal/<?= $messagetemplate->id; ?>" data-delivery-system="модалка" data-loading-text="&lt;i class=&quot;fa fa-refresh fa-spin&quot;&gt;&lt;/i&gt;&nbsp; Отправка тестовой модалки"><i class="fa fa-envelope-o" style="padding-right: 6px;"></i>Отправить тестовую модалку</a><?php
                                        endif;
                                    ?></label>
                                </section>
                                <section>
                                    <label class="checkbox">
                                        <input type="checkbox" name="old" value="1"<?php if ( $messagetemplate->old ) : ?> checked="checked"<?php endif; ?>>
                                        <i></i>Устарело
                                    </label>
                                    <label class="checkbox">
                                        <input type="checkbox" name="enabled" value="1"<?php if ( $messagetemplate->enabled ) : ?> checked="checked"<?php endif; ?>>
                                        <i></i>Включен
                                    </label>
                                </section>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                                <a class="btn btn-default" href="#/<?= BACKEND; ?>/messagetemplate">Отменить</a>
							</footer>
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>

<script>

    function loadBody(id)
    {
        $.ajax({
            url: '/__administer__/messagetemplate/bodyview/' + id + '?_clean',
            success: function(r)
            {
                $('#bodyView').html(r);
            }
        });
    }

    $('select[name=body_id]').change(function(e){
        loadBody($(this).val())
    });

    loadScript('/vendor/alfascript/deliveryman/views/backend/messagetemplate/item.js?v=1.0.2', function() {
        messagetemplate_item($('#content'));
        loadBody(<?=$messagetemplate->body_id?>);
    });
</script>
