<section>
    <p>
        <a class="btn btn-default" href="#/<?= BACKEND; ?>/messagetemplate/bodies">&larr; Вернуться к списку шаблонов</a>
    </p>
</section>

<section id="widget-grid">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="jarviswidget jarviswidget-sortable" id="wid-id-item" data-widget-colorbutton="false" data-widget-editbutton="true" data-widget-sortable="false" data-id="<?= $messagebody->id; ?>">
                <header role="heading">
                    <h2><?= $messagebody->loaded() ? 'Редактирование шаблона сообщения' : 'Добавление шаблона сообщения'; ?></h2>
                    
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                        <input class="form-control" type="text">
                    </div>
                    <div class="widget-body no-padding">
                        <form id="form-update-messagetemplate" class="smart-form" action="/<?= BACKEND; ?>/messagetemplate/body/<?= $messagebody->id; ?>" method="post" enctype="multipart/form-data" data-messagetemplate_id="<?= $messagebody->id; ?>">
                            <fieldset>
                                <section>
                                    <label class="label" for="">Название шаблона</label>
                                    <label class="input" for=""><input type="text" name="name" value="<?= HTML::chars($messagebody->name); ?>" required="" /></label>
                                </section>
                                <section>
                                    <label class="label" for="">Группа шаблона</label>
                                    <label class="input" for=""><input type="text" name="group" value="<?= HTML::chars($messagebody->group); ?>" /></label>
                                </section>
                                <section>
                                    <label class="label" for="">Текст шаблона</label>
                                    <label class="textarea textarea-resizable" for=""><textarea style="width: 100%; height:400px; font-family: Consolas, Menlo, Monaco, Courier, monospace" rows="10" name="body"><?= $messagebody->body; ?></textarea></label>
                                </section>
                                <section>
                                    <label class="label" for="">Предпросмотр</label>
                                    <label class="input" for="">
                                        <div style="width: 100%; height: 400px; overflow: scroll; border: 1px #BDBDBD solid; box-sizing: border-box; margin-top: 5px;" id="bodyView"></div>
                                    </label>
                                </section>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                                <a class="btn btn-default" href="#/<?= BACKEND; ?>/messagetemplate/bodies">Отменить</a>
							</footer>
                        </form>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>

<script>

    function renderBody()
    {
        var url = '<?=GOLDENSTAR_URL?>';
        var text = $('textarea[name="body"]').val();
        $('#bodyView').html('');
        $('#bodyView').append(text.split('{{URL}}').join(url));
    }

    loadScript('/vendor/alfascript/deliveryman/views/backend/messagetemplate/body.js?v=1.0.2', function() {
        messagetemplate_body($('#content'));
        renderBody();

        $('textarea[name="body"]').on('keyup', function(e){
            renderBody();
        });
    });
</script>
