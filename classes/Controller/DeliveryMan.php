<?php defined('SYSPATH') or die('No direct script access.');

class Controller_DeliveryMan extends Controller
{
    
    public function action_unsubscribe()
    {
        $message_delivery_id_enc = $this->request->param('message_delivery_id_enc');
        $message_delivery_id = Encrypt::instance('deliveryman')->decode(base64_decode(strtr($message_delivery_id_enc, '-_', '+/')));
        if ( false === $message_delivery_id || ! ctype_digit($message_delivery_id) )
        {
            throw HTTP_Exception::factory(404);
        }
        
        $message_delivery = ORM::factory('Message_Delivery')->with('message:user')->where('message_delivery.id', '=', $message_delivery_id)->find();
        if ( ! $message_delivery->loaded() || ! $message_delivery->message->loaded() || ! $message_delivery->message->user->loaded() )
        {
            throw HTTP_Exception::factory(404);
        }
        
        DeliveryMan_Message_Delivery::instance(Model_Index::GSCMS_CONFIG('deliveryman.default.email'))
            ->updateStatus(array(
                'message_delivery_id' => $message_delivery->id,
                'status' => 'unsubscribe',
            ));
        
        $userprofile = $message_delivery->message->user->profile;
        $userprofile->notice_advertisment = 0;
        $userprofile->save();
        
        $this->redirect('/userprofile');
    }

    public function action_unsubscribemailing()
    {
        $message_delivery_id_enc = $this->request->param('message_delivery_id_enc');
        $message_delivery_id = Encrypt::instance('deliveryman')->decode(base64_decode(strtr($message_delivery_id_enc, '-_', '+/')));
        if ( false === $message_delivery_id || ! ctype_digit($message_delivery_id) )
        {
            throw HTTP_Exception::factory(404);
        }

        $message_delivery = ORM::factory('MailingDelivery')->where('id', '=', $message_delivery_id)->find();
        if ( ! $message_delivery->loaded() || ! $message_delivery->recipient_id )
        {
            throw HTTP_Exception::factory(404);
        }

        $userprofile = ORM::factory('Userprofile')->where('user_id', '=', $message_delivery->recipient_id)->find();
        $userprofile->notice_advertisment = 0;
        $userprofile->save();
        $message_delivery->unsubscribed_at = date('Y-m-d H:i:s');
        $message_delivery->save();

        $this->redirect('/userprofile');
    }
}

