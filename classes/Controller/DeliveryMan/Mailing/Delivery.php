<?php defined('SYSPATH') or die('No direct script access.');

class Controller_DeliveryMan_Mailing_Delivery extends Controller
{
	public function action_trackRead()
    {
        if ( class_exists('Eventmanager') && defined('ON_MESSAGE_DELIVERY_STATUS') )
        {
            try
            {
                Eventmanager::callAction(ON_MESSAGE_DELIVERY_STATUS, array(
                    'message_delivery_status' => $this->request->param('message_delivery_status'),
                ), true);
            }
            catch(Exception $e)
            {
                Kohana::$log->add(Log::NOTICE, __CLASS__ . ' :ERROR :MESSAGE_DELIVERY_STATUS', array(
                    ':ERROR' => $e->getMessage(),
                    ':MESSAGE_DELIVERY_STATUS' => $this->request->param('message_delivery_status'),
                ));
            }
        }

        $response = base64_decode('iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAACXBIWXMAAAsSAAALEgHS3X78AAAAGUlEQVQokWP8//8/AymAiSTVoxpGNQwpDQBVbQMd5EZaEgAAAABJRU5ErkJggg==');
        $this->response->headers('Cache-Control', 'max-age=600, public');
        $this->response->headers('Content-Type', 'image/png');
        $this->response->headers('Content-Length', strlen($response));
        $this->response->body($response);
	}
}
