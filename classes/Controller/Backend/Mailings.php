<?php defined('SYSPATH') or die();

class Controller_Backend_Mailings extends Controller {

    /**
     * Данные в ответ клиенту при ajax запросе
     *
     * @var array
     */
    private $_result = [];

    public function before()
    {
        Controller_Backend::access();
    }

    public function action_index()
    {
        $body = View::factory('backend/mailings/index');
        $this->response->body($body);
    }

    public function action_datatable()
    {
        $columns = array(
            array
            (
                'data'       => 'id',
                'orderable'  => true,
                'searchable' => true,
            ),
            array
            (
                'data'       => 'description',
                'orderable'  => true,
                'searchable' => true,
            ),
            array
            (
                'data'       => 'template_id',
                'orderable'  => true,
                'searchable' => true,
            ),
            array
            (
                'data'      => 'created_at',
                'orderable' => true,
            ),
            array
            (
                'data'      => 'scheduled_to',
                'orderable' => true,
            ),
            array
            (
                'data'      => 'finished_at',
                'orderable' => true,
            ),

        );

        $datatable = DatatableOrm::factory('Mailing', $columns, $this->request->post());
        $this->_result = $datatable->getData();
    }

    public function action_create()
    {
        $this->action_edit();
    }

    public function action_edit()
    {
        // Обновление существующего заголовка
        if (null !== $this->request->param('_param'))
        {
            $mailing = ORM::factory('Mailing', array(
                'id' => (int) $this->request->param('_param'),
            ));
            if (!$mailing->loaded())
            {
                throw HTTP_Exception::factory(404);
            }
        }
        // Добавление нового заголовка
        else
        {
            $mailing = ORM::factory('Mailing');
        }

        if ($this->request->method() == Request::POST)
        {
            $params = Validation::factory($this->request->post())
                ->label('description', 'Назначение рассылки')
                ->label('template_id', 'Шаблон')
                ->label('recipients', 'Список адресатов')
                ->label('scheduled_to', 'Время отправки')
                ->rule('description', 'not_empty')
                ->rule('template_id', 'not_empty')
                ->rule('recipients', 'not_empty')
                ->rule('scheduled_to', 'not_empty');

            if (!$params->check())
            {
                header('HTTP/1.1 400 Bad Request');
                header('Content-Type: application/json; charset=utf-8');
                die(json_encode($params->errors('validation')));
            }

            $scheduled_to = $this->request->post('scheduled_to');
            $recipients = $this->request->post('recipients');
            if (empty($scheduled_to['date']) OR empty($scheduled_to['time'])
                OR !preg_match('/\d{4}-\d{2}-\d{2}/', $scheduled_to['date'])
                OR !preg_match('/\d{1,2}:\d{2}/', $scheduled_to['time'])
            )
            {
                header('HTTP/1.1 400 Bad Request');
                header('Content-Type: application/json; charset=utf-8');
            }
            if (empty($recipients) OR !preg_match('/[A-Za-z0-9-_]+@[A-Za-z0-9-_.]+/m', $recipients)
            )
            {
                header('HTTP/1.1 400 Bad Request');
                header('Content-Type: application/json; charset=utf-8');
                die(json_encode([
                    'recipients' => 'Список адресатов должен быть по одному адресу в строку',
                ]));
            }

            $scheduledDateTime = $scheduled_to['date'] . ' ' . $scheduled_to['time'];

            $mailing->description = $this->request->post('description');
            $mailing->template_id = $this->request->post('template_id');
            $mailing->recipients = $this->request->post('recipients');
            $mailing->scheduled_to = $scheduledDateTime;
            $mailing->only_confirmed_users = $this->request->post('only_confirmed_users');
            $mailing->save();
        }

        $messagetemplates = ORM::factory('Messagetemplate')->find_all();

        if (empty($mailing->started_at))
        {
            $view = View::factory('backend/mailings/mailing')
                ->bind('mailing', $mailing)
                ->bind('messagetemplates', $messagetemplates);
        }
        else
        {
            $data = [
                'created'      => 'Создано',
                'sent'         => 'Доставлено',
                'bounced'      => 'Отклонено',
                'error'        => 'Ошибок',
                'queue'        => 'В очереди',
                'unsubscribed' => 'Отписалось',
//                'deposited'    => 'Количество игроков, сделавших депозит в течении суток, после рассылки',
            ];

            foreach ($data as $name => $title)
            {
                $mailing_stats[ $name ]['title'] = $title;
                // динамически получаем значение константы, проверяя, существует ли она вообще
                $constexists = defined('Model_MailingDelivery::STATUS_' . strtoupper($name));
                $info['status'] = ($constexists) ? constant('Model_MailingDelivery::STATUS_' . strtoupper($name)) : null;
                // если $name соответствует условиям, то добавляем в массив значения true false
                $mailing_stats[ $name ]['no_percentage'] = ($name === 'created' || $name === 'deposited') ? true : false;
                $info['unsubscribed'] = ($name === 'unsubscribed') ? true : false;
                // получаем массив со статистикой
                $mailing_stats[ $name ]['count'] = $this->getMailingStatistic($mailing->id, $info);
            }

            //$mailing_stats['deposited']['count'] = 1; // временное значение

            $params = array();
            if (empty($mailing->finished_at))
            {
                $params['fa_icon'] = $params['alert_color'] = 'warning';
                $params['message'] = 'выполняется';
                $params['finished'] = '';
            }
            else
            {
                $params['alert_color'] = 'success';
                $params['fa_icon'] = 'thumbs-up';
                $params['message'] = 'завершена';
                $params['finished'] = 'Была завершена в:';
            }

            $view = View::factory('backend/mailings/mailing_show')
                ->bind('mailing', $mailing)
                ->bind('messagetemplates', $messagetemplates)
                ->bind('mailing_stats', $mailing_stats)
                ->bind('params', $params);
        }
        $this->response->body($view);
    }

    public function action_loadtemplate()
    {
        if (null !== $this->request->param('_param'))
        {
            $messageTemplate = Model_Messagetemplate::factory('messagetemplate', $this->request->param('_param'));
            if ($messageTemplate->loaded())
            {
                $view = View::factory('backend/messagetemplate/preview')
                    ->bind('messagetemplate', $messageTemplate)
                    ->render();
                $view = str_replace('{{URL}}', GOLDENSTAR_URL, $view);
                die($view);
            }
        }
        $view = View::factory('backend/messagetemplate/preview')->render();
        die($view);
    }

    public function after()
    {
        if (!empty($this->_result))
        {
            $responseData = json_encode($this->_result);
            $this->response->headers('Content-Type', 'application/json');
            $this->response->body($responseData);
        }
    }

    protected function trackRead(&$body, $message_delivery_id)
    {
        $message_delivery_status = strtr(rtrim(base64_encode(Encrypt::instance('deliveryman')->encode(json_encode(array(
            'message_delivery_id' => $message_delivery_id,
            'status'              => 'opened',
        )))), '='), '+/', '-_');

        $trackReadHtml = '' .
            '<center>' .
            '<table bgcolor="white">' .
            '<tbody>' .
            '<tr>' .
            '<td>' .
            '<font size="-1" color="black">' .
            '<img width="16" height="16" alt="" title="" border="0" src="http://' . Arr::get($_SERVER, 'DOMAIN', $_SERVER['HTTP_HOST']) . '/deliveryman/message/delivery/' . $message_delivery_status . '">' .
            '</font>' .
            '</td>' .
            '</tr>' .
            '</tbody>' .
            '</table>' .
            '</center>';

        // Подставляем email_id во все ссылки, кроме макросов
        $body = preg_replace('/\<\/body\>\s*\<\/html\>/s', $trackReadHtml . '$0', $body);
    }

    /**
     * @param $mailing_id
     * @param array $info
     * @return int
     */
    private function getMailingStatistic($mailing_id, array $info)
    {
        $query = ORM::factory('MailingDelivery')->where('mailing_id', '=', $mailing_id);
        if (isset($info['status']))
        {
            $query->where('status', '=', $info['status']);
        }
        if ($info['unsubscribed'])
        {
            $query->where('unsubscribed_at', '!=', 'null');
        }

        return $query->find_all()->count();
    }
}