<?php defined('SYSPATH') or die();

class Controller_Backend_Messagetemplate extends Controller {

    /**
     * Данные в ответ клиенту при ajax запросе
     *
     * @var array
     */
    private $_result = [];

    public function before()
    {
        Controller_Backend::access();
    }

    public function action_index()
    {
        $body = View::factory('backend/messagetemplate/index');
        $this->response->body($body);
    }

    public function action_datatable()
    {
        $columns = array(
            array
            (
                'data'       => 'id',
                'orderable'  => true,
                'searchable' => true,
            ),
            array
            (
                'data'       => 'name',
                'orderable'  => true,
                'searchable' => true,
            ),
            array
            (
                'data'         => 'subject',
                'orderable'    => true,
                'searchable'   => true,
                'relationship' => 'messageheader',
                'alias'        => 'subject'
            ),
            array
            (
                'data'       => 'subject',
                'orderable'  => true,
                'searchable' => true,
                'alias'      => 'old_subject',
            ),
            array
            (
                'data'      => 'updated_at',
                'orderable' => true,
            ),
            array
            (
                'data'      => 'system',
                'orderable' => true,
            ),
            array
            (
                'data'      => 'old',
                'orderable' => true,
            ),
            array
            (
                'data'      => 'enabled',
                'orderable' => true,
            ),
            array
            (
                'data'       => function (Model_Messagetemplate $row)
                {
                    return '';
                },
                'alias'      => 'body',
                'searchable' => true,
                'visible'    => false,
            ),
            array
            (
                'data'       => function (Model_Messagetemplate $row)
                {
                    return '';
                },
                'alias'      => 'bodyShort',
                'searchable' => true,
                'visible'    => false,
            ),
            array
            (
                'data'       => function (Model_Messagetemplate $row)
                {
                    return '';
                },
                'alias'      => 'bodyModal',
                'searchable' => true,
                'visible'    => false,
            ),
        );

        $datatable = DatatableOrm::factory('Messagetemplate', $columns, $this->request->post())
            ->where('name', '<>', 'no-template');

        $datatable->filter_open();
        if ( ! $this->request->post('disabled'))
        {
            $datatable->where('enabled', '=', 1);
        }
        if ( ! $this->request->post('old'))
        {
            $datatable->where('old', '=', 0);
        }
        $datatable->filter_close();

        $this->_result = $datatable->getData();
    }

    public function action_item()
    {
        // Обновление существующего шаблона сообщения
        if (null !== $this->request->param('_param'))
        {
            $messagetemplate = ORM::factory('Messagetemplate', array(
                'id' => (int)$this->request->param('_param'),
            ));
            if ( ! $messagetemplate->loaded())
            {
                throw HTTP_Exception::factory(404);
            }
        }
        // Добавление нового шаблона сообщения
        else
        {
            $messagetemplate = ORM::factory('Messagetemplate');
            $messagetemplate->enabled = 1;
        }

        if ($this->request->method() == Request::POST)
        {
            $params = Validation::factory($this->request->post())
                ->label('name', 'Название шаблоны')
                ->label('postmaster_msgtype', 'Postmaster Mgstype')
                ->label('header_id', 'Тема письма')
                ->label('body_id', 'Текст письма')
                ->label('bodyShort', 'Текст SMS и сообщения в личном кабинете')
                ->label('bodyModal', 'Текст модального диалогового окна')
                ->rule('name', 'not_empty')
                ->rule('postmaster_msgtype', 'regex', array(':value', '/^[a-zA-Z0-9_]+$/'))
                ->rule('header_id', 'digit')
                ->rule('body_id', 'digit')
                ->rule('bodyShort', 'not_empty');

            if ( ! $params->check())
            {
                header('HTTP/1.1 400 Bad Request');
                header('Content-Type: application/json; charset=utf-8');
                die(json_encode($params->errors('validation')));
            }

            $fields = array('header_id', 'postmaster_msgtype', 'body_id', 'bodyShort', 'bodyModal', 'emailDriverOverride', 'smsDriverOverride', 'old', 'enabled');
            if ( ! $messagetemplate->loaded() || ! $messagetemplate->system)
            {
                $fields[] = 'name';
            }

            $messagetemplate->old = 0;
            $messagetemplate->enabled = 0;
            foreach (array_intersect_key($this->request->post(), array_flip($fields)) as $key => $value)
            {
                $messagetemplate->{$key} = $value;
            }

            $messagetemplate->bodyModal = '' != trim($messagetemplate->bodyModal) ? $messagetemplate->bodyModal : null;
            $messagetemplate->emailDriverOverride = '' != trim($messagetemplate->emailDriverOverride) ? $messagetemplate->emailDriverOverride : null;
            $messagetemplate->smsDriverOverride = '' != trim($messagetemplate->smsDriverOverride) ? $messagetemplate->smsDriverOverride : null;

            $messagetemplate->save();

            // TODO: regenerate profile and modal messages which are not yet viewed

            return;
        }

        $messageHeaders = Model_Messageheader::factory('Messageheader')->find_all();
        $messageBodies = Model_Messagebody::factory('Messagebody')->find_all();

        $view = View::factory('backend/messagetemplate/item')
            ->bind('messagetemplate', $messagetemplate)
            ->set('emailDrivers', Arr::path($messagetemplate->table_columns(), 'emailDriverOverride.options'))
            ->set('smsDrivers', Arr::path($messagetemplate->table_columns(), 'smsDriverOverride.options'))
            ->set('messageHeaders', $messageHeaders)
            ->set('messageBodies', $messageBodies);
        $this->response->body($view);
    }

    public function action_bodyview()
    {
        if (null !== $this->request->param('_param'))
        {
            $messageBody = Model_Messagebody::factory('messagebody', $this->request->param('_param'));
            if ($messageBody->loaded())
            {
                $body = str_replace('{{URL}}', GOLDENSTAR_URL, $messageBody->body);
                die($body);
            }
            die();
        }
        die();
    }

    public function action_delete()
    {
    }


    public function action_sendEmail()
    {
        $this->sendMessage(array('=email'));
    }

    public function action_sendSms()
    {
        $this->sendMessage(array('=sms'));
    }

    public function action_sendNotificationModal()
    {
        $this->sendMessage(array('=modal'));
    }

    protected function sendMessage(array $maskDeliverySystems)
    {
        $messagetemplate = ORM::factory('Messagetemplate', array(
            'id' => (int)$this->request->param('_param'),
        ));
        if ( ! $messagetemplate->loaded())
        {
            throw HTTP_Exception::factory(404);
        }

        $gso_user_id = Auth::instance()->get_user()->gso_user_id;
        if (null === $gso_user_id)
        {
            throw HTTP_Exception::factory(400);
        }

        $params = array();
        $params['user_id'] = $gso_user_id;
        $params['maskDeliverySystems'] = $maskDeliverySystems;
        $params['messagetemplate_id'] = $messagetemplate->id;
        if ($this->request->post('emailOverride'))
        {
            $params['emailOverride'] = $this->request->post('emailOverride');
        }
        if ($this->request->post('smsOverride'))
        {
            $params['smsOverride'] = $this->request->post('smsOverride');
        }

        $response = GsoApi::instance()->sendMessage($params);

        if ('ok' != Arr::get($response, 'status'))
        {
            throw HTTP_Exception::factory(500, Arr::get($response, 'message'));
        }

        $this->_result = [];
    }

    public function after()
    {
        if ( ! empty($this->_result))
        {
            $responseData = json_encode($this->_result);
            $this->response->headers('Content-Type', 'application/json');
            $this->response->body($responseData);
        }
    }

    public function action_headers()
    {
        $body = View::factory('backend/messagetemplate/headers');
        $this->response->body($body);
    }

    public function action_headerstable()
    {
        $columns = array(
            array
            (
                'data'       => 'id',
                'orderable'  => true,
                'searchable' => true,
            ),
            array
            (
                'data'       => 'subject',
                'orderable'  => true,
                'searchable' => true,
            ),
            array
            (
                'data'      => function (Model_Messageheader $row)
                {
                    return $row->Messagetemplates->count_all();
                },
                'orderable' => true,
                'alias'     => 'used'
            ),
            array
            (
                'data'      => 'group',
                'orderable' => true,
                'searchable' => true,
            ),
            array
            (
                'data'      => 'created_at',
                'orderable' => true,
            ),
            array
            (
                'data'      => 'updated_at',
                'orderable' => true,
            ),
        );

        $datatable = DatatableOrm::factory('Messageheader', $columns, $this->request->post());

        $this->_result = $datatable->getData();
    }

    public function action_header()
    {
        // Обновление существующего заголовка
        if (null !== $this->request->param('_param'))
        {
            $messageheader = ORM::factory('Messageheader', array(
                'id' => (int)$this->request->param('_param'),
            ));
            if ( ! $messageheader->loaded())
            {
                throw HTTP_Exception::factory(404);
            }
        }
        // Добавление нового заголовка
        else
        {
            $messageheader = ORM::factory('Messageheader');
        }

        if ($this->request->method() == Request::POST)
        {
            $params = Validation::factory($this->request->post())
                ->label('subject', 'Текст заголовка')
                ->rule('subject', 'not_empty');

            if ( ! $params->check())
            {
                header('HTTP/1.1 400 Bad Request');
                header('Content-Type: application/json; charset=utf-8');
                die(json_encode($params->errors('validation')));
            }

            $messageheader->subject = $this->request->post('subject');
            $messageheader->group = $this->request->post('group');
            $messageheader->save();

            return;
        }

        $view = View::factory('backend/messagetemplate/header')
            ->bind('messageheader', $messageheader);
        $this->response->body($view);
    }

    public function action_bodies()
    {
        $body = View::factory('backend/messagetemplate/bodies');
        $this->response->body($body);
    }

    public function action_bodiestable()
    {
        $columns = array(
            array
            (
                'data'       => 'id',
                'orderable'  => true,
                'searchable' => true,
            ),
            array
            (
                'data'       => 'name',
                'orderable'  => true,
                'searchable' => true,
            ),
            array
            (
                'data'      => function (Model_Messagebody $row)
                {
                    return $row->Messagetemplates->count_all();
                },
                'orderable' => true,
                'alias'     => 'used'
            ),
            array
            (
                'data'       => 'group',
                'orderable'  => true,
                'searchable' => true,
            ),
            array
            (
                'data'      => 'created_at',
                'orderable' => true,
            ),
            array
            (
                'data'      => 'updated_at',
                'orderable' => true,
            ),

        );

        $datatable = DatatableOrm::factory('Messagebody', $columns, $this->request->post());

        $this->_result = $datatable->getData();
    }

    public function action_body()
    {
        // Обновление существующего заголовка
        if (null !== $this->request->param('_param'))
        {
            $messagebody = ORM::factory('Messagebody', array(
                'id' => (int)$this->request->param('_param'),
            ));
            if ( ! $messagebody->loaded())
            {
                throw HTTP_Exception::factory(404);
            }
        }
        // Добавление нового заголовка
        else
        {
            $messagebody = ORM::factory('Messagebody');
        }

        if ($this->request->method() == Request::POST)
        {
            $params = Validation::factory($this->request->post())
                ->label('name', 'Название шаблона')
                ->label('body', 'Текст шаблона')
                ->rule('name', 'not_empty')
                ->rule('body', 'not_empty');

            if ( ! $params->check())
            {
                header('HTTP/1.1 400 Bad Request');
                header('Content-Type: application/json; charset=utf-8');
                die(json_encode($params->errors('validation')));
            }

            $messagebody->name = $this->request->post('name');
            $messagebody->group = $this->request->post('group');
            $messagebody->body = $this->request->post('body');
            $messagebody->save();

            return;
        }

        $view = View::factory('backend/messagetemplate/body')
            ->bind('messagebody', $messagebody);
        $this->response->body($view);
    }


}