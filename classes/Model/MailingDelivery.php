<?php defined('SYSPATH') or die();

/**
 * @property $id int
 * @property $letter_id int
 * @property $mailing_id int
 * @property $recipient_id int
 * @property $recipient_email string
 * @property $created_at string
 * @property $sent_at string
 * @property $status int
 */
class Model_MailingDelivery extends ORM {

    const STATUS_QUEUE = 0;
    const STATUS_SENT = 1;
    const STATUS_BOUNCED = 2;
    const STATUS_ERROR = 3;

    protected $_db_group = 'project_db';
    protected $_table_name = 'mailings_delivery';

    protected $_belongs_to = array(
        'mailing' => array(
            'model' => 'Mailing',
            'foreign_key' => 'mailing_id',
        ),
    );

}
