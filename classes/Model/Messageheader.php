<?php defined('SYSPATH') or die();

/**
 *
 */
class Model_Messageheader extends ORM {
    protected $_db_group = 'project_db';
    protected $_table_name = 'message_headers';

    protected $_has_many = array(
        'Messagetemplates' => array(
            'model' => 'Messagetemplate',
            'foreign_key' => 'header_id',
        ),
    );

}
