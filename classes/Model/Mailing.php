<?php defined('SYSPATH') or die();

/**
 * @property $id int
 * @property $description string
 * @property $template_id int
 * @property $created_at string
 * @property $started_at string
 * @property $finished_at string
 * @property $scheduled_to string
 * @property $recipients string
 * @property $last_recipient string
 */
class Model_Mailing extends ORM {
    protected $_db_group = 'project_db';
    protected $_table_name = 'mailings';

    protected $_belongs_to = array(
        'messagetemplate' => array(
            'foreign_key' => 'template_id'
        ),
    );

}
