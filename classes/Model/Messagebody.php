<?php defined('SYSPATH') or die();

/**
 *
 */
class Model_Messagebody extends ORM {
    protected $_db_group = 'project_db';
    protected $_table_name = 'message_bodies';

    protected $_has_many = array(
        'Messagetemplates' => array(
            'model' => 'Messagetemplate',
            'foreign_key' => 'body_id',
        ),
    );
}
