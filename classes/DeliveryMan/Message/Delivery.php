<?php defined('SYSPATH') or die();

abstract class DeliveryMan_Message_Delivery {

    /**
     * @var  array  DeliveryMan_Message_Delivery instances
     */
    protected static $_instances = array();

    /**
     * @param string|null $driver
     * @return DeliveryMan
     */
    static public function instance($driver)
    {
        if (!isset(self::$_instances[ $driver ]))
        {
            $class = 'DeliveryMan_Driver_' . ucfirst($driver);

            $config = Model_Index::GSCMS_CONFIG('deliveryman.drivers.' . $driver);
            if (!isset($config['sender_name']))
            {
                $config['sender_name'] = Model_Index::GSCMS_CONFIG('deliveryman.sender_name');
            }

            self::$_instances[ $driver ] = new $class($config);
        }

        return self::$_instances[ $driver ];
    }

    /**
     * @var string
     */
    protected $_driver;

    /**
     * @var array
     */
    protected $_config;

    /**
     * @var bool
     */
    protected $_existInSpamFilter = false;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->_driver = lcfirst(substr(strrchr(get_called_class(), '_'), 1));
        $this->_config = $config;

        assert(isset($this->_config['sender_name']));
    }

    /**
     *
     */
    public function getDriver()
    {
        return $this->_driver;
    }

    /**
     * For each message there is a certain format of a message:
     * for Email - long HTML message
     * for SMS and Profile -  short message
     * for Modal - HTML-body of Bootstrap modal
     */
    public function getBodyType()
    {
        return $this->_config['body'];
    }

    /**
     * @return array
     */
    public function getSpamFilter()
    {
        return $this->_config['spamFilter'];
    }

    public function setExistInSpamFilter()
    {
        $this->_existInSpamFilter = true;
    }

    /**
     * @param $sender_name
     * @param $sender_email
     */
    public function fillSender(&$sender_name, &$sender_email)
    {
        if (empty($sender_name) && isset($this->_config['sender_name']))
        {
            $sender_name = $this->_config['sender_name'];
        }

        if (empty($sender_email))
        {
            if (isset($this->_config['sender_email']))
            {
                $sender_email = $this->_config['sender_email'];
            } else
            {
                $sender_email = 'robot@' . DeliveryMan_Message::getDomain();
            }
        }
    }

    /**
     *
     * @param Model_User $user
     * @return Model_Message_Deliver
     */
    public function getLastMessage(Model_User $user)
    {
        return ORM::factory('Message_Delivery')
            ->with('message')
            ->where('message.user_id', '=', $user->id)
            ->where('message_delivery.driver', '=', $this->_driver)
            ->where('message_delivery.delivered_at', 'IS NOT', null)
            ->order_by('message_delivery.id', 'DESC')
            ->find();
    }

    /**
     * Log message delivery
     * @param int $message_id
     * @param string $addressRecepient
     * @param string $subject
     * @param string $body
     * @param string $bodyShort
     */
    protected function createDelivery($message_id = null, $addressRecepient, $addressFrom = null)
    {
        $modelMessage_Delivery = ORM::factory('Message_Delivery');
        $modelMessage_Delivery->driver = $this->_driver;
        $modelMessage_Delivery->message_id = $message_id;
        $modelMessage_Delivery->addressFrom = $addressFrom;
        $modelMessage_Delivery->addressRecepient = $addressRecepient;
        $modelMessage_Delivery->created_at = date('Y-m-d H:i:s');
        $modelMessage_Delivery->save();

        return $modelMessage_Delivery;
    }

    protected function createMailingDelivery($message_id, $mailing_id, $recipient_id, $email)
    {
        /** @var Model_MailingDelivery $modelMailing_Delivery */
        $modelMailing_Delivery = ORM::factory('MailingDelivery');
        $modelMailing_Delivery->letter_id = $message_id;
        $modelMailing_Delivery->mailing_id = $mailing_id;
        $modelMailing_Delivery->recipient_id = $recipient_id;
        $modelMailing_Delivery->recipient_email = $email;
        $modelMailing_Delivery->created_at = date('Y-m-d H:i:s');
        $modelMailing_Delivery->save();

        return $modelMailing_Delivery;
    }

    /**
     *
     * DELETEME:
     * @param string $email_id_encrypted
     */
    public function statusFollowedLink($email_id_encrypted)
    {
        try
        {
            $email_id = Encrypt::instance('deliveryman')->decode(base64_decode(strtr($email_id_encrypted, '-_', '+/')));
            if (false === $email_id || !ctype_digit($email_id))
            {
                throw new ErrorException('Not valid email_id');
            }
        } catch (Exception $e)
        {
            return;
        }

        $modelMessage_Delivery = ORM::factory('Message_Delivery')->where('id', '=', $email_id)->find();
        if (!$modelMessage_Delivery->loaded())
        {
            return;
        }
        $modelMessage_Delivery->followed_link_at = $modelMessage_Delivery->followed_link_at ?: date('Y-m-d H:i:s');
        $modelMessage_Delivery->opened_at = $modelMessage_Delivery->opened_at ?: date('Y-m-d H:i:s');
        $modelMessage_Delivery->delivered_at = $modelMessage_Delivery->delivered_at ?: date('Y-m-d H:i:s');
        $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s');
        $modelMessage_Delivery->save();
    }

    /**
     *
     * @param string $data_enc
     */
    public function updateStatus($data_enc)
    {
        try
        {
            if (is_string($data_enc))
            {
                $data = Encrypt::instance('deliveryman')->decode(base64_decode(strtr($data_enc, '-_', '+/')));
                if (false === $data)
                {
                    throw new InvalidArgumentException;
                }

                $data = json_decode($data, true);
            } elseif (is_array($data_enc))
            {
                $data = $data_enc;
            } else
            {
                throw new InvalidArgumentException;
            }

            if (!is_array($data) || !isset($data['message_delivery_id']) || !ctype_digit($data['message_delivery_id']) || !isset($data['status']))
            {
                throw new InvalidArgumentException;
            }
        } catch (Exception $e)
        {
            return;
        }

        $time = Arr::get($data, 'time', time());

        $modelMessage_Delivery = ORM::factory('Message_Delivery', $data['message_delivery_id']);
        if (!$modelMessage_Delivery->loaded())
        {
            return;
        }

        // Update status update times
        if ('delivered' == $data['status'])
        {
            $modelMessage_Delivery->delivered_at = $modelMessage_Delivery->delivered_at ?: date('Y-m-d H:i:s', $time);
            $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s', $time);
        } elseif ('opened' == $data['status'])
        {
            $modelMessage_Delivery->opened_at = $modelMessage_Delivery->opened_at ?: date('Y-m-d H:i:s', $time);
            $modelMessage_Delivery->delivered_at = $modelMessage_Delivery->delivered_at ?: date('Y-m-d H:i:s', $time);
            $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s', $time);
        } elseif ('followed_link' == $data['status'])
        {
            $modelMessage_Delivery->followed_link_at = $modelMessage_Delivery->followed_link_at ?: date('Y-m-d H:i:s', $time);
            $modelMessage_Delivery->opened_at = $modelMessage_Delivery->opened_at ?: date('Y-m-d H:i:s', $time);
            $modelMessage_Delivery->delivered_at = $modelMessage_Delivery->delivered_at ?: date('Y-m-d H:i:s', $time);
            $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s', $time);
        } elseif ('unsubscribe' == $data['status'])
        {
            //$modelMessage_Delivery->error = 'ok_unsubscribed';
            $modelMessage_Delivery->unsubscribed_at = date('Y-m-d H:i:s', $time);
            $modelMessage_Delivery->opened_at = $modelMessage_Delivery->opened_at ?: date('Y-m-d H:i:s', $time);
            $modelMessage_Delivery->delivered_at = $modelMessage_Delivery->delivered_at ?: date('Y-m-d H:i:s', $time);
            $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s', $time);
        } elseif ('error' == $data['status'])
        {
            $modelMessage_Delivery->error = Arr::get($data, 'error', 'unknown error');
        }
        $modelMessage_Delivery->save();

        if ('opened' == $data['status'] || 'followed_link' == $data['status'] || 'unsubscribe' == $data['status'])
        {
            $user = $modelMessage_Delivery->message->user;
            $user->setEmailExists($modelMessage_Delivery->addressRecepient, true);
            DeliveryMan_Email::instance()->setEmailExists($modelMessage_Delivery->addressRecepient, true);
        }

        return $modelMessage_Delivery;
    }

    public function getBalance()
    {
        return null;
    }

    public function checkMessageDelivery(Database_Result $modelsMessage_Delivery)
    {
    }

    abstract public function sendMessage($message_id = null, $addressRecepient, $subject, $body, $sender_name = null, $sender_email = null, $recipient = null, $mailing_id = null, $only_confirmed_users = null);

    abstract public function createMessageTemplate($list, $title, $body, $date = null, $sender_name = null, $sender_email = null);

    abstract public function createCampaign($message_id, $start_time);

    abstract public function getLists($list_name = null);

    abstract public function createList($title);

    abstract public function editList($list, $new_title);

    abstract public function deleteList($list);

    abstract public function subscribeEmail($lists, $email, $fields = array());

    abstract public function unsubscribeEmail($lists, $email, $is_remove = true);

    abstract public function getFields($name = null);

    abstract public function createField($name, $type, $is_visible = 1);

    abstract public function updateField($field, $name = null, $type = null, $is_visible = null);

    abstract public function deleteField($field);
}
