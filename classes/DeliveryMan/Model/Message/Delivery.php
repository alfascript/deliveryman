<?php defined('SYSPATH') or die();

/**
 * 
 */
class DeliveryMan_Model_Message_Delivery extends ORM {

    protected $_db_group = 'project_db';
    protected $_table_name = 'message_delivery';
    
    protected $_belongs_to = array(
        'message' => array(
            'model' => 'Message',
        ),
    );

    public function getCreatedAtFormatted($format = null)
    {
        $datetime = new DateTime($this->created_at);
        return $datetime->format(null !== $format ? $format : 'd.m.Y H:i');
    }

    public function getTsSentFormatted($format = null)
    {
        $datetime = new DateTime($this->created_at);
        return $datetime->format(null !== $format ? $format : 'd.m.Y H:i');
    }

    public function getSentAtFormatted($format = null)
    {
        if ( null === $this->sent_at )
        {
            return '';
        }
        
        $datetime = new DateTime($this->sent_at);
        return $datetime->format(null !== $format ? $format : 'd.m.Y H:i');
    }

    public function getDeliveredAtFormatted($format = null)
    {
        if ( null === $this->delivered_at )
        {
            return '';
        }
        
        $datetime = new DateTime($this->delivered_at);
        return $datetime->format(null !== $format ? $format : 'd.m.Y H:i');
    }
    
    public function getOpenedAtFormatted($format = null)
    {
        if ( null === $this->opened_at )
        {
            return '';
        }
        
        $datetime = new DateTime($this->opened_at);
        return $datetime->format(null !== $format ? $format : 'd.m.Y H:i');
    }

    public function getFollowedLinkAtFormatted($format = null)
    {
        if ( null === $this->followed_link_at )
        {
            return '';
        }
        
        $datetime = new DateTime($this->followed_link_at);
        return $datetime->format(null !== $format ? $format : 'd.m.Y H:i');
    }

    public function getUnsubscribedAtFormatted($format = null)
    {
        if ( null === $this->unsubscribed_at )
        {
            return '';
        }
        
        $datetime = new DateTime($this->unsubscribed_at);
        return $datetime->format(null !== $format ? $format : 'd.m.Y H:i');
    }
    
}
