<?php defined('SYSPATH') or die();

/**
 * 
 */
class DeliveryMan_Model_Message extends ORM {

    protected $_db_group = 'project_db';
    protected $_belongs_to = array(
        'user' => array(
            'model' => 'User',
        ),
        'template' => array(
            'model' => 'Messagetemplate',
            'foreign_key' => 'messagetemplate_id',
        ),
    );

    protected $_has_many = array(
        'delivery' => array(
            'model' => 'Message_Delivery',
        ),
        'deliveryBack' => array(
            'model' => 'Message_Delivery_Back',
        ),
        'delivery_back' => array(
            'model' => 'Message_Delivery_Back',
        ),
    );
    
    /**
     * @return array
     */
    public function filters()
    {
        return array(
            'params' => array(
                array('json_encode'),
            ),
        );
    }
    
    public function getParam($key = null)
    {
        $params = json_decode($this->params, TRUE);
        
        if ( null === $key )
        {
            return $params;
        }
        
        return Arr::get($params, $key);
    }
    
    public function getCreatedAtFormatted($format = null)
    {
        $datetime = new DateTime($this->created_at);
        return $datetime->format(null !== $format ? $format : 'd.m.Y H:i');
    }

    public function getExpireAtFormatted($format = null)
    {
        if ( null === $this->expire_at )
        {
            return '';
        }
        
        $datetime = new DateTime($this->expire_at);
        return $datetime->format(null !== $format ? $format : 'd.m.Y H:i');
    }
    
}
