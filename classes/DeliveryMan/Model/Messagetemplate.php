<?php defined('SYSPATH') or die();

/**
 * Message templates model
 */
class DeliveryMan_Model_Messagetemplate extends ORM {

    protected $_db_group = 'project_db';
    protected $_has_many = array(
        'messages' => array(
            'model' => 'Message',
        ),
        'newtrigger_actions' => array(
            'model' => 'Newtrigger_Action',
            'foreign_key' => 'messagetemplate_id',
        ),
    );

    protected $_belongs_to = array(
        'messageheader' => array(
            'foreign_key' => 'header_id'
        ),
        'messagebody' => array(
            'foreign_key' => 'body_id'
        ),
    );

    public function getNameFormatted()
    {
        return '[' . $this->name . '] ' . $this->subject;
    }

}
