<?php defined('SYSPATH') or die();

class DeliveryMan_Email {

	/**
	 * @var DeliveryMan_Email
	 */
	protected static $_instance;
    
    /**
     * @return DeliveryMan_Email
     */
    static public function instance()
    {
		if ( ! isset(self::$_instance) )
		{
            self::$_instance = new self();
        }
        
		return self::$_instance;
    }
    
    /**
     * 
     */
    public function __construct()
    {
    }
    
    public function setEmailExists($email, $exists)
    {
        $modelEmail = ORM::factory('Email', $email);
        $modelEmail->email = $email;
        $modelEmail->exists = (int)$exists;
        $modelEmail->save();
    }
    
    public function isEmailExists($email)
    {
        $modelEmail = ORM::factory('Email', $email);
        
        // By default we suppose that email exists
        if ( ! $modelEmail->loaded() )
        {
            return true;
        }
        
        return (bool)$modelEmail->exists;
    }
    
}