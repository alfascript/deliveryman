<?php defined('SYSPATH') or die();

class DeliveryMan_Driver_SmsNull extends DeliveryMan_Message_Delivery {

    use DeliveryMan_Driver_Trait_Sms;
    
    protected $_clientSmsc;
    
    public function __construct(array $config)
    {
        parent::__construct($config);
    }
    
	public function sendMessage($message_id = null, $addressRecepient, $subject, $body, $sender_name = null, $sender_email = null, $recipient = null, $mailing_id = null, $only_confirmed_users = null)
	{
        $phone = $addressRecepient;
        $body = str_replace('\n', "\n", $body);
        
        // Default sender name and email
        if ( empty($sender_name) )
        {
            $this->fillSender($sender_name, $sender_email);
        }
        
        Kohana::$log->add(Log::INFO, 'SMS :$addressRecepient' . "\r\n" . ':body', array(
            ':$addressRecepient' => $addressRecepient,
            ':body' => $body,
        ));
        
        $modelMessage_Delivery = $this->createDelivery($message_id, $phone);
        $modelMessage_Delivery->external_id = $modelMessage_Delivery->id;
        $modelMessage_Delivery->sent_at = date('Y-m-d H:i:s');
        $modelMessage_Delivery->save();
    }

    public function createMessageTemplate($list, $title, $body, $date=null,$sender_name=null, $sender_email=null){}
    public function createCampaign($message_id, $start_time){}
    public function getLists($list=null){}
    public function createList($title){}
    public function editList($list, $new_title){}
    public function deleteList($list){}
    public function subscribeEmail($lists, $email, $fields=array()){}
    public function unsubscribeEmail($lists, $email, $is_remove=true){}
    public function getFields($name=null){}
    public function createField($name, $type, $is_visible=1){}
    public function updateField($field, $name=null, $type=null, $is_visible=null){}
    public function deleteField($field){}

}