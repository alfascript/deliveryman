<?php defined('SYSPATH') or die();

/**
 * Client for doing requests to Unisender API
 * 
 */
class DeliveryMan_Driver_Client_Unisender {

    protected $_config;
    
    /**
     * 
     */
    public function __construct(array $config)
    {
        $this->_config = $config;
	}

	/**
	 * @param string $name
	 * @param array $arguments
	 * @return string
	 */
	public function __call($name, $arguments)
    {
		if ( ! is_array($arguments) || empty($arguments) )
        {
			$params = array();
		}
        else
        {
			$params = $arguments[0];
		}

		return $this->callMethod($name, $params);
	}

	/**
	 * @param array $params
	 * @return string
	 */
	public function subscribe($params)
    {
		$params = (array)$params;

		if (empty($params['request_ip']))
        {
			$params['request_ip'] = $this->getClientIp();
		}

		return $this->callMethod('subscribe', $params);
	}

	/**
	 * @return string
	 */
	protected function getClientIp()
    {
		$result = '';

		if (!empty($_SERVER["REMOTE_ADDR"])) {
			$result = $_SERVER["REMOTE_ADDR"];
		} else if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))  {
			$result = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} else if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
			$result = $_SERVER["HTTP_CLIENT_IP"];
		}

		if (preg_match('/([0-9]|[0-9][0-9]|[01][0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[0-9][0-9]|[01][0-9][0-9]|2[0-4][0-9]|25[0-5])){3}/', $result, $match)) {
			return $match[0];
		}

		return $result;
	}

	/**
	 * @param string $nameMethod
	 * @param array $params
	 * @return array
	 */
	protected function callMethod($nameMethod, $params = array())
    {
		$url = $nameMethod . '?format=json';
        if ( Arr::get($this->_config, 'test_mode') )
        {
            $url = $url . '&test_mode=1';
        }

		if (isset($params['body']) && strlen($params['body']) > 128 * 1024)
        {
			$url .= '&api_key=' . $this->_config['keyApi']. '&request_compression=bzip2';
			$content = bzcompress(http_build_query($params));
		}
        else
        {
			$params = array_merge((array)$params, array('api_key' => $this->_config['keyApi']));
			$content = http_build_query($params);
		}

        $request = Request::factory($this->getApiHost())
            ->method(Request::POST)
     		->headers('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
            ->body($content);

		if ( isset($this->_config['timeout']) )
        {
            $request->client()->options(CURLOPT_TIMEOUT, $this->_config['timeout']);
		}
        
		$countRetry = 0;
		do
        {
			$request->uri($this->getApiHost($countRetry) . $url); 
            try
            {
                $response = $request->execute();
            }
            catch(Request_Exception $e)
            {
                if ( $countRetry == $this->_config['countRetry'] - 1 )
                {
                    throw $e;
                }
                
                $response = Response::factory(array('_status' => 0));
            }
			$countRetry++;
		}
        while (200 !== $response->status() && $countRetry < $this->_config['countRetry']);

		$response = $this->_compileResponse($response->body());
        
        if ( isset($response['error']) || isset($response[0]['errors']) )
        {
            throw new RuntimeException('Error ' . $nameMethod . ' through Unisender: ' . (isset($response['code']) ? '[' . $response['code'] . '] ' : '') . $response['error']);
        }
        
        return $response;
	}

    /**
     * @param $str
     * @return array
     */
    protected function _compileResponse($str)
    {
        return json_decode($str, true);
    }

    
	/**
	 * @param int $countRetry
	 * @return string
	 */
	protected function getApiHost($countRetry = 0)
    {
        $urlsApi = $this->_config['urlsApi'];
        return $urlsApi[$countRetry % count($urlsApi)];
	}
}
