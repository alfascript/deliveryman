<?php defined('SYSPATH') or die();

/**
 * Client for doing requests to IBA Telecom API
 * 
 */
class DeliveryMan_Driver_Client_Ibatelecom {

    protected $_config;
    
    /**
     * 
     */
    public function __construct(array $config)
    {
        $this->_config = $config;
	}

	/**
	 * @param string $name
	 * @param array $arguments
	 * @return string
	 */
	public function __call($name, $arguments)
    {
		if ( ! is_array($arguments) || empty($arguments) )
        {
			$params = array();
		}
        else
        {
			$params = $arguments[0];
		}

		return $this->callMethod($name, $params);
	}

	/**
	 * @param array $params
	 * @return string
	 */
	public function subscribe($params)
    {
		$params = (array)$params;

		if (empty($params['request_ip']))
        {
			$params['request_ip'] = $this->getClientIp();
		}

		return $this->callMethod('subscribe', $params);
	}

	/**
	 * @return string
	 */
	protected function getClientIp()
    {
		$result = '';

		if (!empty($_SERVER["REMOTE_ADDR"])) {
			$result = $_SERVER["REMOTE_ADDR"];
		} else if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))  {
			$result = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} else if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
			$result = $_SERVER["HTTP_CLIENT_IP"];
		}

		if (preg_match('/([0-9]|[0-9][0-9]|[01][0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[0-9][0-9]|[01][0-9][0-9]|2[0-4][0-9]|25[0-5])){3}/', $result, $match)) {
			return $match[0];
		}

		return $result;
	}

	/**
	 * @param string $methodName
	 * @param array $params
	 * @return array
	 */
	protected function callMethod($methodName, $params = array())
    {
        $params['security'] = array(
            'login' => array('@' => array('value' => $this->_config['login'])),
            'password' => array('@' => array('value' => $this->_config['password'])),
        );
        
        $request_xml = '<?xml version="1.0" encoding="utf-8"?>' . "\r\n" . $this->_compileRequest(array(
            'request' => $params,
        ));
        
        $request = Request::factory($this->getApiHost())
            ->method(Request::POST)
     		->headers('Content-Type', 'text/xml; charset=utf-8')
            ->body($request_xml);

		if ( isset($this->_config['timeout']) )
        {
            $request->client()->options(CURLOPT_TIMEOUT, $this->_config['timeout']);
		}
        $request->client()->options(CURLOPT_SSL_VERIFYPEER, false);
        
		$countRetry = 0;
        $maxCountRetry = max(1, (int)$this->_config['countRetry']);
		while($countRetry < $maxCountRetry)
        {
            $uri = str_replace('{{METHOD}}', $methodName, $this->getApiHost($countRetry));
			$request->uri($uri);
            
            try
            {
                $response = $request->execute();
            }
            catch(Request_Exception $e)
            {
                if ( $countRetry == $this->_config['countRetry'] - 1 )
                {
                    throw $e;
                }
                
                $response = Response::factory(array('_status' => 0));
            }
            
            if ( 200 == $response->status() && preg_match('/^\s*\<\?xml/', $response->body()) )
            {
                break;
            }
            
            $countRetry++;
		}

        try
        {
    		$response = $this->_compileResponse($response->body());
        }
        catch(Exception $e)
        {
            // TODO: temporary, remove after fix
            if ( 'state' == $methodName && false !== strpos($response->body(), '<title>400 Bad Request</title>') && false !== strpos($response->body(), '<address>Apache/2.2.16 (Debian) Server at goldenstar.ws Port 443</address>') )
            {
                Kohana::$log->add(Log::NOTICE, 'DELIVERYMAN/DRIVER/CLIENT/IBATELECOM/UNRESOLVED_BUG');
                $response = new stdClass();
                $response->state = array();
                
                return $response;
            }
            
            $error = $e->getMessage() . ', request: ' . $request_xml . ', response: ' . $response->body();
            $response = new stdClass();
            $response->error = $error;
        }
        
        if ( isset($response->error) || ('index' == $methodName && 'send' != $response->information) )
        {
            throw new RuntimeException('Error ' . $methodName . ' through Ibatelecom: ' . (isset($response->error) ? $response->error : (isset($response->information) ? $response->information : 'unknown error')));
        }
        
        return $response;
	}
    
    /**
     * 
     * @param array $request
     * @return string Request XML
     */
    protected function _compileRequest(array $request, &$xml = '')
    {
        foreach ($request as $key => $subArray)
        {
            if ( '@' == $key )
            {
                continue;
            }
            
            $attrs = '';
            if ( isset($subArray['@']) )
            {
                foreach ($subArray['@'] as $attr => $value)
                {
                    $attrs .= " $attr='" . str_replace('\'', '\\\'', $value) . "'";
                }
            }
            
            if ( empty($subArray) )
            {
                $xml .= "<$key$attrs />";
            }
            else
            {
                $xml .= "<$key$attrs>";
                
                if (is_scalar($subArray))
                {
                    $xml .= $subArray;
                }
                else
                {
                    $this->_compileRequest($subArray, $xml);
                }
                
                $xml .= "</$key>";
            }
        }
        
        return $xml;
    }    

    /**
     * @param $str
     * @return array
     */
    protected function _compileResponse($str)
    {
        return simplexml_load_string($str);
    }
    
	/**
	 * @param int $countRetry
	 * @return string
	 */
	protected function getApiHost($countRetry = 0)
    {
        //$urlsApi = $this->_config['urlsApi'];
        //return $urlsApi[$countRetry % count($urlsApi)];
        
        return 'https://lk.ibatele.com/xml/{{METHOD}}.php';
	}
}
