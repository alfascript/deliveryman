<?php defined('SYSPATH') or die();

/**
 * Client for doing requests to SMSC API
 * 
 */
class DeliveryMan_Driver_Client_Smsc {

    protected $_config;
    
    /**
     * 
     */
    public function __construct(array $config)
    {
        $this->_config = $config;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return string
     */
    public function __call($name, $arguments)
    {
        if ( ! is_array($arguments) || empty($arguments) )
        {
            $params = array();
        }
        else
        {
            $params = $arguments[0];
        }

        return $this->sendCmd($name, $params);
    }
    
    // ВНУТРЕННИЕ ФУНКЦИИ
    // Функция вызова запроса. Формирует URL и делает 3 попытки чтения
    private function sendCmd($cmd, $params = array())
    {
        if ( 'send' === $cmd )
        {
            $format = 0;
            static $formats = array(
                1 => array('flash => 1'),
                2 => array('push' => '1'),
                3 => array('hlr' => '1'),
                4 => array('bin' => '1'),
                5 => array('bin' => '2'),
            );

            $params['cost'] = 3;
            $params['translit'] = 0;
            if ( $format > 0 )
            {
                $params = $params + $formats[$format];
            }
            $params['charset'] = $this->_config['charset'];
        }
        elseif ( 'status' === $cmd )
        {
            $params['charset'] = $this->_config['charset'];
        }
        
        $params['login'] = $this->_config['login'];
        $params['psw'] = $this->_config['password'];
        $params['fmt'] = 1;
        
        $url = ($this->_config['https'] ? 'https' : 'http').'://smsc.ru/sys/'.$cmd.'.php?'.http_build_query($params);

        $m = $this->callApi($url);

        if ( 'balance' === $cmd )
        {
            $m = explode(",", $m);
            // $m = (balance) или (0, -error)

            if ( $this->_config['debug'] )
            {
                if ( ! isset($m[1]) )
                {
                    Kohana::$log->add(Log::INFO, 'Сумма на счете: :balance руб.', array(
                        ':balance' => $m[0], 
                    ));
                }
                else
                {
                    Kohana::$log->add(Log::ERROR, 'Ошибка № :errno', array(
                        ':errno' => -$m[1], 
                    ));
                }
            }
            
            if ( isset($m[1]) )
            {
                throw new Exception('Ошибка получения баланса SMSC, код ошибки: ' . (string)(-$m[1]));
            }

            return $m[0];
        }
        elseif ( 'send' === $cmd )
        {
            $m = explode(",", $m);
            // $m = (id, cnt, cost, balance) или (id, -error)

            if ( $m[1] == -9 )
            {
                ignore_user_abort(true);
                sleep(60);
                $m = $this->callApi($url);
            }

            if ( $this->_config['debug'] )
            {
                if ($m[1] > 0)
                {
                    Kohana::$log->add(Log::INFO, 'SMS-сообщение через SMSC успешно отправлено. ID: :id, всего SMS: :count, стоимость: :cost руб., баланс: :balance руб.', array(
                        ':id' => $m[0],
                        ':count' => $m[1],
                        ':cost' => $m[2],
                        ':balance' => $m[3],
                    ));
                }
                else
                {
                    Kohana::$log->add(Log::DEBUG, 'Ошибка отправки SMS-сообщения через SMSC, код ошибки: :errno :id', array(
                        ':id' => $m[0] ? ', ID: '.$m[0] : '',
                        ':errno' => (string)(-$m[1]),
                    ));
                }
            }
        
            if ( $m[1] <= 0 )
            {
                $mapCodeToError = array(
                    1 => 'Ошибка в параметрах.',
                    2 => 'Неверный логин или пароль.',
                    3 => 'Недостаточно средств на счете Клиента.',
                    4 => 'IP-адрес временно заблокирован из-за частых ошибок в запросах.',
                    5 => 'Неверный формат даты.',
                    6 => 'Сообщение запрещено (по тексту или по имени отправителя).',
                    7 => 'Неверный формат номера телефона.',
                    8 => 'Сообщение на указанный номер не может быть доставлено.',
                    9 => 'Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты.',
                );
                throw new Kohana_Exception('Ошибка отправки SMS-сообщения через SMSC: :error', array(
                    ':error' => Arr::get($mapCodeToError, -$m[1], 'Неизвестная ошибка'),
                ));
            }
        }
        elseif ( 'status' === $cmd )
        {
            return $m;
        }
        else
        {
            throw new Exception('$cmd=' . $cmd);
        }
    }

    private function callApi($url)
    {
        $i = 0;
        do
        {
            if ($i)
            {
                sleep(2);
            }

            $ret = $this->readUrl($url);
        }
        while ( $ret == "" && ++$i < $this->_config['countRetry'] );

        if ($ret == "")
        {
            throw new Kohana_Exception('Ошибка чтения адреса: :url', array(
                ':url' => $url,
            ));
        }

        return $ret;
    }

    // Функция чтения URL. Для работы должно быть доступно:
    // curl или fsockopen (только http) или включена опция allow_url_fopen для file_get_contents

    private function readUrl($url)
    {
        $ret = "";
        $post = $this->_config['post'] || strlen($url) > 2000;

        if (function_exists("curl_init"))
        {
            $c = curl_init();

            if ($post)
            {
                list($url, $post) = explode('?', $url, 2);
                curl_setopt($c, CURLOPT_POST, true);
                curl_setopt($c, CURLOPT_POSTFIELDS, $post);
            }

            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 10);
            if ( isset($this->_config['timeout']) )
            {
                if ( ini_get('max_execution_time') > 0 )
                {
                    ini_set('max_execution_time', ini_get('max_execution_time') + 10 + $this->_config['timeout']);
                }
                curl_setopt($c, CURLOPT_TIMEOUT, $this->_config['timeout']);
            }
            if ( isset($this->_config['proxy']) )
            {
                curl_setopt($c, CURLOPT_PROXY, $this->_config['proxy']);
            }
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

            $ret = curl_exec($c);
            curl_close($c);
        }
        elseif ( ! $this->_config['https'] && function_exists("fsockopen"))
        {
            $m = parse_url($url);

            $fp = fsockopen($m["host"], 80, $errno, $errstr, 10);

            if ($fp)
            {
                fwrite($fp, ($post ? "POST $m[path]" : "GET $m[path]?$m[query]")." HTTP/1.1\r\nHost: smsc.ru\r\nUser-Agent: PHP".($post ? "\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: ".strlen($m['query']) : "")."\r\nConnection: Close\r\n\r\n".($post ? $m['query'] : ""));

                while (!feof($fp))
                $ret = fgets($fp, 100);

                fclose($fp);
            }
        }
        else
        {
            $ret = file_get_contents($url);
        }

        return $ret;
    }
    
    
}
