<?php defined('SYSPATH') or die();

/**
 * 
 */
trait DeliveryMan_Driver_Trait_Email {
    
    public function getAddressRecepient(Model_User $user, array $addressRecepientOverride)
    {
        return Arr::get($addressRecepientOverride, 'email', $user->profile->email);
    }
    
    protected function checkEmail($email)
    {
        // Validation of emails
        if ( ! Valid::email($email) )
        {
            throw new DeliveryMan_Exception_InvalidEmailException;
        }
        
        if ( ! DeliveryMan_Email::instance()->isEmailExists($email) )
        {
            throw new DeliveryMan_Exception_EmailBoxNotExistsException;
        }
    }
    
    /**
     * @TODO:
     * 
     * @param string $body
     * @param Model_User $user
     * @return string
     */
    protected function unsubscribe(&$body, Model_Message_Delivery $message_delivery)
    {
        $unsubscribeUrl = '';
        
        if ( null !== $message_delivery->message_id && false !== strpos($body, '{{UnsubscribeUrl}}') )
        {
            $message_delivery_id_enc = Encrypt::instance('deliveryman')->encode($message_delivery->id);
            $unsubscribeUrl = 'http://' . Arr::get($_SERVER, 'DOMAIN', $_SERVER['HTTP_HOST']) . '/deliveryman/unsubscribe/' . strtr(rtrim(base64_encode($message_delivery_id_enc), '='), '+/', '-_');
            $body = str_replace('{{UnsubscribeUrl}}', $unsubscribeUrl, $body);
        }

        return $unsubscribeUrl;
    }

    /**
     * 
     * 
     * @param string $body
     * @param Model_User $user
     * @return string
     */
    protected function trackRead(&$body, $message_delivery_id)
    {
        $message_delivery_status = strtr(rtrim(base64_encode(Encrypt::instance('deliveryman')->encode(json_encode(array(
            'message_delivery_id' => $message_delivery_id,
            'status' => 'opened',
        )))), '='), '+/', '-_');
        
        $trackReadHtml = '' .
        '<center>' .
            '<table bgcolor="white">' .
                '<tbody>' .
                    '<tr>' .
                        '<td>' .
                            '<font size="-1" color="black">' .
                                '<img width="16" height="16" alt="" title="" border="0" src="http://' . Arr::get($_SERVER, 'DOMAIN', $_SERVER['HTTP_HOST']) . '/deliveryman/message/delivery/' . $message_delivery_status . '">' .
                            '</font>' .
                        '</td>' .
                    '</tr>' .
                '</tbody>' .
            '</table>' .
        '</center>';

        // Подставляем email_id во все ссылки, кроме макросов
        $body = preg_replace('/\<\/body\>\s*\<\/html\>/s', $trackReadHtml . '$0', $body);
    }
    
    /**
     * 
     * 
     * @param string $body
     * @param Model_User $user
     * @return string
     */
    protected function followedLink(&$body, $message_delivery_id)
    {
        $message_delivery_status = strtr(rtrim(base64_encode(Encrypt::instance('deliveryman')->encode(json_encode(array(
            'message_delivery_id' => $message_delivery_id,
            'status' => 'followed_link',
        )))), '='), '+/', '-_');
        
        // Подставляем email_id во все ссылки, кроме макросов
        $body = preg_replace_callback('/<a(.*)href=([\'"])([^\'"]*)([\'"].*)>/', function($matches) use($message_delivery_status)
        {
            $href = $matches[3];
            
            // Если это {{UnsubscribeUrl}}, {{WebLetterUrl}} или любой другой макрос, то пропускаем ссылку
            if ( preg_match('/^\s*{{[^}]+}}\s*$/', $href) )
            {
                return $matches[0];
            }
            
            // Добавляем ?email_id= в ссылку
            $url_components = parse_url($href);
            parse_str(Arr::get($url_components, 'query'), $query);
            $query['mds'] = $message_delivery_status;
            $url_components['query'] = http_build_query($query);
            $href = http_build_url($url_components);
            
            return '<a' . $matches[1] . 'href=' . $matches[2] . $href . $matches[4] . '>';
        }, $body);
    }
    
}
