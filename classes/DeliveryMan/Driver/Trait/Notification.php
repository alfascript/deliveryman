<?php defined('SYSPATH') or die();

trait DeliveryMan_Driver_Trait_Notification {

    public function getAddressRecepient(Model_User $user, array $addressRecepientOverride)
    {
        return Arr::get($addressRecepientOverride, 'user_id', $user->id);
    }
    
}