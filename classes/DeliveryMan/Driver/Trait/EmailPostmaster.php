<?php defined('SYSPATH') or die();

/**
 * 
 */
trait DeliveryMan_Driver_Trait_EmailPostmaster {

    protected $_postmasterMsgtype;
    
    public function setPostmasterMsgtype($postmasterMsgtype)
    {
        $this->_postmasterMsgtype = ! empty($postmasterMsgtype) ? $postmasterMsgtype : null;
        
        return $this;
    }
    
    public function getPostmasterMsgtype()
    {
        return $this->_postmasterMsgtype;
    }
    
}
