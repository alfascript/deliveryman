<?php defined('SYSPATH') or die();

trait DeliveryMan_Driver_Trait_Sms {

    public function getAddressRecepient(Model_User $user, array $addressRecepientOverride)
    {
        return Arr::get($addressRecepientOverride, 'sms', $user->profile->mobile);
    }
    
}