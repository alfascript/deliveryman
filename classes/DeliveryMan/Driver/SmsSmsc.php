<?php defined('SYSPATH') or die();

class DeliveryMan_Driver_SmsSmsc extends DeliveryMan_Message_Delivery {

    use DeliveryMan_Driver_Trait_Sms;
    
    protected $_clientSmsc;
    
    public function __construct(array $config)
    {
        parent::__construct($config);
        
        $this->_clientSmsc = new DeliveryMan_Driver_Client_Smsc(array_intersect_key($config, array(
            'login' => true,
            'password' => true,
            'post' => true,
            'https' => true,
            'charset' => true,
            'debug' => true,
            'timeout' => true,
            'proxy' => true,
            'countRetry' => true,
        )));
    }
    
    // Функция получения баланса
    //
    // без параметров
    //
    // возвращает баланс в виде строки или false в случае ошибки
    public function getBalance()
    {
        return $this->_clientSmsc->balance();
    }
    
    // Функция отправки SMS
    //
    // обязательные параметры:
    //
    // $phones - список телефонов через запятую или точку с запятой
    // $message - отправляемое сообщение
    //
    // необязательные параметры:
    //
    // $translit - переводить или нет в транслит (1,2 или 0)
    // $time - необходимое время доставки в виде строки (DDMMYYhhmm, h1-h2, 0ts, +m)
    // $id - идентификатор сообщения. Представляет собой 32-битное число в диапазоне от 1 до 2147483647.
    // $format - формат сообщения (0 - обычное sms, 1 - flash-sms, 2 - wap-push, 3 - hlr, 4 - bin, 5 - bin-hex)
    // $sender - имя отправителя (Sender ID)
    // $query - строка дополнительных параметров, добавляемая в URL-запрос ("valid=01:00&maxsms=3&tz=2")
    //
    // возвращает массив (<id>, <количество sms>, <стоимость>, <баланс>) в случае успешной отправки
    // либо массив (<id>, -<код ошибки>) в случае ошибки
    //public function sendMessage($phones, $message, $translit = 0, $time = 0, $id = 0, $format = 0, $sender = false, $query = "")
    public function sendMessage($message_id = null, $addressRecepient, $subject, $body, $sender_name = null, $sender_email = null, $recipient = null, $mailing_id = null, $only_confirmed_users = null)
    {
        $phone = $addressRecepient;
        $body = str_replace('\n', "\n", $body);

        $modelMessage_Delivery = $this->createDelivery($message_id, $phone);

        // Default sender name and email
        if ( empty($sender_name) )
        {
            $sender_email = '123';
            $this->fillSender($sender_name, $sender_email);
        }
        
        // Параметры
        $params['phones'] = $phone;
        $params['mes'] = $body;
        $modelMessage_Delivery->external_id = $params['id'] = $modelMessage_Delivery->id;
        if ( ! empty($sender_name) )
        {
            $params['sender'] = $sender_name;
        }
        
        try
        {
            $this->_clientSmsc->send($params);
        }
        catch(Exception $e)
        {
            $modelMessage_Delivery->error = $e->getMessage();
            $modelMessage_Delivery->save();
            throw $e;
        }

        $modelMessage_Delivery->sent_at = date('Y-m-d H:i:s');
        $modelMessage_Delivery->save();
    }
    
    /**
     * 
     * @param Database_Result $modelsMessage_Delivery
     */
    public function checkMessageDelivery(Database_Result $modelsMessage_Delivery)
    {
        $phones = array();
        $ids = array();
        foreach($modelsMessage_Delivery as $modelMessage_Delivery)
        {
            $phones[] = $modelMessage_Delivery->addressRecepient;
            $ids[] = $modelMessage_Delivery->external_id;
        }
        
        $params = array();
        $params['phone'] = implode(',', $phones);
        $params['id'] = implode(',', $ids);
        // При fmt=1 и all=0 при некоторых статусах не всегда приходит идентифкатор сообщения, поэтому используем all=1
        $params['all'] = 1;
        $result = $this->_clientSmsc->status($params);
        
        $result = explode("\n", $result);
        
        foreach($result as $i => $status)
        {
            $status = $this->parseStatus($status, (bool)Arr::get($params, 'all'), $ids[$i]);

            $modelMessage_Delivery = ORM::factory('Message_Delivery')
                ->where('driver', '=', $this->getDriver())
                ->where('external_id', '=', $status['external_id'])
                ->find();
            if ( ! $modelMessage_Delivery->loaded() )
            {
                Kohana::$log->add(Log::DEBUG, 'DELIVERYMAN/CRON/STATUSMESSAGEDELIVERY/DEBUG EXTERNAL_ID IS WRONG :STATUS', array(
                    ':STATUS' => print_r($status, true),
                ));
                continue;
            }
            
            if ( -3 == $status['status'] || -1 == $status['status'] )
            {
                $modelMessage_Delivery->sent_at = null;
            }
            elseif ( 0 == $status['status'] )
            {
                $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s', $status['last_timestamp']);
            }
            elseif ( 1 == $status['status'] )
            {
                $modelMessage_Delivery->delivered_at = date('Y-m-d H:i:s', $status['last_timestamp']);
                $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s', Arr::get($status, 'send_timestamp'));
            }
            else
            {
                $codeToMessage = array(
                    '-2' =>	'Неверный логин или пароль.',
                    '-4' =>	'IP-адрес временно заблокирован.',
                    '-5' =>	'Ошибка удаления сообщения.',
                    '-9' =>	'Попытка отправки более пяти запросов на получение статуса одного и того же сообщения в течение минуты.',
                    '-3' =>	'Сообщение не найдено	Возникает при множественном запросе статусов, если для указанного номера телефона и ID сообщение не найдено.',
                    '-1' =>	'Ожидает отправки	Если при отправке сообщения было задано время получения абонентом, то до этого времени сообщение будет находиться в данном статусе, в других случаях сообщение в этом статусе находится непродолжительное время перед отправкой на SMS-центр.',
                    '0' =>	'Передано оператору	Сообщение было передано на SMS-центр оператора для доставки.',
                    '1' =>	'Доставлено	Сообщение было успешно доставлено абоненту.',
                    '3' =>	'Просрочено	Возникает, если время "жизни" сообщения истекло, а оно так и не было доставлено получателю, например, если абонент не был доступен в течение определенного времени или в его телефоне был переполнен буфер сообщений.',
                    '20' =>	'Невозможно доставить	Попытка доставить сообщение закончилась неудачно, это может быть вызвано разными причинами, например, абонент заблокирован, не существует, находится в роуминге без поддержки обмена SMS, или на его телефоне не поддерживается прием SMS-сообщений.',
                    '22' =>	'Неверный номер	Неправильный формат номера телефона.',
                    '23' =>	'Запрещено	Возникает при срабатывании ограничений на отправку дублей, на частые сообщения на один номер (флуд), на номера из черного списка, на запрещенные спам фильтром тексты или имена отправителей (Sender ID).',
                    '24' =>	'Недостаточно средств	На счете Клиента недостаточная сумма для отправки сообщения.',
                    '25' =>	'Недоступный номер	Телефонный номер не принимает SMS-сообщения, или на этого оператора нет рабочего маршрута.',
                );
                $modelMessage_Delivery->error = trim($modelMessage_Delivery->error . ' ' . Arr::get($codeToMessage, (string)$status['status'], Arr::get($status, 'status_name', 'error#' . $status['status'])));
            }
            
            $modelMessage_Delivery->last_check_at = date('Y-m-d H:i:s');
            $modelMessage_Delivery->save();
        }
    }

    protected function parseStatus($status, $all, $guessed_external_id)
    {
        $result = array();

        $status = explode(',', $status);
        $result['status'] = $status[0];
        if ( ! $all )
        {
            if ( ! isset($status[3]) )
            {
                Kohana::$log->add(Log::DEBUG, 'DELIVERYMAN/CRON/STATUSMESSAGEDELIVERY/DEBUG STATUS[3] IS NOT SET :STATUS GUESSED_EXTERNAL_ID::GUESSED_EXTERNAL_ID', array(
                    ':STATUS' => implode(',', $status),
                    ':GUESSED_EXTERNAL_ID' => $guessed_external_id,
                ));
                $status[3] = $guessed_external_id;
            }

            $result['last_timestamp'] = $status[1];
            $result['err'] = $status[2];
            $result['external_id'] = $status[3];
        }
        else
        {
            // по идее этот if при all=1 не требуется, но навсякий случай оставил
            if ( ! isset($status[9]) )
            {
                Kohana::$log->add(Log::DEBUG, 'DELIVERYMAN/CRON/STATUSMESSAGEDELIVERY/DEBUG STATUS[9] IS NOT SET :STATUS GUESSED_EXTERNAL_ID::GUESSED_EXTERNAL_ID', array(
                    ':STATUS' => implode(',', $status),
                    ':GUESSED_EXTERNAL_ID' => $guessed_external_id,
                ));
                $status[9] = $guessed_external_id;
            }

            $result['last_timestamp'] = $status[1];
            $result['err'] = $status[2];
            $result['send_timestamp'] = $status[3];
            $result['phone'] = $status[4];
            $result['cost'] = $status[5];
            $result['sender'] = $status[6];
            $result['status_name'] = $status[7];
            $result['message'] = $status[8];
            $result['external_id'] = $status[9];
        }

        return $result;
    }

    public function createMessageTemplate($list, $title, $body, $date=null,$sender_name=null, $sender_email=null){}
    public function createCampaign($message_id, $start_time){}
    public function getLists($list=null){}
    public function createList($title){}
    public function editList($list, $new_title){}
    public function deleteList($list){}
    public function subscribeEmail($lists, $email, $fields=array()){}
    public function unsubscribeEmail($lists, $email, $is_remove=true){}
    public function getFields($name=null){}
    public function createField($name, $type, $is_visible=1){}
    public function updateField($field, $name=null, $type=null, $is_visible=null){}
    public function deleteField($field){}

}