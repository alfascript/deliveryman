<?php defined('SYSPATH') or die();

/**
 * 
 */
class DeliveryMan_Driver_NotificationProfile extends DeliveryMan_Message_Delivery {

    use DeliveryMan_Driver_Trait_Notification;
    
    /**
     * Отправка письма на email
     * @param string|array $emails Список получателей, через запятую или массив
     * @param string $title
     * @param string $body
     * @param string|null $sender_name
     * @param string|null $sender_email
     * @return bool
     */
    public function sendMessage($message_id = null, $addressRecepient, $subject, $body, $sender_name = null, $sender_email = null, $recipient = null, $mailing_id = null, $only_confirmed_users = null)
    {
        $modelMessage_Delivery = $this->createDelivery($message_id, $addressRecepient);
        $modelMessage_Delivery->sent_at = date('Y-m-d H:i:s');
        $modelMessage_Delivery->save();
    }

    public function getNotificationProfile(Model_User $user, $idMessageDelivery)
    {
        return ORM::factory('Message_Delivery')
            ->with('message')
            ->where('message.user_id', '=', $user->id)
            ->where('message_delivery.id', '=', $idMessageDelivery)
            ->where('message_delivery.driver', '=', $this->_driver)
            ->where('message_delivery.delivered_at', 'IS', null)
            ->find();
    }

    /**
     * @param Model_User $user
     * @param int $offset
     * @param int $count
     * @return array of Model_Message_Delivery
     */
    public function getNotificationsProfile(Model_User $user, $offset, $count)
    {
        return ORM::factory('Message_Delivery')
            ->with('message')
            ->where('message.user_id', '=', $user->id)
            ->where('message_delivery.driver', '=', $this->_driver)
            ->order_by('message_delivery.id', 'DESC')
            ->offset($offset)
            ->limit($count)
            ->find_all();
    }
    
    /**
     * @param Model_User $user
     * @param int $offset
     * @param int $count
     * @return array of Model_Message_Delivery
     */
    public function getCountNotificationsProfile(Model_User $user)
    {
        return ORM::factory('Message_Delivery')
            ->with('message')
            ->where('message.user_id', '=', $user->id)
            ->where('message_delivery.driver', '=', $this->_driver)
            ->count_all();
    }

    public function createMessageTemplate($list, $title, $body, $date=null,$sender_name=null, $sender_email=null){}
    public function createCampaign($message_id, $start_time){}
    public function getLists($list=null){}
    public function createList($title){}
    public function editList($list, $new_title){}
    public function deleteList($list){}
    public function subscribeEmail($lists, $email, $fields=array()){}
    public function unsubscribeEmail($lists, $email, $is_remove=true){}
    public function getFields($name=null){}
    public function createField($name, $type, $is_visible=1){}
    public function updateField($field, $name=null, $type=null, $is_visible=null){}
    public function deleteField($field){}
}