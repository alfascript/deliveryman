<?php defined('SYSPATH') or die();

class DeliveryMan_Driver_EmailUnisender extends DeliveryMan_Message_Delivery {

    use DeliveryMan_Driver_Trait_Email;
    
    protected $_clientUnisender;
    
    public function __construct(array $config)
    {
        parent::__construct($config);
        
        $this->_clientUnisender = new DeliveryMan_Driver_Client_Unisender(Arr::extract($config, array(
            'urlsApi',
            'keyApi',
            'timeout',
            'countRetry',
            'test_mode',
        )));
    }
    
    /**
     * 
     */
    public function getBalance()
    {
        $result = $this->_clientUnisender->getUserInfo(array(
            'login' => $this->_config['login'],
        ));
        
        return Arr::path($result, 'result.balance');
    }
    
    /**
     * 
     * @param string|array $emails
     * @param string $subject
     * @param string $body
     * @param string|null $sender_name
     * @param string|null $sender_email
     * @throws Exception
     */
    public function sendMessage($message_id = null, $addressRecepient, $subject, $body, $sender_name = null, $sender_email = null, $recipient = null, $mailing_id = null, $only_confirmed_users = null)
    {
        $email = trim($addressRecepient);
        $this->checkEmail($email);
        
        // Default sender name and email
        if ( empty($sender_name) || empty($sender_email) )
        {
             $this->fillSender($sender_name, $sender_email);
        }

        $modelMessage_Delivery = $this->createDelivery($message_id, $email, $sender_email);
        if ( false === strpos($body, '{{_UnsubscribeUrl}}') && false === strpos($body, '{{UnsubscribeUrl}}') )
        {
            $body .= '<p>Чтобы отписаться от этой рассылки, перейдите по <a href="{{UnsubscribeUrl}}">ссылке</a></p>';
            
            if ( null !== $message_id )
            {
                $messagetemplate = ORM::factory('Message', $message_id)->template;
                $this->reportUnsubscribeUrlMissing($messagetemplate->name, $email);
            }
        }
        $this->followedLink($body, $modelMessage_Delivery->id);

        $params = array(
            'email'        => $email,
            'sender_name'  => $sender_name,
            'sender_email' => $sender_email,
            'subject'      => $subject,
            'body'         => (string)$body,
            'list_id'      => $this->_config['lists']['notification'],
            'images_as'    => 'only_links',
            'track_read'   => 1,
            'error_checking' => 1,
            'wrap_type'    => 'skip',
        );

        try
        {
            $result = $this->_clientUnisender->sendEmail($params);
        }
        catch(Exception $e)
        {
            $modelMessage_Delivery->error = $e->getMessage();
            $modelMessage_Delivery->save();
            throw $e;
        }

//        Kohana::$log->add(Log::DEBUG, 'sendMail result'."\n".':result', array(
//            ':result' => print_r($result, TRUE),
//        ));

        $result = $result['result'];
        if ( 0 === key($result) )
        {
            $result = $result[0];
        }
        
        $modelMessage_Delivery->external_id = Arr::get($result, 'id', Arr::get($result, 'email_id'));
        $modelMessage_Delivery->sent_at = date('Y-m-d H:i:s');
        $modelMessage_Delivery->save();
    }

    /**
     * 
     * @param string|array $messagetemplateNames
     * @return void
     */
    protected function reportUnsubscribeUrlMissing($messagetemplateName, $email)
    {
        try
        {
            $messagetemplateNotFound = Model_Index::GSCMS_CONFIG('deliveryman.messagetemplateNotFound');
        }
        catch(Exception $e)
        {
            $messagetemplateNotFound = null;
        }
        if ( null === $messagetemplateNotFound )
        {
            return;
        }
        
        foreach($messagetemplateNotFound as $reportReciever)
        {
            try
            {
                DeliveryMan_Message_Delivery::instance($reportReciever[0])
                    ->sendMessage(null, $reportReciever[1], 'Отсутствует параметр UnsubscribeUrl', 'Отсутствует параметр {{UnsubscribeUrl}} при отправке сообщения по шаблону "' . $messagetemplateName . '", получатель: ' . $email);
            }
            catch(Exception $e)
            {
            }
        }
    }
    
    /**
     * 
     * @param Database_Result $modelsMessage_Delivery
     */
    public function checkMessageDelivery(Database_Result $modelsMessage_Delivery)
    {
        $external_ids = $modelsMessage_Delivery->as_array(null, 'external_id');
        
        $result = $this->_clientUnisender->checkEmail(array(
            'email_id' => implode(',', $external_ids),
        ));

        $statuses = $result['result']['statuses'];
        foreach($statuses as $status)
        {
            $modelMessage_Delivery = ORM::factory('Message_Delivery')->where('external_id', '=', $status['id'])->find();
            if ( 'not_sent' == $status['status'] )
            {
                $modelMessage_Delivery->sent_at = null;
            }
            elseif ( 'ok_sent' == $status['status'] )
            {
                $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s');
            }
            elseif ( 'ok_delivered' == $status['status'] )
            {
                $modelMessage_Delivery->delivered_at = date('Y-m-d H:i:s');
                $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s');
            }
            elseif ( 'ok_read' == $status['status'] || 'ok_spam_folder' == $status['status'] )
            {
                $modelMessage_Delivery->opened_at = $modelMessage_Delivery->opened_at ?: date('Y-m-d H:i:s');
                $modelMessage_Delivery->delivered_at = $modelMessage_Delivery->delivered_at ?: date('Y-m-d H:i:s');
                $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s');
            }
            elseif ( 'ok_link_visited' == $status['status'] )
            {
                $modelMessage_Delivery->followed_link_at = $modelMessage_Delivery->followed_link_at ?: date('Y-m-d H:i:s');
                $modelMessage_Delivery->opened_at = $modelMessage_Delivery->opened_at ?: date('Y-m-d H:i:s');
                $modelMessage_Delivery->delivered_at = $modelMessage_Delivery->delivered_at ?: date('Y-m-d H:i:s');
                $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s');
            }
            elseif ( 'ok_unsubscribed' == $status['status'] )
            {
                $modelMessage_Delivery->unsubscribed_at = $modelMessage_Delivery->unsubscribed_at ?: date('Y-m-d H:i:s');
                $modelMessage_Delivery->opened_at = $modelMessage_Delivery->opened_at ?: date('Y-m-d H:i:s');
                $modelMessage_Delivery->delivered_at = $modelMessage_Delivery->delivered_at ?: date('Y-m-d H:i:s');
                $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s');
            }
            elseif ( 'err_will_retry' == $status['status'] || 'err_resend' == $status['status'] )
            {
            }
            else
            {
                $modelMessage_Delivery->error = $modelMessage_Delivery->error . $status['status'];
            }
            
            $modelMessage_Delivery->last_check_at = date('Y-m-d H:i:s');
            $modelMessage_Delivery->save();
        }
    }
    
    /**
     * Функкция рассылки тектового письма по адресам из листа
     * $list может быть как имя листа так его id
     * @param $list
     * @param $title
     * @param $body
     * @param null $date
     * @param null $sender_name
     * @param null $sender_email
     * @return bool;
     */
    function createMessageTemplate($list, $title, $body, $date=null, $sender_name=null, $sender_email=null) {
        if(is_array($this->_config)) {
            if(!$sender_name || !$sender_email) {
                $this->fillSender($sender_name, $sender_email);
            }

            if(!is_null($date)) {
                if(!is_int($date)) $date=strtotime($date);
                $date = date('Y-m-d H:i:s', $date);
            }

            $listData = $this->getLists($list);

            if(is_array($listData) && !empty($listData)) {
                $listData = array_shift($listData);
                $data = array(
                    'format'       => 'json',
                    'api_key'      => $this->_config['secret_key'],
                    'sender_name'  => $sender_name,
                    'sender_email' => $sender_email,
                    'subject'      => $title,
                    'body'         => $body,
                    'list_id'      => $listData['id']
                );

                $url  = str_replace('{METHOD}', 'createMessageTemplate', $this->_config['url']);
                $res = $this->_request($url, Request::POST, $data);

                if(!isset($res['error']) && isset($res['result']['message_id'])) {
                     return $this->createCampaign($res['result']['message_id'], $date);
                }
                else {
                    Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->createMessageTemplate (Error ['.$res['error'].': '.$res['code'].']!)');
                }

            }
        }

        return false;
    }

    //Функция планирования времени рассылки текстового письма
    function createCampaign($message_id, $start_time=null) {
        $return = null;
        if(is_array($this->_config)) {
            $data = array(
                'format'       => 'json',
                'api_key'      => $this->_config['secret_key'],
                'message_id'   => $message_id,
                'start_time'   => $start_time,
                'timezone'     => 'UTC'
            );

            $url  = str_replace('{METHOD}', 'createCampaign', $this->_config['url']);
            $res = $this->_request($url, Request::POST, $data);
            if(!isset($res['error']) && isset($res['result']['campaign_id'])) {
                $return = $res['result']['campaign_id'];
            }
            else {
                Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->createCampaign (Error ['.$res['error'].': '.$res['code'].']!)');
            }
        }

        return $return;
    }

    /**
     * Функция получения списков
     * @param null $list
     * @return null|array
     * $list может быть id листа или имя листа
     * если $list пустая то функция вернет все существующие листы
     */
    function getLists($list=null){
        if(!is_array($list) && !is_null($list)) {
           $list = array($list);
        }

        $return = null;
        if(is_array($this->_config)) {
            $data = array(
                'format'       => 'json',
                'api_key'      => $this->_config['secret_key']
            );

            $url  = str_replace('{METHOD}', 'getLists', $this->_config['url']);
            $res = $this->_request($url, Request::POST, $data);

            if(isset($res['result']) && is_array($res['result']) && !empty($res['result'])) {
                foreach($res['result'] as $e) {
                     if(!$list || (in_array($e['id'],$list) || in_array($e['title'],$list))) {
                         if(!is_array($return)) $return = array();
                         $return[] = $e;
                     }
                }
            }
        }

        return $return;
    }

    //Функция создания листа
    function createList($title)  {
        $return = null;
        if(is_array($this->_config)) {
            $data = array(
                'format'       => 'json',
                'api_key'      => $this->_config['secret_key'],
                'title'        => $title
            );

            $url  = str_replace('{METHOD}', 'createList', $this->_config['url']);
            $res = $this->_request($url, Request::POST, $data);

            if(!isset($res['error']) && isset($res['result']['id'])) {
                $return = $res['result']['id'];
            }
            else {
                Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->createList('.$title.') (Error ['.$res['error'].': '.$res['code'].']!)');
            }
        }

        return $return;
    }

    //Функция редактирования листа
    function editList($list, $new_title) {
        $return = null;
        if(is_array($this->_config)) {
            $listData = $this->getLists($list);

            if(is_array($listData) && !empty($listData)) {
                $listData = array_shift($listData);

                $data = array(
                    'format'       => 'json',
                    'api_key'      => $this->_config['secret_key'],
                    'list_id'      => $listData['id'],
                    'title'        => $new_title
                );

                $url  = str_replace('{METHOD}', 'updateList', $this->_config['url']);
                $res = $this->_request($url, Request::POST, $data);

                if(!isset($res['error'])) {
                    $return = true;
                }
                else {
                    Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->editList('.$list.','.$new_title.') (Error ['.$res['error'].': '.$res['code'].']!)');
                }
            }
            else {
                Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->editList('.$list.','.$new_title.') (Error [list ('.$list.') not found]!)');
            }
        }

        return $return;
    }

    //Функция удаления листа
    function deleteList($list) {
        $return = null;
        if(is_array($this->_config)) {
            $listData = $this->getLists($list);

            if(is_array($listData) && !empty($listData)) {
                $listData = array_shift($listData);

                $data = array(
                    'format'       => 'json',
                    'api_key'      => $this->_config['secret_key'],
                    'list_id'      => $listData['id'],
                );

                $url  = str_replace('{METHOD}', 'deleteList', $this->_config['url']);
                $res = $this->_request($url, Request::POST, $data);

                if(!isset($res['error'])) {
                    $return = true;
                }
                else {
                    Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->deleteList('.$list.') (Error ['.$res['error'].': '.$res['code'].']!)');
                }
            }
            else {
                Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->deleteList('.$list.') (Error [list ('.$list.') not found]!)');
            }
        }

        return $return;
    }

    //Функция подписки email адреса в листы
    function subscribeEmail($lists, $email, $fields=array()) {
        $return = null;

        if(is_array($this->_config)) {
            if(!is_array($lists)) {
                $lists=array($lists);
            }

            $listsData = $this->getLists($lists);

            if(is_array($listsData) && !empty($listsData)) {
                $ids = array();
                foreach($listsData as $l) {
                    $ids[] = $l['id'];
                }

                $fdata = array();

                if(is_array($fields) && !empty($fields)) {
                    foreach($fields as $key=>$val) {
                        $fdata['fields['.$key.']'] = $val;
                    }
                }

                $data = array_merge($fdata,
                    array(
                    'format'         => 'json',
                    'api_key'        => $this->_config['secret_key'],
                    'list_ids'       => implode(',', $ids),
                    'fields[email]'  => $email
                ));

                $url  = str_replace('{METHOD}', 'subscribe', $this->_config['url']);
                $res = $this->_request($url, Request::POST, $data);

                if(!isset($res['error']) && isset($res['result']['person_id'])) {
                    $return = $res['result']['person_id'];
                }
                else {
                    Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->subscribeEmail (Error ['.$res['error'].': '.$res['code'].']!)');
                }
            }
            else {
                Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->subscribeEmail (Error Lists ('.implode(',', $lists).') not found!)');
            }
        }

        return $return;
    }

    //Функция отписки email адреса от листов
    function unsubscribeEmail($lists, $email, $is_remove=true) {
        $return = null;

        if(is_array($this->_config)) {
            if(!is_array($lists)) {
                $lists=array($lists);
            }

            $listsData = $this->getLists($lists);
            if(is_array($listsData) && !empty($listsData)) {
                $ids = array();
                foreach($listsData as $l) {
                    $ids[] = $l['id'];
                }

                $data = array(
                    'format'         => 'json',
                    'api_key'        => $this->_config['secret_key'],
                    'list_ids'       => implode(',', $ids),
                    'contact_type'   => 'email',
                    'contact'        => $email
                );

                $url  = str_replace('{METHOD}', (!$is_remove?'unsubscribe':'exclude'), $this->_config['url']);
                $res = $this->_request($url, Request::POST, $data);

                if(!isset($res['error']) && isset($res['result'])) {
                    $return = true;
                }
                else {
                    Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->unsubscribeEmail (Error ['.$res['error'].': '.$res['code'].']!)');
                }
            }
            else {
                Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->unsubscribeEmail (Error Lists ('.implode(',', $lists).') not found!)');
            }
        }

        return $return;
    }

    //Функция получения списка полей листа
    function getFields($name=null) {
        $return = null;
        if(is_array($this->_config)) {
            if(!is_null($name) && !is_array($name)) {
                $name = array($name);
            }

            $data = array(
                'format'         => 'json',
                'api_key'        => $this->_config['secret_key']
            );

            $url  = str_replace('{METHOD}', 'getFields', $this->_config['url']);
            $res = $this->_request($url, Request::POST, $data);

            if(!isset($res['error']) && isset($res['result'])) {
                foreach($res['result'] as $e) {
                    if(!$name || (in_array($e['id'],$name) || in_array($e['name'],$name))) {
                        if(!is_array($return)) $return = array();
                        $return[] = $e;
                    }
                }
            }
        }

        return $return;
    }

    //Функция создания поля для листа
    function createField($name, $type, $is_visible=1) {
        $return = null;
        $types = array('string','text','number','bool');

        if(is_array($this->_config) && in_array($type, $types)) {
            $fdata = $this->getFields($name);
            if(!$fdata) {
                $data = array(
                    'format'         => 'json',
                    'api_key'        => $this->_config['secret_key'],
                    'name'           => $name,
                    'type'           => $type,
                    'is_visible'     => $is_visible,
                    'view_pos'       => 1
                );

                $url  = str_replace('{METHOD}', 'createField', $this->_config['url']);
                $res = $this->_request($url, Request::POST, $data);

                if(!isset($res['error']) && isset($res['result']['id'])) {
                    $return = $res['result']['id'];
                }
                else {
                    Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->createField (Error ['.$res['error'].': '.$res['code'].']!)');
                }
            }
        }

        return $return;
    }

    //Функция изменения поля в листе
    function updateField($field, $name=null, $type=null, $is_visible=null) {
        $return = null;
        $types = array('string','text','number','bool');

        if(is_array($this->_config) && (!$type || in_array($type, $types))) {
            $fdata = $this->getFields($field);

            if(is_array($fdata) && !empty($fdata)) {
                $fdata = array_shift($fdata);

                if(!$name) $name=$fdata['name'];
                if(!$type) $type=$fdata['type'];
                if(!$is_visible) $is_visible=$fdata['is_visible'];

                $data = array(
                    'format'         => 'json',
                    'api_key'        => $this->_config['secret_key'],
                    'id'             => $fdata['id'],
                    'name'           => $name,
                    'type'           => $type,
                    'is_visible'     => $is_visible,
                    'view_pos'       => 1
                );

                $url  = str_replace('{METHOD}', 'updateField', $this->_config['url']);
                $res = $this->_request($url, Request::POST, $data);

                if(!isset($res['error']) && isset($res['result']['id'])) {
                    $return = $res['result']['id'];
                }
                else {
                    Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->updateField (Error ['.$res['error'].': '.$res['code'].']!)');
                }
            }
            else {
                Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->updateField (Error Field ('.$field.') not found!)');
            }

        }

        return $return;
    }

    //Функция удаления поля из листа
    function deleteField($field) {
        $return = null;

        if(is_array($this->_config)) {
            $fdata = $this->getFields($field);

            if(is_array($fdata) && !empty($fdata)) {
                $fdata = array_shift($fdata);
                $data = array(
                    'format'         => 'json',
                    'api_key'        => $this->_config['secret_key'],
                    'id'             => $fdata['id'],
                );

                $url  = str_replace('{METHOD}', 'deleteField', $this->_config['url']);
                $res = $this->_request($url, Request::POST, $data);

                if(!isset($res['error']) && isset($res['result'])) {
                    $return = true;
                }
                else {
                    Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->deleteField (Error ['.$res['error'].': '.$res['code'].']!)');
                }
            }
            else {
                Kohana::$log->add(Log::ERROR, 'DeliveryMan_Unisender->deleteField (Error Field ('.$field.') not found!)');
            }
        }

        return $return;
    }

}