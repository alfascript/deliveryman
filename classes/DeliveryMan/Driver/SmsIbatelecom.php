<?php defined('SYSPATH') or die();

class DeliveryMan_Driver_SmsIbatelecom extends DeliveryMan_Message_Delivery {

    use DeliveryMan_Driver_Trait_Sms;
    
    protected $_clientIbatelecom;
    
    public function __construct(array $config)
    {
        parent::__construct($config);
        
        $this->_clientIbatelecom = new DeliveryMan_Driver_Client_Ibatelecom(Arr::extract($config, array(
            'urlsApi',
            'login',
            'password',
            'timeout',
            'countRetry',
            'test_mode',
        )));
    }
    
    /**
     * 
     */
    public function getBalance()
    {
        $result = $this->_clientIbatelecom->balance();
        
        return $result->money;
    }
    
    /**
     * 
     * @param string|array $emails
     * @param string $title
     * @param string $body
     * @param string|null $sender_name
     * @param string|null $sender_email
     * @throws Exception
     */
    public function sendMessage($message_id = null, $addressRecepient, $subject, $body, $sender_name = null, $sender_email = null, $recipient = null, $mailing_id = null, $only_confirmed_users = null)
    {
        $phone = $addressRecepient;
        $body = str_replace('\n', "\n", $body);
        
        // Default sender name and email
        if ( empty($sender_name) )
        {
            $sender_email = '123';
            $this->fillSender($sender_name, $sender_email);
        }

        $modelMessage_Delivery = $this->createDelivery($message_id, $phone, $sender_name);
        
        $params = array(
            'message'  => array(
                '@' => array(
                    'type' => 'sms',
                ),
                'sender' => $sender_name,
                'text' => (string)$body,
                'abonent' => array(
                    '@' => array(
                        'phone' => $phone,
                        'number_sms' => 1,
                        'client_id_sms' => $modelMessage_Delivery->id,
                    ),
                ),
            ),
        );

        try
        {
            $result = $this->_clientIbatelecom->index($params);
        }
        catch(Exception $e)
        {
            $modelMessage_Delivery->error = $e->getMessage();
            $modelMessage_Delivery->save();
            throw $e;
        }
        
//        Kohana::$log->add(Log::DEBUG, 'sendSms result'."\n".':result', array(
//            ':result' => print_r($result, TRUE),
//        ));
        
        $modelMessage_Delivery->external_id = (string)$result->information['id_sms'];
        $modelMessage_Delivery->sent_at = date('Y-m-d H:i:s');
        $modelMessage_Delivery->save();
    }

    /**
     * 
     * @param Database_Result $modelsMessage_Delivery
     */
    public function checkMessageDelivery(Database_Result $modelsMessage_Delivery)
    {
        $external_ids = $modelsMessage_Delivery->as_array(null, 'external_id');
        
        $get_state = '';
        foreach($external_ids as $external_id)
        {
            $get_state .= '<id_sms>' . $external_id . '</id_sms>';
        }
        
        $result = $this->_clientIbatelecom->state(array(
            'get_state' => $get_state,
        ));

        foreach($result->state as $status)
        {
            // Bug in Ibatelecom
            if ( empty($status['id_sms']) )
            {
                continue;
            }
            
            $modelMessage_Delivery = ORM::factory('Message_Delivery')->where('external_id', '=', $status['id_sms'])->find();
            if ( 'send' == $status )
            {
                $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: date('Y-m-d H:i:s');
            }
            elseif ( 'deliver' == $status )
            {
                $modelMessage_Delivery->delivered_at = $modelMessage_Delivery->delivered_at ?: date('Y-m-d H:i:s');
                $modelMessage_Delivery->sent_at = $modelMessage_Delivery->sent_at ?: $status['time'];
            }
            else
            {
                $modelMessage_Delivery->error = $modelMessage_Delivery->error . $status;
            }
            
            $modelMessage_Delivery->last_check_at = date('Y-m-d H:i:s');
            $modelMessage_Delivery->save();
        }
    }

    public function createMessageTemplate($list, $title, $body, $date=null,$sender_name=null, $sender_email=null){}
    public function createCampaign($message_id, $start_time){}
    public function getLists($list=null){}
    public function createList($title){}
    public function editList($list, $new_title){}
    public function deleteList($list){}
    public function subscribeEmail($lists, $email, $fields=array()){}
    public function unsubscribeEmail($lists, $email, $is_remove=true){}
    public function getFields($name=null){}
    public function createField($name, $type, $is_visible=1){}
    public function updateField($field, $name=null, $type=null, $is_visible=null){}
    public function deleteField($field){}

}