<?php defined('SYSPATH') or die();

/**
 * Драйвер для массовых рассылок.
 * Не должен использоваться для обычных писем!
 */
class DeliveryMan_Driver_EmailPhpMailing extends DeliveryMan_Message_Delivery {

    use DeliveryMan_Driver_Trait_EmailPostmaster;
    protected $unsubscribeUrl;
    protected $user;

    /**
     * Отправка письма на email
     * @param null $message_id
     * @param $addressRecepient
     * @param string $subject
     * @param string $body
     * @param string|null $sender_name
     * @param string|null $sender_email
     * @param Model_User|null $recipient
     * @param int|null $mailing_id
     * @param tinyint|null $only_confirmed_users
     * @return bool
     * @throws DeliveryMan_Exception_EmailBoxNotExistsException
     * @throws DeliveryMan_Exception_InvalidEmailException
     */
    public function sendMessage($message_id = null, $addressRecepient, $subject, $body, $sender_name = null, $sender_email = null, $recipient = null, $mailing_id = null, $only_confirmed_users = null)
    {
        if ( ! $recipient instanceof Model_User)
        {
            Kohana::$log->add(Kohana_Log::ERROR, 'Recipient must be an instance of Model_User');

            return false;
        }

        $this->user = $recipient;
        $email = trim($addressRecepient);

        // Default sender name and email
        if (empty($sender_name) || empty($sender_email))
        {
            $this->fillSender($sender_name, $sender_email);
        }
        $modelMessage_Delivery = $this->createMailingDelivery($message_id, $mailing_id, $recipient->id, $email);

        // проверка на только подтвержденных пользователей
        if ($only_confirmed_users && ! $this->checkEmail($email))
        {
            $modelMessage_Delivery->status = Model_MailingDelivery::STATUS_BOUNCED;
            $modelMessage_Delivery->save();

            return false;
        }

        $this->unsubscribeUrl = $this->unsubscribe($body, $modelMessage_Delivery->id);
//        $this->followedLink($body, $modelMessage_Delivery->id);
        $body = $this->replaceMacros($body);
        $subject = $this->replaceMacros($subject);

        $this->trackRead($body, $modelMessage_Delivery->id);
        $headers = array(
            "MIME-Version: 1.0",
            "Content-type: text/html; charset=UTF-8",
            "Message-Id: " . self::formatMessageId($message_id, $sender_email),
            "From: " . mb_encode_mimeheader($sender_name, Kohana::$charset) . " <{$sender_email}>",
            "Return-Receipt-To: <{$sender_email}>",
        );
        if ('' != $this->unsubscribeUrl)
        {
            $complaints_email = $sender_email;
            $headers[] = "List-Unsubscribe: <{$this->unsubscribeUrl}>, <mailto:$complaints_email?subject=Unsubscribe>";
        }
        if ($postmasterMsgtype = $this->getPostmasterMsgtype())
        {
            $headers[] = "List-Id: <{$postmasterMsgtype}>";
            $headers[] = "X-Postmaster-Msgtype: {$postmasterMsgtype}";
        }

        $mail = new PHPMailer;
        $mail->isSMTP();

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer'       => false,
                'verify_peer_name'  => false,
                'allow_self_signed' => true,
            ),
        );

        $mail->Host = $this->_config['smtpHost'];
        $mail->Port = $this->_config['smtpPort'];
        $mail->From = $sender_email;
        $mail->FromName = $sender_name;
        $mail->addCustomHeader("MIME-Version: 1.0");
        $mail->addCustomHeader("Content-type: text/html; charset=UTF-8");
        $mail->addCustomHeader("Message-Id: " . self::formatMessageId($message_id, $sender_email));
        if ('' != $this->unsubscribeUrl)
        {
            $complaints_email = $sender_email;
            $mail->addCustomHeader("List-Unsubscribe: <{$this->unsubscribeUrl}>, <mailto:$complaints_email?subject=Unsubscribe>");
        }
        $mail->addAddress($email);
        $mail->Subject = mb_encode_mimeheader($subject, Kohana::$charset);
        $mail->Body = $body;
        $mail->CharSet = 'utf-8';
        $mail->isHTML(true);

        if ( ! $mail->send())
        {
            throw new RuntimeException('Error sending email through emailPhp');
        }

        $modelMessage_Delivery->sent_at = date('Y-m-d H:i:s');
        $modelMessage_Delivery->save();
        return true;
    }

    public function createMessageTemplate($list, $title, $body, $date = null, $sender_name = null, $sender_email = null)
    {
    }

    public function createCampaign($message_id, $start_time)
    {
    }

    public function getLists($list = null)
    {
    }

    public function createList($title)
    {
    }

    public function editList($list, $new_title)
    {
    }

    public function deleteList($list)
    {
    }

    public function subscribeEmail($lists, $email, $fields = array())
    {
    }

    public function unsubscribeEmail($lists, $email, $is_remove = true)
    {
    }

    public function getFields($name = null)
    {
    }

    public function createField($name, $type, $is_visible = 1)
    {
    }

    public function updateField($field, $name = null, $type = null, $is_visible = null)
    {
    }

    public function deleteField($field)
    {
    }

    public static function formatMessageId($id, $sender_email)
    {
        return "<" . $id . strrchr($sender_email, '@') . ">";
    }

    protected function unsubscribe(&$body, $message_id)
    {
        $message_delivery_id_enc = Encrypt::instance('deliveryman')->encode($message_id);
        $unsubscribeUrl = 'http://' . $this->_config['sender_domain'] . '/deliveryman/unsubscribemailing/' . strtr(rtrim(base64_encode($message_delivery_id_enc), '='), '+/', '-_');

        return $unsubscribeUrl;
    }

    protected function followedLink(&$body, $message_delivery_id)
    {
        $message_delivery_status = strtr(rtrim(base64_encode(Encrypt::instance('deliveryman')->encode(json_encode(array(
            'message_delivery_id' => $message_delivery_id,
            'status'              => 'followed_link',
        )))), '='), '+/', '-_');

        // Подставляем email_id во все ссылки, кроме макросов
        $body = preg_replace_callback('/<a(.*)href=([\'"])([^\'"]*)([\'"].*)>/', function ($matches) use ($message_delivery_status)
        {
            $href = $matches[3];

            // Если это {{UnsubscribeUrl}}, {{WebLetterUrl}} или любой другой макрос, то пропускаем ссылку
            if (preg_match('/^\s*{{[^}]+}}\s*$/', $href))
            {
                return $matches[0];
            }

            // Добавляем ?email_id= в ссылку
            $url_components = parse_url($href);
            parse_str(Arr::get($url_components, 'query'), $query);
            $query['mds'] = $message_delivery_status;
            $url_components['query'] = http_build_query($query);
            $href = http_build_url($url_components);

            return '<a' . $matches[1] . 'href=' . $matches[2] . $href . $matches[4] . '>';
        }, $body);
    }

    public function getAddressRecepient(Model_User $user, array $addressRecepientOverride)
    {
        return Arr::get($addressRecepientOverride, 'email', $user->profile->email);
    }

    protected function checkEmail($email)
    {
        if ($this->EmailConfirmed($email))
        {
            return true;
        }

        return false;
    }

    protected function trackRead(&$body, $message_delivery_id)
    {
        return;
        $message_delivery_status = strtr(rtrim(base64_encode(Encrypt::instance('deliveryman')->encode(json_encode(array(
            'message_delivery_id' => $message_delivery_id,
            'status'              => 'opened',
        )))), '='), '+/', '-_');

        $trackReadHtml = '' .
            '<center>' .
            '<table bgcolor="white">' .
            '<tbody>' .
            '<tr>' .
            '<td>' .
            '<font size="-1" color="black">' .
            '<img width="16" height="16" alt="" title="" border="0" src="http://' . $this->_config['sender_domain'] . '/deliveryman/mailing/delivery/' . $message_delivery_status . '">' .
            '</font>' .
            '</td>' .
            '</tr>' .
            '</tbody>' .
            '</table>' .
            '</center>';
        
        $newbody = preg_replace('/\<\/body\>\s*\<\/html\>/s', $trackReadHtml . '$0', $body); 
        $body = (strcmp($body, $newbody) === 0) ? $body . $trackReadHtml : $newbody; // если подстановки изображения не произошло, то добавляем в конец строки
        
    }

    protected function replaceMacros($body)
    {

        $what = [
            '{{PROJECT}}',
            '{{Unsubscribe}}',
            '{{UnsubscribeUrl}}',
            '{{YEAR}}',
            '{{MAIN_MIRROR}}',
            '{{LAST_MIRROR}}',
            '{{DOMAIN}}',
            '{{URL}}',
        ];
        $replacement = [
            PROJECT,
            'Чтобы отписаться от этой рассылки, перейдите по <a href="' . $this->unsubscribeUrl . '">ссылке</a>',
            $this->unsubscribeUrl,
            date('Y'),
            $this->_config['sender_domain'],
            $this->_config['sender_domain'],
            $this->_config['sender_domain'],
            'http://' . $this->_config['sender_domain'],
        ];

        $body = str_replace($what, $replacement, $body);

        return $body;
    }

    public function EmailConfirmed($email)
    {
        $modelUserProfile = ORM::factory('Userprofile')
            ->where('email', '=', $email)
            ->where('is_confirmed_email', '=', 1)
            ->find();

        if ($modelUserProfile->loaded())
        {
            return true;
        }

        return false;
    }
}
