<?php defined('SYSPATH') or die();

class DeliveryMan_Driver_EmailPhpMirror extends DeliveryMan_Driver_EmailPhp {

    protected $_lastMirror;
    
    public function setLastMirror($lastMirror)
    {
        $this->_lastMirror = $lastMirror;
    }

    public function sendMessage($message_id = null, $addressRecepient, $subject, $body, $sender_name = null, $sender_email = null, $recipient = null, $mailing_id = null, $only_confirmed_users = null)
    {
        assert(null === $sender_email, 'Sender email must not be set. Please, remove deliveryman.emailPhpMirror.sender_email key from config.');
        assert(null !== $this->_lastMirror, 'Last mirror is not set.');
        
        $sender_email = 'robot@' . $this->_lastMirror;
        
        return parent::sendMessage($message_id, $addressRecepient, $subject, $body, $sender_name, $sender_email, $recipient, $mailing_id);
    }
    
}