<?php defined('SYSPATH') or die();

/**
 * 
 */
class DeliveryMan_Driver_EmailNull extends DeliveryMan_Message_Delivery {

    use DeliveryMan_Driver_Trait_Email;
    use DeliveryMan_Driver_Trait_EmailPostmaster;
    
    /**
     * Отправка письма на email
     * @param string|array $emails Список получателей, через запятую или массив
     * @param string $subject
     * @param string $body
     * @param string|null $sender_name
     * @param string|null $sender_email
     * @return bool
     */
    public function sendMessage($message_id = null, $addressRecepient, $subject, $body, $sender_name = null, $sender_email = null, $recipient = null, $mailing_id = null, $only_confirmed_users = null)
    {
        $email = trim($addressRecepient);
        $this->checkEmail($email);
        
        // Default sender name and email
        if ( empty($sender_name) || empty($sender_email) )
        {
            $this->fillSender($sender_name, $sender_email);
        }

        $modelMessage_Delivery = $this->createDelivery($message_id, $email, $sender_email);
        
        $unsubscribeUrl = $this->unsubscribe($body, $modelMessage_Delivery);
        $this->followedLink($body, $modelMessage_Delivery->id);
        $this->trackRead($body, $modelMessage_Delivery->id);

        $headers = array(
            "MIME-Version: 1.0",
            "Content-type: text/html; charset=UTF-8",
            "Message-Id: <" . $modelMessage_Delivery->id . strrchr($sender_email, '@') . ">",
            "From: {$sender_name} <{$sender_email}>",
            "X-Confirm-Reading-To: <{$sender_email}>",
            "Disposition-Notification-To: <{$sender_email}>",
            "Return-Receipt-To: <{$sender_email}>",
        );
        if ( '' != $unsubscribeUrl )
        {
            $complaints_email = $sender_email;
            $headers[] = "List-Unsubscribe: <{$unsubscribeUrl}>, <mailto:$complaints_email?subject=Unsubscribe>";
//            $headers[] = "X-Unsubscribe: {$unsubscribeUrl}";
//            $headers[] = "X-Complaints-To: $complaints_email";
        }
        if ( $postmasterMsgtype = $this->getPostmasterMsgtype() )
        {
            $headers[] = "List-Id: <{$postmasterMsgtype}>";
            $headers[] = "X-Postmaster-Msgtype: {$postmasterMsgtype}";
        }
        
        Kohana::$log->add(Log::INFO, ':headers' . "\r\n" . ':body', array(
            ':headers' => implode("\r\n", array_merge($headers, array('To: ' . $addressRecepient . "\r\n", 'Subject: ' . $subject . "\r\n"))) . "\r\n\r\n",
            ':body' => $body,
        ));

        $headers = implode('', $headers);
        
        $modelMessage_Delivery->external_id = $modelMessage_Delivery->id;
        $modelMessage_Delivery->sent_at = date('Y-m-d H:i:s');
        $modelMessage_Delivery->save();
    }

    public function createMessageTemplate($list, $title, $body, $date=null,$sender_name=null, $sender_email=null){}
    public function createCampaign($message_id, $start_time){}
    public function getLists($list=null){}
    public function createList($title){}
    public function editList($list, $new_title){}
    public function deleteList($list){}
    public function subscribeEmail($lists, $email, $fields=array()){}
    public function unsubscribeEmail($lists, $email, $is_remove=true){}
    public function getFields($name=null){}
    public function createField($name, $type, $is_visible=1){}
    public function updateField($field, $name=null, $type=null, $is_visible=null){}
    public function deleteField($field){}
}