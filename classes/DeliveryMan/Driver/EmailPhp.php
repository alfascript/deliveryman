<?php defined('SYSPATH') or die();

/**
 * Запасной план на случай если все очень плохо
 * Если не передано имя драйвера и неверно указан
 * default в конфигах, или произошла какая то ошибка
 * при инициализации базового класса модуля
 * например нет в конфигах ключа email...
 * или же если по каким либо причинам выбранный драйвер
 * не может произвести отправку то его задача произвести отправку через этот драйвер.
 * (по умолчанию берется дефолтный email PHP)
 *
 */
class DeliveryMan_Driver_EmailPhp extends DeliveryMan_Message_Delivery {

    use DeliveryMan_Driver_Trait_Email;
    use DeliveryMan_Driver_Trait_EmailPostmaster;

    /**
     * Отправка письма на email
     * @param null $message_id
     * @param $addressRecepient
     * @param string $subject
     * @param string $body
     * @param string|null $sender_name
     * @param string|null $sender_email
     * @param int|null $recipient_id
     * @param int|null $mailing_id
     * @param tinyint|null $only_confirmed_users
     * @return bool
     * @throws DeliveryMan_Exception_EmailBoxNotExistsException
     * @throws DeliveryMan_Exception_InvalidEmailException
     * @throws phpmailerException
     * @internal param null $mailing
     * @internal param array|string $emails Список получателей, через запятую или массив
     */
    public function sendMessage($message_id = null, $addressRecepient, $subject, $body, $sender_name = null, $sender_email = null, $recipient = null, $mailing_id = null, $only_confirmed_users = null)
    {
        $email = trim($addressRecepient);
        $this->checkEmail($email);

        // Default sender name and email
        if ( empty($sender_name) || empty($sender_email) )
        {
            $this->fillSender($sender_name, $sender_email);
        }

        $modelMessage_Delivery = $this->createDelivery($message_id, $email, $sender_email);

        $unsubscribeUrl = $this->unsubscribe($body, $modelMessage_Delivery);
        $this->followedLink($body, $modelMessage_Delivery->id);
        $this->trackRead($body, $modelMessage_Delivery->id);

        $headers = array(
            "MIME-Version: 1.0",
            "Content-type: text/html; charset=UTF-8",
            "Message-Id: " . self::formatMessageId($modelMessage_Delivery, $sender_email),
            "From: " . mb_encode_mimeheader($sender_name, Kohana::$charset) . " <{$sender_email}>",
            "Return-Receipt-To: <{$sender_email}>",
        );
        if ( '' != $unsubscribeUrl )
        {
            $complaints_email = $sender_email;
            $headers[] = "List-Unsubscribe: <{$unsubscribeUrl}>, <mailto:$complaints_email?subject=Unsubscribe>";
//            $headers[] = "X-Unsubscribe: {$unsubscribeUrl}";
//            $headers[] = "X-Complaints-To: $complaints_email";
        }
        if ( $postmasterMsgtype = $this->getPostmasterMsgtype() )
        {
            $headers[] = "List-Id: <{$postmasterMsgtype}>";
            $headers[] = "X-Postmaster-Msgtype: {$postmasterMsgtype}";
        }
        $headers = implode("\r\n", $headers) . "\r\n";
        $parameters = '-f ' . $sender_email;

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host = $this->_config['smtpHost'];
        $mail->Port = $this->_config['smtpPort'];
        $mail->From = $sender_email;
        $mail->FromName = $sender_name;
        $mail->addCustomHeader("MIME-Version: 1.0");
        $mail->addCustomHeader("Content-type: text/html; charset=UTF-8");
        $mail->addCustomHeader("Message-Id: " . self::formatMessageId($modelMessage_Delivery, $sender_email));
        if ( '' != $unsubscribeUrl )
        {
            $complaints_email = $sender_email;
            $mail->addCustomHeader("List-Unsubscribe: <{$unsubscribeUrl}>, <mailto:$complaints_email?subject=Unsubscribe>");
        }
        $mail->addAddress($email);
        $mail->Subject = mb_encode_mimeheader($subject, Kohana::$charset);
        $mail->Body = $body;
        $mail->CharSet = 'utf-8';
        $mail->isHTML(true);

        if( ! $mail->send())
        {
            throw new RuntimeException('Error sending email through emailPhp');
        }

        $modelMessage_Delivery->sent_at = date('Y-m-d H:i:s');
        $modelMessage_Delivery->save();
    }

    public function createMessageTemplate($list, $title, $body, $date=null,$sender_name=null, $sender_email=null){}
    public function createCampaign($message_id, $start_time){}
    public function getLists($list=null){}
    public function createList($title){}
    public function editList($list, $new_title){}
    public function deleteList($list){}
    public function subscribeEmail($lists, $email, $fields=array()){}
    public function unsubscribeEmail($lists, $email, $is_remove=true){}
    public function getFields($name=null){}
    public function createField($name, $type, $is_visible=1){}
    public function updateField($field, $name=null, $type=null, $is_visible=null){}
    public function deleteField($field){}

    public static function formatMessageId(Model_Message_Delivery $modelMessage_Delivery, $sender_email)
    {
        return "<" . $modelMessage_Delivery->id . strrchr($sender_email, '@') . ">";
    }

}
