<?php defined('SYSPATH') or die();

class DeliveryMan_Message {

	/**
	 * @var  array  DeliveryMan_Message instances
	 */
	protected static $_instances = array();
    protected $_tries = 3;

    /**
     * @param Model_User $user
     * @return DeliveryMan_Message
     */
    static public function instance($user, array $maskDeliverySystems = null)
    {
        if ( $user instanceof FrontUser )
        {
            $user = ORM::factory('User', $user->id);
        }

        if ( ! $user instanceof Model_User || ! $user->loaded() )
        {
            throw new Exception('User is not loaded');
        }

        $config = Model_Index::GSCMS_CONFIG('deliveryman');

		if ( ! isset(self::$_instances[$user->id]) )
		{
            self::$_instances[$user->id] = new self($config, $user, $maskDeliverySystems);
        }

		return self::$_instances[$user->id];
    }

    /**
     * @var array
     */
    protected $_config;

    /**
     * @var Model_User
     */
    protected $_user;

    /**
     * @var int
     */
    protected $_ttl;

    /**
     * @var array
     */
    protected $_params;

    /**
     * @var array
     */
    protected $_addressRecepientOverride = array();

    /**
     * @var array
     */
    protected $_lastMirrorOverride;

    /**
     * @var array
     */
    protected $_driverOverride = array();

    /**
     * @var bool
     */
    protected $_testMessage = false;

    /**
     * @var bool
     */
    protected $_useDefaultMirror = false;

    /**
     * @param array $config
     */
    public function __construct($config, Model_User $user)
    {
        $this->_config = $config;
        $this->_user = $user;
    }

    /**
     *
     * @param int $ttl
     * @return DeliveryMan_Message
     */
    public function setTtl($ttl)
    {
        $this->_ttl = $ttl;

        return $this;
    }

    /**
     *
     * @param array $params
     * @return \DeliveryMan_Message
     */
    public function setParams(array $params)
    {
        $this->_params = $params;

        return $this;
    }

    /**
     *
     * @param string $email
     * @return \DeliveryMan_Message
     */
    public function setEmailOverride($email)
    {
        $this->_addressRecepientOverride['email'] = $email;

        return $this;
    }

    /**
     *
     * @param string $email
     * @return \DeliveryMan_Message
     */
    public function setSmsOverride($mobile)
    {
        $this->_addressRecepientOverride['sms'] = $mobile;

        return $this;
    }

    /**
     *
     * @param string $lastMirror
     * @return \DeliveryMan_Message
     */
    public function setLastMirrorOverride($lastMirror)
    {
        $this->_lastMirrorOverride = $lastMirror;

        return $this;
    }

    /**
     *
     * @param boolean $testMessage
     * @return \DeliveryMan_Message
     */
    public function setTestMessage($testMessage)
    {
        $this->_testMessage = (bool)$testMessage;

        return $this;
    }

    /**
     * Delivery systems initialization
     * @param array $maskDeliverySystems
     * @return array of DeliveryMan_Message_Delivery
     */
    public function initDeliverySystems($maskDeliverySystems)
    {
        /**
         * Process masking
         */
        $allDeliverySystems = array('profile', 'modal', 'email', 'sms');

        // Default mask is all except 'modal'
        if ( empty($maskDeliverySystems) )
        {
            $maskDeliverySystems = array('!modal');
        }

        // Process masking
        $allowedDeliverySystems = array();
        $disallowedDeliverySystems = array();
        $strictDelivery = array();
        foreach($maskDeliverySystems as $maskDeliverySystem)
        {
            if ( 0 === strpos($maskDeliverySystem, '!') )
            {
                $maskDeliverySystem = substr($maskDeliverySystem, 1);
                $disallowedDeliverySystems[] = $maskDeliverySystem;
            }
            else
            {
                if ( 0 === strpos($maskDeliverySystem, '=') )
                {
                    $maskDeliverySystem = substr($maskDeliverySystem, 1);
                    $strictDelivery[] = $maskDeliverySystem;
                }
                $allowedDeliverySystems[] = $maskDeliverySystem;
            }
        }
        if ( 0 === count($allowedDeliverySystems) )
        {
            $allowedDeliverySystems = $allDeliverySystems;
        }
        $maskDeliverySystems = array_diff($allowedDeliverySystems, $disallowedDeliverySystems);

        /**
         * Create delivery systems
         */
        $deliverySystems = array();

        // Системы уведомления на сайте
        if ( null === $maskDeliverySystems || in_array('profile', $maskDeliverySystems) )
        {
            $deliverySystems['profile'] = DeliveryMan_Message_Delivery::instance('notificationProfile');
        }
        if ( null === $maskDeliverySystems || in_array('modal', $maskDeliverySystems) )
        {
            $deliverySystems['modal'] = DeliveryMan_Message_Delivery::instance('notificationModal');
        }

        $userNotificationMethods = array_unique(array_merge($this->getUserNotifactionMethods($this->_user, $this->_addressRecepientOverride), $strictDelivery));

        // Электронная почта
        if ( (null === $maskDeliverySystems || in_array('email', $maskDeliverySystems)) && in_array('email', $userNotificationMethods) )
        {
            $deliverySystems['email'] = DeliveryMan_Message_Delivery::instance($this->getEmailDriver());
        }

        // Сотовый телефон
        if ( (null === $maskDeliverySystems || in_array('sms', $maskDeliverySystems)) && in_array('sms', $userNotificationMethods) )
        {
            $deliverySystems['sms'] = DeliveryMan_Message_Delivery::instance($this->getSmsDriver());

            // FIXME: Временный фикс
            if ( 'smsIbatelecom' == $deliverySystems['sms']->getDriver() && isset($this->_config['drivers']['smsIbatelecom']['notSupportedOperators']) )
            {
                $phone = $deliverySystems['sms']->getAddressRecepient($this->_user, $this->_addressRecepientOverride);
                $phonePrefixesNotSupportedByIbatelecom = array('373', '770');
                if ( preg_match('/^(' . implode('|', $this->_config['drivers']['smsIbatelecom']['notSupportedOperators']) . ')/', $phone) )
                {
                    $deliverySystems['sms'] = DeliveryMan_Message_Delivery::instance(Arr::get($this->_config, 'drivers.smsIbatelecom', 'smsSmsc'));
                }
            }
        }

        // Носимые компьютеры (умные часы, очки)
        //if ( (null === $maskDeliverySystems || in_array('wearable', $maskDeliverySystems)) && $this->_user->profile->notificationSms )
        //{
        //    $deliverySystems['wearable'] = DeliveryMan_Message_Delivery::instance($this->_config['default']['wearable']);
        //}

        // Отладка
        if ( (null === $maskDeliverySystems || in_array('null', $maskDeliverySystems)) )
        {
            if ( in_array('email', $userNotificationMethods) )
            {
                $deliverySystems['null-email'] = DeliveryMan_Message_Delivery::instance('emailNull');
            }

            if ( in_array('sms', $userNotificationMethods) )
            {
                $deliverySystems['null-sms'] = DeliveryMan_Message_Delivery::instance('smsNull');
            }
        }

        return $deliverySystems;
    }

    /**
     *
     * @return array
     */
    public static function getUserNotifactionMethods(Model_User $user, array $addressRecepientOverride = array())
    {
        $notificationMethods = array();
        $notice = $user->profile->notice;

        if ( 'nothing' == $notice )
        {
            $notice = 'all';
        }
        elseif ( 'auto' == $notice && null !== $user->profile->email )
        {
            $notice = 'email';
        }
        elseif ( 'auto' == $notice && null !== $user->profile->mobile )
        {
            $notice = 'mobile';
        }

        if ( 'all' == $notice )
        {
            if ( $user->profile->email || isset($addressRecepientOverride['email']) )
            {
                $notificationMethods[] = 'email';
            }

            if ( $user->profile->mobile || isset($addressRecepientOverride['sms']) )
            {
                $notificationMethods[] = 'sms';
            }
        }
        elseif ( 'email' == $notice )
        {
            $notificationMethods[] = 'email';
        }
        elseif ( 'mobile' == $notice )
        {
            $notificationMethods[] = 'sms';
        }

        return $notificationMethods;
    }

    /**
     *
     * @param string|array $messagetemplateNames
     * @param array|null $maskDeliverySystems
     * @param null $time
     * @return DeliveryMan_Message
     * @throws DeliveryMan_Exception_MessagetemplateNotFound
     * @throws Kohana_Exception
     */
    public function sendMessage($messagetemplateNames, array $maskDeliverySystems = null, /* implicit */ $time = null)
    {
        $time = isset($time) ? $time : time();

        $messagetemplate = $this->findMessagetemplate($messagetemplateNames);
        if ( ! $messagetemplate->loaded() )
        {
            $this->reportMessagetemplateNotFound($messagetemplateNames);
            throw new DeliveryMan_Exception_MessagetemplateNotFound('Шаблон сообщений :messagetemplateNames не найден', array(
                ':messagetemplateNames' => json_encode($messagetemplateNames),
            ));
        }

        $this->_driverOverride = array();
        $this->setDriverOverride($messagetemplate->emailDriverOverride, $messagetemplate->smsDriverOverride);

        $deliverySystems = $this->initDeliverySystems($maskDeliverySystems);


        foreach($deliverySystems as $notificationMethod => $deliverySystem)
        {
            $addressRecepient = $deliverySystem->getAddressRecepient($this->_user, $this->_addressRecepientOverride);
            $domain = substr(strrchr($addressRecepient, '@'), 1);
            if(in_array($domain, $deliverySystem->getSpamFilter()))
            {
                $deliverySystem->setExistInSpamFilter();
                $this->_useDefaultMirror = true;
                break;
            }
        }

        $params = array();

        $params['PROJECT'] = PROJECT;
        $params['MAIN_MIRROR'] = Model_Index::GSCMS_CONFIG('global.main_mirror');
        $params['LAST_MIRROR'] = null !== $this->_lastMirrorOverride ? $this->_lastMirrorOverride : $this->_user->last_mirror;

        if($this->_useDefaultMirror)
        {
            $params['LAST_MIRROR'] = $params['DOMAIN'] = $this->getDomain();
        }
        elseif ( $this->hasEmailPhpMirror($deliverySystems))
        {
            $params['DOMAIN'] = $params['LAST_MIRROR'];
        }
        else
        {
            $params['DOMAIN'] = $this->getDomain();
        }
        $_SERVER['DOMAIN'] = $params['DOMAIN']; // TODO: this is too hacky
        $params['URL'] = 'http://' . $params['DOMAIN'];
        $params['YEAR'] = date('Y');
        $params['BALANCE'] = floor($this->_user->wallet->balance) . $this->_user->currency->sign3;

        $params = array_merge((array)$this->_params, $params, (array)Arr::get($this->_config, 'additional_params'));

        $message = ORM::factory('Message');
        $message->user_id = $this->_user->id;
        $message->messagetemplate_id = $messagetemplate->id;
        $message->params = $params;
        $message->created_at = date('Y-m-d H:i:s', $time);
        if ( null !== $this->_ttl )
        {
            $message->expire_at = date('Y-m-d H:i:s', $time + $this->_ttl);
        }
        $message->save();

        $this->deliverMessage($deliverySystems, $message, $time);

        return $message;
    }

    protected function hasEmailPhpMirror(array $deliverySystems)
    {
        foreach($deliverySystems as $deliverySystem)
        {
            if ( 'emailPhpMirror' == $deliverySystem->getDriver() )
            {
                return true;
            }
        }

        return false;
    }

    /**
     *
     */
    protected function setDriverOverride($emailDriverOverride = null, $smsDriverOverride = null)
    {
        $this->_driverOverride['email'] = $emailDriverOverride;
        $this->_driverOverride['sms'] = $emailDriverOverride;
    }

    /**
     *
     * @return string
     */
    protected function getEmailDriver()
    {
        return Arr::get($this->_driverOverride, 'email', $this->_config['default']['email']);
    }

    /**
     *
     * @return string
     */
    protected function getSmsDriver()
    {
        return Arr::get($this->_driverOverride, 'sms', $this->_config['default']['sms']);
    }

    /**
     * @param string|array $messagetemplateNames
     */
    public static function findMessagetemplate($messagetemplateNames)
    {
        foreach((array)$messagetemplateNames as $messagetemplateName)
        {
            $messagetemplate = ORM::factory('Messagetemplate')
                ->where('name', '=', $messagetemplateName)
                ->where('enabled', '=', 1)
                ->find();

            if ( $messagetemplate->loaded() )
            {
                break;
            }
        }

        return $messagetemplate;
    }

    /**
     *
     * @param string|array $messagetemplateNames
     * @return void
     */
    protected function reportMessagetemplateNotFound($messagetemplateNames)
    {
        $messagetemplateNotFound = Arr::get($this->_config, 'messagetemplateNotFound');
        if ( null === $messagetemplateNotFound )
        {
            return;
        }

        foreach($messagetemplateNotFound as $reportReciever)
        {
            try
            {
                DeliveryMan_Message_Delivery::instance($reportReciever[0])
                    ->sendMessage(null, $reportReciever[1], 'Шаблон сообщений не найден', 'Не найден ни один из шаблонов сообщений: "' . implode('", "', (array)$messagetemplateNames) . '", пользователь: ' . $this->_user->id);
            }
            catch(Exception $e)
            {
            }
        }
    }

    /**
     *
     * @return string
     */
    public static function getDomain()
    {
        $emailDriver = Model_Index::GSCMS_CONFIG('deliveryman.default.email');

        try
        {
            $sender_email = Model_Index::GSCMS_CONFIG('deliveryman.drivers.' . $emailDriver . '.sender_email');
            $domain = substr(strrchr($sender_email, '@'), 1);
        }
        catch(Exception $e)
        {
            $domain = Arr::get($_SERVER, 'HTTP_HOST');
        }

        return $domain;
    }

    /**
     * Message delivery using specified delivery systems
     * @param $deliverySystems
     * @param Model_Message $message
     * @param null $time
     * @return DeliveryMan_Message
     * @throws Exception
     * @throws Kohana_Exception
     */
    protected function deliverMessage($deliverySystems, Model_Message $message, /* implicit */ $time = null)
    {
        $messagetemplate = $message->template;
        $params = json_decode($message->params, true);
        $subject = $this->substituteBody($messagetemplate->messageheader->subject, $params, $messagetemplate->name);

        // Message bodies cache
        $bodies = array();

        // Deliver message using all specified delivery systems
        foreach($deliverySystems as $notificationMethod => $deliverySystem)
        {
            // Get address recepient
            $addressRecepient = $deliverySystem->getAddressRecepient($this->_user, $this->_addressRecepientOverride);
            if ( empty($addressRecepient) )
            {
                throw new Kohana_Exception('Trying to send message to unknown address message_id=:message_id, user_id=:user_id', array(
                    ':message_id' => $message->id,
                    ':user_id' => $message->user->id,
                ));
            }

            // For each message there is a certain format of a message:
            // for Email - long HTML message
            // for SMS and Profile -  short message
            // for Modal - HTML-body of Bootstrap modal
            $bodyType = $deliverySystem->getBodyType();

            // If message body is not yet in the cache, update the cache
            if ( ! isset($bodies[$bodyType]) )
            {
                $bodyNotSubstituted = null;
                if($bodyType == 'body')
                {
                    $bodyNotSubstituted = $messagetemplate->messagebody->body;
                }
                // Short message should be always set, so lets fallback to it
                if ( null === $bodyNotSubstituted )
                {
                    $bodyNotSubstituted = $messagetemplate->bodyShort;
                }
                if ( null === $bodyNotSubstituted )
                {
                    Kohana::$log->add(Log::ERROR, 'Could not find text for message body message_id = :message_id, messagetemplate_name = :messagetemplate_name, type = :type', array(
                        ':message_id' => $message->id,
                        ':messagetemplate_name' => $messagetemplate->name,
                        ':type' => $bodyType,
                    ));
                }
                $bodies[$bodyType] = $this->substituteBody($bodyNotSubstituted, $params, $messagetemplate->name);
            }
            $body = $bodies[$bodyType];

            // Special substitution for {{Unsubscribe}} and {{WebLetter}}
            $body = $this->specialSubstitution($body, $deliverySystem->getDriver());

            // Интеллектуальная система альтернативной доставки сообщений
            // Если в течении часа предыдущее сообщение не было доставлено, то меняем систему доставки на альтернативную
            $modelMessage_delivery = ORM::factory('Message_Delivery')
                ->with('message')
                ->where('message.messagetemplate_id', '=', $message->messagetemplate_id)
                ->where('message.created_at', '>=', date('Y-m-d H:i:s', $time - Date::HOUR))
                ->where('message.user_id', '=', $message->user_id)
                ->where('message_delivery.driver', 'LIKE', $notificationMethod . '%')
                ->order_by('message_delivery.id', 'DESC')
                ->find();
            if ( $modelMessage_delivery->loaded()
                && $deliverySystem->getDriver() == $modelMessage_delivery->driver
                && null !== $modelMessage_delivery->error
                && null !== ($driverFallback = Arr::path($this->_config, 'drivers.' . $deliverySystem->getDriver() . '.driverFallback')) )
            {
                if ( null === Arr::path($this->_config, 'drivers.' . $driverFallback . '.driverFallback') )
                {
                    Arr::set_path($this->_config, 'drivers.' . $driverFallback . '.driverFallback', $deliverySystem->getDriver());
                }

                // This is exacly Log::ERROR, because runtime should not enter here too often
                Kohana::$log->add(Log::ERROR, 'DeliveryMan_Driver_:Driver->sendMessage(:message_id, :addressRecepient) Fallback to driver :driverFallback by Intellectual alternative delivery system selector', array(
                    ':Driver' => ucfirst($deliverySystem->getDriver()),
                    ':message_id' => $message->id,
                    ':addressRecepient' => $addressRecepient,
                    ':driverFallback' => $driverFallback,
                ));

                $deliverySystem = DeliveryMan_Message_Delivery::instance($driverFallback);
            }

            if ( 0 === strpos($deliverySystem->getDriver(), 'email') && ! $this->_user->isEmailExists($addressRecepient) )
            {
                throw new RuntimeException('Tried to send email to not existent email box ' . $addressRecepient);
            }

            $this->sendThroughDeliverySystem($deliverySystem, $messagetemplate, $message, $params, $addressRecepient, $subject, $body);
        }

        return $this;
    }

    protected function sendThroughDeliverySystem($deliverySystem, $messagetemplate, $message, $params, $addressRecepient, $subject, $body)
    {
        if ( 'emailPhpMirror' == $deliverySystem->getDriver() )
        {
            $deliverySystem->setLastMirror($params['LAST_MIRROR']);
        }
        if ( in_array($deliverySystem->getDriver(), array('emailNull', 'emailPhp', 'emailPhpMirror')) )
        {
            $deliverySystem->setPostmasterMsgtype($messagetemplate->postmaster_msgtype);
        }

        // Send message using specified delivery system
        try
        {
            $deliverySystem->sendMessage($message->id, $addressRecepient, $subject, $body);
        }
        catch(Exception $e)
        {
            Kohana::$log->add(Log::ERROR, 'DeliveryMan_Driver_:Driver->sendMessage(:message_id, :addressRecepient) :errorMessage (:errorFile::errorLine)', array(
                ':Driver' => ucfirst($deliverySystem->getDriver()),
                ':message_id' => $message->id,
                ':addressRecepient' => $addressRecepient,
                ':errorMessage' => $e->getMessage(),
                ':errorFile' => $e->getFile(),
                ':errorLine' => $e->getLine(),
            ));

            $driverFallback = Arr::path($this->_config, 'drivers.' . $deliverySystem->getDriver() . '.driverFallback');
            if ( null === $driverFallback )
            {
                throw $e;
            }

            Kohana::$log->add(Log::NOTICE, 'DeliveryMan_Driver_:Driver->sendMessage(:message_id, :addressRecepient) I use fallback driver :driverFallback', array(
                ':Driver' => ucfirst($deliverySystem->getDriver()),
                ':message_id' => $message->id,
                ':addressRecepient' => $addressRecepient,
                ':driverFallback' => $driverFallback,
            ));

            if(--$this->_tries)
            {
                $deliverySystem = DeliveryMan_Message_Delivery::instance($driverFallback);
                $this->sendThroughDeliverySystem($deliverySystem, $messagetemplate, $message, $params, $addressRecepient, $subject, $body);
            }
            else
            {
                Kohana::$log->add(Log::NOTICE, 'DeliveryMan_Driver_:Driver->sendMessage(:message_id, :addressRecepient) Amount of tries exceeded.', array(
                    ':Driver' => ucfirst($deliverySystem->getDriver()),
                    ':message_id' => $message->id,
                    ':addressRecepient' => $addressRecepient,
                    ':driverFallback' => $driverFallback,
                ));
            }
        }
    }

    /**
     *
     */
    protected function specialSubstitution($body, $driver)
    {
        $specialSubstitution = Arr::get($this->_config, 'specialSubstitution');
        if ( null === $specialSubstitution || ! isset($specialSubstitution['Unsubscribe']) || ! isset($specialSubstitution['WebLetter']) )
        {
            return $body;
        }

        $body = preg_replace_callback('/{{(Unsubscribe|WebLetter)}}/', function($matches) use ($specialSubstitution, $driver)
        {
            $param = $matches[1];
            if ( 'Unsubscribe' == $param )
            {
                return $specialSubstitution['Unsubscribe'];
            }
            elseif ( 'WebLetter' == $param )
            {
                if ( 'emailUnisender' != $driver )
                {
                    // TODO: currently WebLetter feature is not implemented on project
                    return '';
                }

                return $specialSubstitution['WebLetter'];
            }
            else
            {
                throw new ErrorException('Special substitution for ' . $param . ' is not implemented');
            }
        }, $body);

        return $body;
    }

    /**
     *
     */
    protected function substituteBody($body, array $params, $messagetemplate_name)
    {
        $body = preg_replace_callback('/{{([^}]+)}}/', function($matches) use ($params, $messagetemplate_name)
        {
            $param = $matches[1];
            if ( isset($params[$param]) )
            {
                return $params[$param];
            }
            elseif ( ! in_array($param, array('Unsubscribe', 'WebLetter', 'UnsubscribeUrl', 'WebLetterUrl')) )
            {
                if ( $this->_testMessage )
                {
                    return '$' . $param;
                }

                Kohana::$log->add(Log::ERROR, 'DELIVERYMAN\MESSAGE\SUBSTITUTEBODY\ERROR COULD NOT FIND SUBSTITUTION FOR :PARAM IN TEMPLATE :MESSAGETEMPLATE_NAME', array(
                    ':PARAM' => $param,
                    ':MESSAGETEMPLATE_NAME' => $messagetemplate_name,
                ));
            }

            return $matches[0];
        }, $body);

        return $body;
    }

    public function getNotificationModal($idMessageDelivery)
    {
        return DeliveryMan_Message_Delivery::instance('notificationModal')->getNotificationModal($this->_user, $idMessageDelivery);
    }

    public function getNotificationModalNext()
    {
        return DeliveryMan_Message_Delivery::instance('notificationModal')->getNotificationModalNext($this->_user);
    }

    public function getNotificationProfile($idMessageDelivery)
    {
        return DeliveryMan_Message_Delivery::instance('notificationProfile')->getNotificationProfile($this->_user, $idMessageDelivery);
    }

    public function getNotificationsProfile($offset = 0, $count = 10)
    {
        return DeliveryMan_Message_Delivery::instance('notificationProfile')->getNotificationsProfile($this->_user, $offset, $count);
    }

    public function getCountNotificationsProfile()
    {
        return DeliveryMan_Message_Delivery::instance('notificationProfile')->getCountNotificationsProfile($this->_user);
    }


}
