<?php

class ParsePostfixLogs_LogFile extends Kohana_Log_File {

    public static function logWriter($message, $line = null, $file = null)
    {
        $dir = Cron_ParseMailingPostfixLogs::getConfig()->get('logDir');

        $logWriter = ($line && $file) ? new Kohana_Log_File($dir) : new static($dir);

        $message = [
            'time'  => time(),
            'level' => Kohana_Log::INFO,
            'body'  => $message,
            'file'  => $file,
            'line'  => $line,
        ];

        $logWriter->write([
            $message,
        ]);
    }

    public function format_message(array $message, $format = "time: body")
    {
        $message['time'] = Date::formatted_time('@' . $message['time'], Log_Writer::$timestamp, Log_Writer::$timezone, true);
        $message['level'] = $this->_log_levels[ $message['level'] ];

        $string = strtr($format, array_filter($message, 'is_scalar'));

        if (isset($message['additional']['exception']))
        {
            // Re-use as much as possible, just resetting the body to the trace
            $message['body'] = $message['additional']['exception']->getTraceAsString();
            $message['level'] = $this->_log_levels[ Log_Writer::$strace_level ];

            $string .= PHP_EOL . strtr($format, array_filter($message, 'is_scalar'));
        }

        return $string;
    }
}