<?php

/**
 * A class to parse 'maillog' lines and add them to the 'Db helper' queue
 */
class ParsePostfixLogs_MaillogImporter_Parser {

    /**
     * @var ParsePostfixLogs_MaillogImporter_Writer_WriterInterface
     */
    protected $_writer = null;

    public function __construct(ParsePostfixLogs_MaillogImporter_Writer_WriterInterface $writer)
    {
        $this->_writer = $writer;
    }

    public function parseLine($line)
    {
        if (!$this->_parseMessageIds($line))
        {
            $this->_parseSendStatuses($line);

            return false;
        }

        return true;
    }

    /**
     * Get message IDs: Postfix message ID and NC message ID, including date updated.
     * @param  string $line
     * @return boolean
     */
    protected function _parseMessageIds($line)
    {
        if (preg_match('/' . Cron_ParseMailingPostfixLogs::getConfig()->get('dateTimeRegex')
            . Cron_ParseMailingPostfixLogs::getConfig()->get('MessageIdRegex')
            . Cron_ParseMailingPostfixLogs::getConfig()->get('messageIdHost')
            . '>)/', $line, $matches)
        )
        {
            $postfixMessageId = $matches[3];
            $this->_writer->addData($postfixMessageId, array(
                'email_message_id' => $matches[4],
                'date_updated'     => ParsePostfixLogs_MaillogImporter_DateConvertor::createFromPostfix($matches[1]),
                'status'           => 'unknown',
                'info'             => '',
            ));

            return true;
        }

        return false;
    }

    /**
     * Get statuses: success/deferred/bounced
     * @param  string $line
     * @return boolean
     */
    protected function _parseSendStatuses($line)
    {
        if (preg_match('/' . Cron_ParseMailingPostfixLogs::getConfig()->get('dateTimeRegex')
            . Cron_ParseMailingPostfixLogs::getConfig()->get('SendStatusesRegex')
            . '/', $line, $matches)
        )
        {
            $this->_updateData($matches);

            return true;
        }

        return false;
    }

    protected function _updateData(array $matches)
    {
        if (empty($matches[5]))
        {
            $matches[5] = null;
        }
        if ($matches[4] == 'bounced')
        {
            // укороченный info
            if (preg_match('/(host [a-z.].*)(\[[0-9].*)(said:.)(.*)/', $matches[5], $match))
            {
                $matches[5] = $match[4];
            }
        }
        $postfixMessageId = $matches[3];
        $this->_writer->updateData($postfixMessageId, array(
            'date_updated' => ParsePostfixLogs_MaillogImporter_DateConvertor::createFromPostfix($matches[1]),
            'status'       => $matches[4],
            'info'         => $matches[5],
        ));
    }
}