<?php
use Predis\Collection\Iterator;

/**
 * Email log writer for redis
 */
class ParsePostfixLogs_MaillogImporter_Writer_RedisMessage extends ParsePostfixLogs_MaillogImporter_Writer_WriterInterface {

    const DATETIME_FORMAT_MYSQL = 'Y-m-d H:i:s';

    private $_redis;

    private $_db;

    public function __construct()
    {
        $options = [];
        $this->_redis = new Predis\Client(Cron_ParseMailingPostfixLogs::getConfig()['redis']['params'], $options);
        $this->_db = Cron_ParseMailingPostfixLogs::getConfig()['redis']['db'];
    }

    public function addData($postfixMessageId, array $mailing)
    {
        $mailing['date_updated'] = $mailing['date_updated']->format(self::DATETIME_FORMAT_MYSQL);
        $this->_redis->select($this->_db);
        $this->_redis->hmset('messageDelivery', 'postfix_message_id:' . $postfixMessageId, serialize($mailing));
    }

    public function updateData($postfixMessageId, array $mailing)
    {
        if ($this->_redis->hexists('messageDelivery', 'postfix_message_id:' . $postfixMessageId))
        {
            $result = unserialize($this->_redis->hget('messageDelivery', 'postfix_message_id:' . $postfixMessageId));
            $mailing += $result;
            $this->addData($postfixMessageId, $mailing);
        }
    }

    public function updateMessages()
    {
        $this->_redis->select($this->_db);

        foreach (new Iterator\HashKey($this->_redis, 'messageDelivery') as $field => $value)
        {
            $this->fillDeliveryTable(unserialize($value));
        }
        $this->_redis->del('messageDelivery');
    }

    function fillDeliveryTable(array $log)
    {
    }

}
