<?php

abstract class ParsePostfixLogs_MaillogImporter_Writer_WriterInterface {

    /**
     * Add new message to be logged
     * @param string $postfixMessageId
     * @param array $data
     */
    abstract function addData($postfixMessageId, array $data);

    /**
     * Update the data of an existing message
     * @param string $postfixMessageId
     * @param array $data
     */
    abstract function updateData($postfixMessageId, array $data);

    /**
     * Create data to the 'Mailings' table
     *
     */
    abstract function updateMessages();

    /**
     * Fill 'Mailings' table
     * @param array $log
     * email_message_id
     * date_updated
     * status
     * info
     */
    abstract function fillDeliveryTable(array $log);
}