<?php
use Predis\Collection\Iterator;

/**
 * Email log writer for redis
 */
class ParsePostfixLogs_MaillogImporter_Writer_Redis extends ParsePostfixLogs_MaillogImporter_Writer_WriterInterface {

    const DATETIME_FORMAT_MYSQL = 'Y-m-d H:i:s';

    private $_redis;

    private $_db;

    public function __construct()
    {
        $options = [];
        $this->_redis = new Predis\Client(Cron_ParseMailingPostfixLogs::getConfig()['redis']['params'], $options);
        $this->_db = Cron_ParseMailingPostfixLogs::getConfig()['redis']['db'];
    }

    public function addData($postfixMessageId, array $mailing)
    {
        $mailing['date_updated'] = $mailing['date_updated']->format(self::DATETIME_FORMAT_MYSQL);
        $this->_redis->select($this->_db);
        $this->_redis->hmset('mailings', 'postfix_message_id:' . $postfixMessageId, serialize($mailing));
    }

    public function updateData($postfixMessageId, array $mailing)
    {
        if ($this->_redis->hexists('mailings', 'postfix_message_id:' . $postfixMessageId))
        {
            $result = unserialize($this->_redis->hget('mailings', 'postfix_message_id:' . $postfixMessageId));
            $mailing += $result;
            $this->addData($postfixMessageId, $mailing);
        }
    }

    public function updateMessages()
    {
        $this->_redis->select($this->_db);

        foreach (new Iterator\HashKey($this->_redis, 'mailings') as $field => $value)
        {
            $this->fillDeliveryTable(unserialize($value));
        }
        $this->_redis->del('mailings');
    }

    function fillDeliveryTable(array $log)
    {
        $mailings = ORM::factory('MailingDelivery')->where('letter_id', '=', $log['email_message_id'])->find();
        if ($mailings->loaded())
        {
            if ($log['status'] == 'bounced')
            {
                $mailings->status = Model_MailingDelivery::STATUS_BOUNCED;
                $mailings->details = $log['info'];
            }
            elseif ($log['status'] == 'deferred')
            {
                $mailings->status = Model_MailingDelivery::STATUS_QUEUE;
                $mailings->details = $log['info'];
            }
            elseif ($log['status'] == 'sent')
            {
                $mailings->status = Model_MailingDelivery::STATUS_SENT;
                $mailings->sent_at = $log['date_updated'];
            }
            elseif ($log['status'] == 'unknown')
            {
                return;
//                $mailings->status = Model_MailingDelivery::STATUS_ERROR;
//                $mailings->details = 'Статус не определен';
            }
            $mailings->save();
        }
        else
        {
            ParsePostfixLogs_LogFile::logWriter('Не удалось обновить письмо с $letter_id = ' . $log['email_message_id'] . ', возможно его не существует');
        }
    }
}
