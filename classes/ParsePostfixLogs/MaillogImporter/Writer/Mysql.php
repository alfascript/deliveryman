<?php

/**
 * Email log writer for MySQL
 */
class ParsePostfixLogs_MaillogImporter_Writer_MySql extends ParsePostfixLogs_MaillogImporter_Writer_WriterInterface {

    const DATETIME_FORMAT_MYSQL = 'Y-m-d H:i:s';

    /**
     * @var DB::Builder
     */
    protected $_db = null;

    /**
     * @var array
     */
    protected $_inserts = array();

    /**
     * @var string
     */
    protected $_tableName = '';

    /**
     * @var integer
     */
    protected $_batchInsertsCount = 1;

    public function __construct()
    {
        $this->_batchInsertsCount = Cron_ParseMailingPostfixLogs::getConfig()['MySql']['batchInsertsCount'];
        $this->_db = Database::instance('project_db');
        $this->_tableName = Cron_ParseMailingPostfixLogs::getConfig()['MySql']['temporaryTable'];
    }

    public function __destruct()
    {
        $this->_flushInserts();
    }

    public function addData($postfixMessageId, array $data)
    {
        $this->_inserts[$postfixMessageId] = $data;
        if (count($this->_inserts) >= $this->_batchInsertsCount)
        {
            $this->_flushInserts();
        }
    }

    public function updateData($postfixMessageId, array $data)
    {
        if (array_key_exists($postfixMessageId, $this->_inserts))
        {
            if (array_key_exists('info', $this->_inserts[$postfixMessageId]) && ! empty($data['info']))
            {
                $oldInfo = $this->_inserts[$postfixMessageId]['info'];
                $data['info'] = $oldInfo . (! empty($oldInfo) ? "\n" : '') . $data['info'];
            }
            $this->_inserts[$postfixMessageId] = array_replace($this->_inserts[$postfixMessageId], $data);
        }
        else
        {
            $this->_updateEmailData($postfixMessageId, $data['status'], $data['info']);
        }
    }

    protected function _updateEmailData($postfixMessageId, $status, $info)
    {
        $sql = 'UPDATE `' . $this->_tableName . '` SET `status` = ' . $this->_db->quote($status);
        // Update 'info' only when there's something to add
        if ( ! empty($info))
        {
            $info = $this->_db->quote($info);
            $sql .= ', `info` = ' . $info;
        }
        $sql .= ' WHERE `postfix_message_id` = ' . $this->_db->quote($postfixMessageId);

        $this->_db->query(Database::UPDATE, $sql);
    }

    protected function _flushInserts()
    {
        // Don't do anything if there's nothing to insert
        if (empty($this->_inserts))
        {
            return;
        }
        // Generate SQL statement
        $sql = 'INSERT INTO `' . $this->_tableName . '` '
            . '(`email_message_id`, `postfix_message_id`, `date_updated`, `status`, `info`) VALUES' . "\r\n";
        $insertsCount = count($this->_inserts);
        $i = 0;
        foreach ($this->_inserts as $postfixMessageId => $insertData)
        {
            // Generate values
            $sql .= '('
                . $this->_db->quote($insertData['email_message_id']) . ', '
                . $this->_db->quote($postfixMessageId) . ', '
                . $this->_db->quote($insertData['date_updated']->format(self::DATETIME_FORMAT_MYSQL)) . ', '
                . $this->_db->quote($insertData['status']) . ', '
                . $this->_db->quote($insertData['info'])
                . ')';
            // Add commas only when needed
            $sql .= (++$i == $insertsCount) ? "\r\n" : ",\r\n";
        }
        // If key already exists do an UPDATE
        $sql .=
            ' ON DUPLICATE KEY UPDATE '
            . '`email_message_id` = VALUES(`email_message_id`), '
            . '`postfix_message_id` = VALUES(`postfix_message_id`), '
            . '`date_updated` = VALUES(`date_updated`), '
            . '`status` = VALUES(`status`), '
            . '`info` = VALUES(`info`)';

        // Execute INSERT statement and cleanup 'inserts' array
        $this->_db->query(Database::INSERT, $sql);

        $this->_inserts = array();
    }

    public function updateMessages()
    {
        // выбрать данные из таблицы mailings_delivery_postfixlogs
        $sql = 'SELECT * FROM `' . $this->_tableName . '`';
        $logs = $this->_db->query(Database::SELECT, $sql);

        // заполнить этими данными таблицу mailings_delivery
        foreach ($logs as $log)
        {
            $this->fillDeliveryTable($log);
        }
        // удаляем все данные во временной таблице
        $sql = 'DELETE FROM `' . $this->_tableName . '`';
        $dbStatement = $this->_db->query(Database::DELETE, $sql);
        if ($dbStatement === 0)
        {
            ParsePostfixLogs_LogFile::logWriter('Не получается выполнить команду DELETE и удалить временную таблицу', __LINE__, __FILE__);
        }
    }

    function fillDeliveryTable(array $log)
    {
        $mailings = ORM::factory('MailingDelivery')->where('letter_id', '=', $log['email_message_id'])->find();
        if ($mailings->loaded())
        {
            if ($log['status'] == 'bounced')
            {
                $mailings->status = Model_MailingDelivery::STATUS_BOUNCED;
                $mailings->details = $log['info'];
            }
            elseif ($log['status'] == 'deferred')
            {
                $mailings->status = Model_MailingDelivery::STATUS_QUEUE;
                $mailings->details = $log['info'];
            }
            elseif ($log['status'] == 'sent')
            {
                $mailings->status = Model_MailingDelivery::STATUS_SENT;
                $mailings->sent_at = $log['date_updated'];
            }
            elseif ($log['status'] == 'unknown')
            {
                return;
//                $mailings->status = Model_MailingDelivery::STATUS_ERROR;
//                $mailings->details = 'Статус не определен';
            }
            $mailings->save();
        }
        else
        {
            ParsePostfixLogs_LogFile::logWriter('Не удалось обновить письмо с $letter_id = ' . $log['email_message_id'] . ', возможно его не существует');
        }
    }
}
