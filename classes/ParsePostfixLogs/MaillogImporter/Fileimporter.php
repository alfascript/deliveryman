<?php

/**
 * Read the 'maillog' file and pass it to the 'parser'
 */
class ParsePostfixLogs_MaillogImporter_FileImporter {

    /**
     * @var resource
     */
    protected $_fileHandle = null;

    /**
     * @var string
     */
    protected $_fileName = '';

    public function __construct($fileName = '/var/log/mail.log')
    {
        $this->_fileName = $fileName;

        // Load 'maillog' file
        if (!file_exists($this->_fileName))
        {
            ParsePostfixLogs_LogFile::logWriter('Файла "' . $this->_fileName . '" не существует', __LINE__, __FILE__);
            throw new ParsePostfixLogs_MaillogImporter_MaillogException('Maillog file "' . $this->_fileName . '" does not exist');
        }
        if (!is_readable($fileName))
        {
            ParsePostfixLogs_LogFile::logWriter('Невозможно прочитать файл "' . $this->_fileName . '"', __LINE__, __FILE__);
            throw new ParsePostfixLogs_MaillogImporter_MaillogException('Maillog file "' . $this->_fileName . '" is not readable');
        }
        $this->_fileHandle = fopen($this->_fileName, 'r');
        if (empty($this->_fileHandle))
        {
            ParsePostfixLogs_LogFile::logWriter('Невозможно открыть файл "' . $this->_fileName . '" для чтения', __LINE__, __FILE__);
            throw new ParsePostfixLogs_MaillogImporter_MaillogException('Could not open maillog file "' . $this->_fileName . '" for reading');
        }
    }

    public function import(ParsePostfixLogs_MaillogImporter_Writer_WriterInterface $writer)
    {
        $dateTimeRegex = Cron_ParseMailingPostfixLogs::getConfig()->get('dateTimeRegex');
        // Get all the data from the 'maillog' and add it to the database
        $MaillogImporter = new ParsePostfixLogs_MaillogImporter_Parser($writer);
        $count = array('lettercount' => 0, 'linecount' => 0);
        while (($line = fgets($this->_fileHandle)) != false)
        {
            if (preg_match('/' . $dateTimeRegex . '/', $line, $matches))
            {
                if($MaillogImporter->parseLine($line))
                {
                    $count['lettercount']++;
                }
                $count['linecount'] ++;
            }
            else
            {
                ParsePostfixLogs_LogFile::logWriter('дата в строке "' . $line . '" не соответствует регулярному выражению', __LINE__, __FILE__);
                throw new ParsePostfixLogs_MaillogImporter_MaillogException('Could not match line with the date regex: "' . $line . '"');
            }
        }
        return $count;
    }

    public function __destruct()
    {
        // Close 'maillog' file
        if (!empty($this->_fileHandle))
        {
            fclose($this->_fileHandle);
        }
    }
}