<?php defined('SYSPATH') or die();
/**
 * Истечение срока доставки сообщения (только для драйверов notificationModal и notificationToaster)
 */
class Cron_ExpireMessageDelivery extends Cron
{

    public function run(array $params)
    {
        $time = Arr::get($params, 'time', time());

        $modelsMessage_Delivery = ORM::factory('Message_Delivery')
            ->with('message')
            ->where('message.expire_at', '<', date('Y-m-d H:i:s', $time))
            ->where('message_delivery.driver', 'IN', array('notificationModal', 'notificationToaster'))
            ->where('message_delivery.delivered_at', 'IS', null)
            ->find_all();
        
        foreach($modelsMessage_Delivery as $modelMessage_Delivery)
        {
            Database::instance()->begin();
            
            $modelMessage_Delivery->reload();
            
            if ( null === $modelMessage_Delivery->delivered_at )
            {
                $modelMessage_Delivery->delete();

                Database::instance()->commit();
            }
            else
            {
                Database::instance()->rollback();
            }
        }
    }
    
}
