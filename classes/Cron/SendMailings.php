<?php defined('SYSPATH') or die();

class Cron_SendMailings extends Cron {

    /*
     * Log writer
     */
    private $logWriter;

    private $lockHandler;

    public function run(array $params)
    {
        $config = (array)Model_Index::GSCMS_CONFIG('deliveryman.drivers.emailPhpMailing');
        $config['sender_name'] = (string)Model_Index::GSCMS_CONFIG('deliveryman.sender_name');
        $now = date('Y-m-d H:i:s');
        /** @var Model_Mailing $mailing */
        $mailing = Model_Mailing::factory('Mailing')
            ->where('scheduled_to', '<', $now)
            ->where('finished_at', 'IS', null)
            ->order_by('scheduled_to', 'ASC')
            ->find();
        if ( ! $mailing->loaded())
        {
            return;
        }
        $this->writeLog('Mailing id #' . $mailing->id, __LINE__);

        if ($this->acquireLock($mailing->id))
        {
            $recipientsList = $this->recipientsList($mailing);

            $this->writeLog('Approximately ' . count($recipientsList) . ' more emails to process', __LINE__);
            foreach ($recipientsList as $recipient)
            {
                if (empty($recipient))
                {
                    continue;
                }
                $recipient = trim($recipient);

                /** @var Model_User $user */
                $user = ORM::factory('User')->where('username', '=', $recipient)->find();
                if ($user->loaded())
                {
                    /** @var Model_User_Profile $userProfile */
                    $userProfile = ORM::factory('Userprofile')->where('user_id', '=', $user->id)->find();
                    if ($userProfile->loaded())
                    {
                        $letter_id = $this->generateMessageId($mailing->id, $user->id);
                        $recipient_email = $userProfile->email;

                        /** @var DeliveryMan_Model_Messagetemplate $messageTemplate */
                        $messageTemplate = ORM::factory('Messagetemplate')
                            ->where('id', '=', $mailing->template_id)->find();

                        $subject = $messageTemplate->messageheader->subject;
                        $body = $messageTemplate->messagebody->body;
                        if ($userProfile->notice_advertisment)
                        {
                            $message = new DeliveryMan_Driver_EmailPhpMailing($config);
                            $message->sendMessage($letter_id, $recipient_email, $subject, $body, null, null, $user, $mailing->id, $mailing->only_confirmed_users);
                        }
                        $mailing->last_recipient = $recipient_email;
                        $mailing->save();
                    }
                }
            }
        }
        else
        {
            $this->writeLog('Couldn\'t acquire lock. Assuming that another process is running ' . $mailing->id, __LINE__);
        }
        $mailing->finished_at = date('Y-m-d H:i:s');
        $mailing->save();
        $this->freeLock();
        $this->writeLog('Good bye', __LINE__);
    }

    private function writeLog($message, $line)
    {
        if (null === $this->logWriter)
        {
            $this->logWriter = new Kohana_Log_File(GSCMSLOGS . 'mailings');
        }
        $message = [
            'time'  => time(),
            'level' => Kohana_Log::INFO,
            'body'  => $message,
            'file'  => __FILE__,
            'line'  => $line,
        ];
        $this->logWriter->write([
            $message,
        ]);
    }

    private function acquireLock($id)
    {
        $this->lockHandler = fopen('/tmp/mailing.lock', 'r+');
        if (flock($this->lockHandler, LOCK_EX))
        {
            ftruncate($this->lockHandler, 0);
            fwrite($this->lockHandler, time() . ' > Mailing id ' . $id);

            return true;
        }

        return false;
    }

    private function freeLock()
    {
        flock($this->lockHandler, LOCK_UN);
    }

    private function recipientsList($mailing)
    {
        if (empty($mailing->started_at))
        {
            $this->writeLog('Starting now.', __LINE__);
            $mailing->started_at = date('Y-m-d H:i:s');
            $mailing->save();
            $list = explode("\n", $mailing->recipients);

            return $list;
        }
        if ($mailing->last_recipient)
        {
            $this->writeLog('Mailing was started at ' . $mailing->started_at . '. Continuing from user ' . $mailing->last_recipient, __LINE__);
            $recipientsList = explode($mailing->last_recipient, $mailing->recipients);
            $list = explode("\n", $recipientsList[1]);
        }
        else
        {
            $list = explode("\n", $mailing->recipients);
        }

        return $list;
    }

    private function generateMessageId($mailing_id, $user_id = null)
    {
        return 'M' . microtime(true) . '.' . $mailing_id . '.' . $user_id;
    }

}