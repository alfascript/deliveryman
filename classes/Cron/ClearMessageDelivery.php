<?php defined('SYSPATH') or die();

/**
 * Clear message delivery
 */
class Cron_ClearMessageDelivery extends Cron
{
    
    public function run(array $params)
    {
        $time = Arr::get($params, 'time', time());

        // Перекладываем устаревшие сообщение в таблицу message_delivery_back
        $modelsMessage_Delivery = ORM::factory('Message_Delivery')
            ->where('created_at', '<=', date('Y-m-d H:i:s', $time - Date::MONTH))
            ->find_all();
        foreach($modelsMessage_Delivery as $modelMessage_Delivery)
        {
            $modelMessage_Delivery_Back = ORM::factory('Message_Delivery_Back');
            foreach($modelMessage_Delivery->table_columns() as $field => $dummy)
            {
                $modelMessage_Delivery_Back->$field = $modelMessage_Delivery->$field;
            }
            $modelMessage_Delivery_Back->save();
            $modelMessage_Delivery->delete();
        }
        
        // Удаляем очень старые сообщение
        DB::delete(ORM::factory('Message_Delivery_Back')->table_name())
            ->where('created_at', '<=', date('Y-m-d H:i:s', $time - 6 * Date::MONTH))
            ->execute();
        
        // Удаляем сообщения не имеющие message_delivery
        $messageDelivery = DB::select(array(DB::expr('DISTINCT message_delivery.message_id'), 'message_id'))
            ->from(array(ORM::factory('Message_Delivery')->table_name(), 'message_delivery'));
        DB::delete(ORM::factory('Message')->table_name())
            ->where('created_at', '<', date('Y-m-d H:i:s', $time - Date::MONTH))
            ->where('id', 'NOT IN', $messageDelivery)
            ->execute();
    }
    
}