<?php defined('SYSPATH') or die();

/*
 * Парсинг логов Postfix(по рассылке goldenstar/mailings_delivery)
 * для unix-cron (goldenstar/crontab) -
 * пути к файлам,
 * пути к логам(нужно создать соотв. папку),
 * регулярные выражения используемые в скрипте,
 * выбор инструмента обработки данных MySql/redis,
 * находятся в конфиге parsepostfix.php
 * 
 * код вставки задачи для cron лежит в sql/create_cron_task.sql
 * */

class Cron_ParseMailingPostfixLogs extends Cron {

    private static $_config;
    
    public static function getConfig()
    {
        if (empty(self::$_config))
        {
            self::$_config = Kohana::$config->load('parsepostfix');
        }
        return self::$_config;
    }
    
    public function run(array $params)
    {
        try
        {
            $twoDaysAgo = new DateTime('2 days ago');
            $messages = Model_MailingDelivery::factory('MailingDelivery')
                ->where('status', 'IS', NULL)
                ->where('created_at', '>', $twoDaysAgo->format('Y-m-d H:i:s'))
                ->count_all();
            if($messages == 0)
            {
                return true;
            }
            $maillogPaths = self::getConfig()->get('MaillogPathes');
            foreach ($maillogPaths as $maillogPath){
                // Create database writer
                $dbWriterClass = 'ParsePostfixLogs_MaillogImporter_Writer_' . self::getConfig()->get('dbWriterClass');
                $dbWriter = new $dbWriterClass();
                // Create file importer and import data via the database writer
                $fileImporter = new ParsePostfixLogs_MaillogImporter_FileImporter($maillogPath);
                ParsePostfixLogs_LogFile::logWriter('ОБРАБОТКА файла логов "' . $maillogPath);
                $count = $fileImporter->import($dbWriter);
                ParsePostfixLogs_LogFile::logWriter('ОБРАБОТАНО: ' . $count['linecount'] . ' строк, писем найдено: ' . $count['lettercount']);
            }
            ParsePostfixLogs_LogFile::logWriter('ОБНОВЛЕНИЕ данных в таблице рассылок (mailings_delivery)...');
            $dbWriter->updateMessages();
            ParsePostfixLogs_LogFile::logWriter('ЗАВЕРШЕНО' . PHP_EOL);
            return true;
        }
        catch (Exception $ex)
        {
            throw $ex;
        }
    }
}