<?php defined('SYSPATH') or die();

/**
 * Clear message delivery
 */
class Cron_StatusMessageDelivery extends Cron
{

    public function run(array $params)
    {
        $time = Arr::get($params, 'time', time());

        $drivers = array(
            'emailUnisender',
            'smsSmsc',
            'smsUnisender',
        );
        foreach($drivers as $driver)
        {
            $query = ORM::factory('Message_Delivery')
                ->where('driver', '=', $driver)
                ->where('external_id', 'IS NOT', null)
                ->where('error', 'IS', null)
                ->where('created_at', '>', date('Y-m-d H:i:s', $time - Date::DAY));

            if ( 'emailUnisender' == $driver )
            {
                $query->where_open();
                    $query->or_where_open();
                        $query->where('delivered_at', 'IS', null);
                        $query->where('last_check_at', '<', date('Y-m-d H:i:s', $time - 5 * Date::MINUTE));
                    $query->or_where_close();
                    $query->or_where_open();
                        $query->where('opened_at', 'IS', null);
                        $query->where('last_check_at', '<', date('Y-m-d H:i:s', $time - 30 * Date::MINUTE));
                    $query->or_where_close();
                    $query->or_where_open();
                        $query->where('unsubscribed_at', 'IS', null);
                        $query->where('last_check_at', '<', date('Y-m-d H:i:s', $time - 60 * Date::MINUTE));
                    $query->or_where_close();
                $query->where_close();
            }
            elseif ( 'smsSmsc' == $driver || 'smsUnisender' == $driver || 'smsIbatelecom' == $driver )
            {
                $query->where('delivered_at', 'IS', null);
                $query->where('last_check_at', '<', date('Y-m-d H:i:s', $time - 5 * Date::MINUTE));
            }
            else
            {
                $query->where('last_check_at', '<', date('Y-m-d H:i:s', $time - 60 * Date::MINUTE));
            }

            $perChunk = 100;
            $chunk = 0;
            while(true)
            {
                $queryChunk = clone $query;
                $modelsMessage_Delivery = $queryChunk
                    ->limit($perChunk)
                    ->offset($perChunk * $chunk++)
                    ->find_all();
                if ( 0 == count($modelsMessage_Delivery) )
                {
                    break;
                }

                try
                {
                    DeliveryMan_Message_Delivery::instance($driver)->checkMessageDelivery($modelsMessage_Delivery);
                }
                catch(Exception $e)
                {
                    Kohana::$log->add(Log::ERROR, 'DELIVERYMAN/CRON/STATUSMESSAGEDELIVERY/ERROR :DRIVER :MESSAGE', array(
                        ':DRIVER' => $driver,
                        ':MESSAGE' => $e->getMessage(),
                    ));
                }
            }
        }

        if ( isset($e) )
        {
            throw $e;
        }
    }

}
