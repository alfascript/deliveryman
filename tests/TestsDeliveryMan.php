<?php defined('SYSPATH') or die('No direct access allowed!');

class TestsDeliveryMan extends TestsBase
{
    
    public function testDeliveryManMessageDelivery()
    {
        // setUp
        DB::delete(ORM::factory('Message_Delivery')->table_name())
            ->where('addressRecepient', '=', 'test@test.test')
            ->execute();
        
        DeliveryMan_Message_Delivery::instance('emailNull')->sendMessage(null, 'test@test.test', 'Тема тестового письма', 'Сообщение тестового письма', 'Короткое сообщение');

        $this->assertGreaterThan(0, ORM::factory('Message_Delivery')
            ->where('driver', '=', 'emailNull')
            ->where('addressRecepient', '=', 'test@test.test')
            ->count_all());
        
        // tearDown
        DB::delete(ORM::factory('Message_Delivery')->table_name())
            ->where('addressRecepient', '=', 'test@test.test')
            ->execute();
    }

    public function testDeliveryManMessage()
    {
        // setUp
        $user = ORM::factory('User', array('username' => Model_Index::GSCMS_CONFIG('tests.email')));
        $this->deleteModel($user);
        $messagetemplate = ORM::factory('Messagetemplate', array('name' => 'test'));
        if ( $messagetemplate->loaded() )
        {
            $messagetemplate->delete();
        }
        
        // Регистриуем тестового пользователя test@test.te:12345
        $this->registerTestUser(false);
        
        // Отправляем сообщение с тестовым шаблоном
        $user = ORM::factory('User', array('username' => Model_Index::GSCMS_CONFIG('tests.email')));
        $messagetemplate = ORM::factory('Messagetemplate');
        $messagetemplate->name = 'test';
        $messagetemplate->subject = 'Тема сообщения';
        $messagetemplate->body = 'Полный текст сообщения';
        $messagetemplate->bodyShort = 'Короткий текст сообщения';
        $messagetemplate->enabled = 1;
        $messagetemplate->save();
        DeliveryMan_Message::instance($user)->sendMessage('test');

        $this->assertGreaterThan(0, $user->messages->where('messagetemplate_id', '=', $messagetemplate->id)->count_all());
        
        // tearDown
        $this->deleteModel($messagetemplate);
        $this->deleteModel($user);
    }
    
}
