<?php defined('SYSPATH') OR die('No direct access allowed.');

return [
    'menu' => [
        'scope' => [
            'route' => 'mailings',
            'title' => 'Рассылки',
        ],
        'items' => [
            'mailings' => [
                'action' => '',
                'title'  => 'История рассылок'
            ],
            'mailing_create' => [
                'action' => 'create',
                'title'  => 'Создать рассылку'
            ],
        ]
    ],
];