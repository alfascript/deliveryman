<?php defined('SYSPATH') OR die('No direct access allowed.');

return [
    'menu' => [
        'scope' => [
            'route' => 'messagetemplate',
            'title' => 'Сообщения',
        ],
        'items' => [
            'messagetemplate' => [
                'action' => '',
                'title'  => 'Сообщения'
            ],
            'message_headers' => [
                'action' => 'headers',
                'title'  => 'Заголовки'
            ],
            'message_bodies' => [
                'action' => 'bodies',
                'title'  => 'Шаблоны'
            ],
        ]
    ],
];