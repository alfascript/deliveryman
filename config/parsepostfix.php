<?php defined('SYSPATH') or die('No direct script access.');

return array(
    /**
     * The hostname you are sending mail from. Example: mysite.com
     */
    'messageIdHost'     => 'postmailers.ru',

    /**
     * Logs path
     */
    //'logDir'            => GSCMSLOGS . 'mailings_postfix/',
    'logDir'            => DOCROOT . '../../cmsdir/logs/mailings_postfix/',

    /**
     * The default filename to be parsed when the script is called withot any parameters.
     */
    'MaillogPathes'     => array(
        '/var/log/mail.log.1',
        '/var/log/mail.log',
    ),

    /**
     * The regular expression used to detect the date of the line in the Postfix
     * 'mail.log' file. It is unlikely that you need to change this unless you have
     * some really weird package of Postfix.
     */
    'dateTimeRegex'     => '^([a-zA-Z]{3} [\d|\s]\d \d{2}\:\d{2}\:\d{2})',

    /**
     * The regular expression used to detect letter_id of the line in the Postfix
     * 'mail.log' file.
     */
    'MessageIdRegex'    => ' (ip-[0-9\-]*)(?:.*postfix\/cleanup\[\d+\]\: )([A-Z0-9]{10})(?:\: message-id=<)(M[0-9\.]+)(?:@',

    /**
     * The regular expression used to detect send statuses of the line in the Postfix
     * 'mail.log' file.
     */
    'SendStatusesRegex' => ' (ip-[0-9\-]*)(?:.*postfix\/smtp\[\d+\]\: )([A-Z0-9]{10})(?:\: to=<.*>.*dsn=[\d\.]+, status=)([a-zA-Z]+)(?: \()(.*)(?:\))',

    /**
     * Available writers: MySql/Redis (Warning: CaSe-sENsitiVe)
     *
     * Corresponding configuration files can be found inside the 'configs' directory.
     * If you are using the 'MySql' writer be sure to create database and import the
     * table schema from the 'sql' directory.
     *
     */
    'dbWriterClass'     => 'Redis',

    /*
     * MySql config
     */
    'MySql'             => array(

        /**
         * This is a very important option that depends on the configuration
         * of your MySQL server. This is the batch insert count when
         * parsing. '1000' must be pretty standard count, but if you
         * experience any problems you can tweak this eiter up or down.
         */
        'batchInsertsCount' => '1000',

        /*
         * Name of a temporary table
         */
        'temporaryTable'    => 'mailings_delivery_postfixlogs',
    ),

    /*
     * redis config
     */
    'redis'             => array(
        /*
         * Name of the redis db
         */
        'db' => 0,

        'params' => array(
            'scheme' => 'tcp',
            'host'   => 'gso-redis.0afk2e.0001.euc1.cache.amazonaws.com',
            'port'   => 6379,
        ),
    ),

);