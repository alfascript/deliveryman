<?php

return array(
    'project_db' => array(
        'type'         => 'MySQLi',
        'connection'   => array(
            'hostname'   => '127.0.0.1',
            'database'   => '',
            'username'   => '',
            'password'   => '',
            'persistent' => false,
        ),
        'table_prefix' => '',
        'charset'      => 'utf8',
        'caching'      => false,
    ),
);